<?php
// $Id: modinfo.php,v 1.2 2004/06/05 09:05:05 mithyt2 Exp $
// Module Info

// The name of this module
define("_MI_MYDOWNLOADS_NAME","Downloads");

// A brief description of this module
define("_MI_MYDOWNLOADS_DESC","Creates a downloads section where users can download/submit/rate various files.");

// Names of blocks for this module (Not all module has blocks)
define("_MI_MYDOWNLOADS_BNAME1","Recent Downloads");
define("_MI_MYDOWNLOADS_BNAME2","Top Downloads");

// Sub menu titles
define("_MI_MYDOWNLOADS_SMNAME1","Submit");
define("_MI_MYDOWNLOADS_SMNAME2","Popular");
define("_MI_MYDOWNLOADS_SMNAME3","Top Rated");

// Names of admin menu items
define("_MI_MYDOWNLOADS_ADMENU2","Add/Edit Downloads");
define("_MI_MYDOWNLOADS_ADMENU3","Submitted Downloads");
define("_MI_MYDOWNLOADS_ADMENU4","Broken Downloads");
define("_MI_MYDOWNLOADS_ADMENU5","Modified Downloads");

// Title of config items
define('_MI_MYDOWNLOADS_POPULAR', 'Number of hits for downloadable items to be marked as popular');
define('_MI_MYDOWNLOADS_NEWDLS', 'Maximum number of new download items displayed on top page');
define('_MI_MYDOWNLOADS_PERPAGE', 'Maximum number of download items displayed on each page');
define('_MI_MYDOWNLOADS_USESHOTS', 'Select yes to display screenshot images for each download item');
define('_MI_MYDOWNLOADS_SHOTWIDTH', 'Maximum width of screenshot/thumbnails images');
define('_MI_MYDOWNLOADS_SHOTHEIGHT', 'Maximum height of screenshot/thumbnails images');
define('_MI_MYDOWNLOADS_CHECKHOST', 'Disallow direct download linking? (leeching)');
define('_MI_MYDOWNLOADS_REFERERS', 'These sites can directly link to your files <br />Separate with | ');
define("_MI_MYDOWNLOADS_ANONPOST","Allow anonymous users to post download items?");
define('_MI_MYDOWNLOADS_AUTOAPPROVE','Auto approve new downloads without admin intervention?');
define('_MI_MYDOWNLOADS_MAXFILESIZE','Maximum upload size');
define('_MI_MYDOWNLOADS_IMGWIDTH','Maximum upload Image width');
define('_MI_MYDOWNLOADS_IMGHEIGHT','Maximum upload Image height');
define('_MI_MYDOWNLOADS_UPLOADDIR','Upload Directory (No trailing slash)');
define('_MI_MYDOWNLOADS_MIMETYPES','Allowed Mimetypes for Upload');
define('_MI_MYDOWNLOADS_ALLOWSUBMISS','Allow user submissions?');
define('_MI_MYDOWNLOADS_ALLOWUPLOADS','Allow user uploads?');
define('_MI_MYDOWNLOADS_SCREENSHOTS','Screen Shots Upload Directory');
define('_MI_MYDOWNLOADS_CATEGORYIMG','Category Image Upload Directory');
define('_MI_MYDOWNLOADS_MAINIMGDIR','Main Image Directory');
define('_MI_MYDOWNLOADS_USETHUMBS', 'Thumb Nails:');
define('_MI_MYDOWNLOADS_DATEFORMAT', 'Choose Date Format:');
define('_MI_MYDOWNLOADS_SHOWDISCLAIMER', 'Show Disclaimer before User Submission?');
define('_MI_MYDOWNLOADS_SHOWDOWNDISCL', 'Show Disclaimer before User Download?');
define('_MI_MYDOWNLOADS_DISCLAIMER', 'Enter Submission Disclaimer Text:');
define('_MI_MYDOWNLOADS_DOWNDISCLAIMER', 'Enter Download Disclaimer Text:');
define('_MI_MYDOWNLOADS_PLATFORM', 'Enter Platforms:');
define('_MI_MYDOWNLOADS_EMPTYCATS', 'Empty Categories:');
define('_MI_MYDOWNLOADS_SUBCATS', 'Sub-Categories:');
define('_MI_MYDOWNLOADS_VERSIONTYPES', 'Version Status:');
define('_MI_MYDOWNLOADS_LICENSE', 'Enter License:');
define('_MI_MYDOWNLOADS_LIMITS', 'Enter File Limitations:');
// Description of each config items
define('_MI_MYDOWNLOADS_USETHUMBSDSC', 'Downloads will use thumb nails for images. Set to \'No\' to use orginal image if the server does not support this option.');
define('_MI_MYDOWNLOADS_POPULARDSC', '');
define('_MI_MYDOWNLOADS_NEWDLSDSC', '');
define('_MI_MYDOWNLOADS_PERPAGEDSC', '');
define('_MI_MYDOWNLOADS_USESHOTSDSC', '');
define('_MI_MYDOWNLOADS_SHOTWIDTHDSC', '');
define('_MI_MYDOWNLOADS_REFERERSDSC', '');
define('_MI_MYDOWNLOADS_AUTOAPPROVEDSC', '');
define('_MI_MYDOWNLOADS_MAXFILESIZEDSC', '');
define('_MI_MYDOWNLOADS_IMGWIDTHDSC', '');
define('_MI_MYDOWNLOADS_IMGHEIGHTDSC', '');
define('_MI_MYDOWNLOADS_UPLOADDIRDSC', '');
define('_MI_MYDOWNLOADS_MIMETYPESDSC', '');
define('_MI_MYDOWNLOADS_ALLOWSUBMISSDSC', '');
define('_MI_MYDOWNLOADS_MAINIMGDIRDSC', '');
define('_MI_MYDOWNLOADS_SHOWDISCLAIMERDSC', '');
define('_MI_MYDOWNLOADS_SHOWDOWNDISCLDSC', '');
define('_MI_MYDOWNLOADS_DISCLAIMERDSC', '');
define('_MI_MYDOWNLOADS_PLATFORMDSC', 'List of platforms to enter <br />Separate with | IMPORTANT: Do not change this once the site is Live, Add new to the end of the list!');
define('_MI_MYDOWNLOADS_EMPTYCATSDSC', 'Select Yes to display categories with no download entries. Selecting \'No\' will hide Empty categories from the listings');
define('_MI_MYDOWNLOADS_SUBCATSDSC', 'Select Yes to display sub-categories. Selecting \'No\' will hide sub-categories from the listings');

define('_MI_MYDOWNLOADS_LICENSEDSC', 'List of platforms to enter <br />Separate with |');

// Text for notifications
define('_MI_MYDOWNLOADS_GLOBAL_NOTIFY', 'Global');
define('_MI_MYDOWNLOADS_GLOBAL_NOTIFYDSC', 'Global downloads notification options.');

define('_MI_MYDOWNLOADS_CATEGORY_NOTIFY', 'Category');
define('_MI_MYDOWNLOADS_CATEGORY_NOTIFYDSC', 'Notification options that apply to the current file category.');

define('_MI_MYDOWNLOADS_FILE_NOTIFY', 'File');
define('_MI_MYDOWNLOADS_FILE_NOTIFYDSC', 'Notification options that apply to the current file.');

define('_MI_MYDOWNLOADS_GLOBAL_NEWCATEGORY_NOTIFY', 'New Category');
define('_MI_MYDOWNLOADS_GLOBAL_NEWCATEGORY_NOTIFYCAP', 'Notify me when a new file category is created.');
define('_MI_MYDOWNLOADS_GLOBAL_NEWCATEGORY_NOTIFYDSC', 'Receive notification when a new file category is created.');
define('_MI_MYDOWNLOADS_GLOBAL_NEWCATEGORY_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New file category');                              

define('_MI_MYDOWNLOADS_GLOBAL_FILEMODIFY_NOTIFY', 'Modify File Requested');
define('_MI_MYDOWNLOADS_GLOBAL_FILEMODIFY_NOTIFYCAP', 'Notify me of any file modification request.');
define('_MI_MYDOWNLOADS_GLOBAL_FILEMODIFY_NOTIFYDSC', 'Receive notification when any file modification request is submitted.');
define('_MI_MYDOWNLOADS_GLOBAL_FILEMODIFY_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : File Modification Requested');

define('_MI_MYDOWNLOADS_GLOBAL_FILEBROKEN_NOTIFY', 'Broken File Submitted');
define('_MI_MYDOWNLOADS_GLOBAL_FILEBROKEN_NOTIFYCAP', 'Notify me of any broken file report.');
define('_MI_MYDOWNLOADS_GLOBAL_FILEBROKEN_NOTIFYDSC', 'Receive notification when any broken file report is submitted.');
define('_MI_MYDOWNLOADS_GLOBAL_FILEBROKEN_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : Broken File Reported');

define('_MI_MYDOWNLOADS_GLOBAL_FILESUBMIT_NOTIFY', 'File Submitted');
define('_MI_MYDOWNLOADS_GLOBAL_FILESUBMIT_NOTIFYCAP', 'Notify me when any new file is submitted (awaiting approval).');
define('_MI_MYDOWNLOADS_GLOBAL_FILESUBMIT_NOTIFYDSC', 'Receive notification when any new file is submitted (awaiting approval).');
define('_MI_MYDOWNLOADS_GLOBAL_FILESUBMIT_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New file submitted');

define('_MI_MYDOWNLOADS_GLOBAL_NEWFILE_NOTIFY', 'New File');
define('_MI_MYDOWNLOADS_GLOBAL_NEWFILE_NOTIFYCAP', 'Notify me when any new file is posted.');
define('_MI_MYDOWNLOADS_GLOBAL_NEWFILE_NOTIFYDSC', 'Receive notification when any new file is posted.');
define('_MI_MYDOWNLOADS_GLOBAL_NEWFILE_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New file');

define('_MI_MYDOWNLOADS_CATEGORY_FILESUBMIT_NOTIFY', 'File Submitted');
define('_MI_MYDOWNLOADS_CATEGORY_FILESUBMIT_NOTIFYCAP', 'Notify me when a new file is submitted (awaiting approval) to the current category.');   
define('_MI_MYDOWNLOADS_CATEGORY_FILESUBMIT_NOTIFYDSC', 'Receive notification when a new file is submitted (awaiting approval) to the current category.');      
define('_MI_MYDOWNLOADS_CATEGORY_FILESUBMIT_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New file submitted in category'); 

define('_MI_MYDOWNLOADS_CATEGORY_NEWFILE_NOTIFY', 'New File');
define('_MI_MYDOWNLOADS_CATEGORY_NEWFILE_NOTIFYCAP', 'Notify me when a new file is posted to the current category.');   
define('_MI_MYDOWNLOADS_CATEGORY_NEWFILE_NOTIFYDSC', 'Receive notification when a new file is posted to the current category.');      
define('_MI_MYDOWNLOADS_CATEGORY_NEWFILE_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New file in category'); 

define('_MI_MYDOWNLOADS_FILE_APPROVE_NOTIFY', 'File Approved');
define('_MI_MYDOWNLOADS_FILE_APPROVE_NOTIFYCAP', 'Notify me when this file is approved.');
define('_MI_MYDOWNLOADS_FILE_APPROVE_NOTIFYDSC', 'Receive notification when this file is approved.');
define('_MI_MYDOWNLOADS_FILE_APPROVE_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : File Approved');

?>
