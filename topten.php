<?php 
// $Id: topten.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";

global $xoopsDB, $xoopsUser;

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
$xoopsOption['template_main'] = 'mydownloads_topten.html';

$groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
$gperm_handler = &xoops_gethandler('groupperm');

include XOOPS_ROOT_PATH . "/header.php";

$action_array = array('hit' => 0, 'rate' => 1);
$list_array = array('hits', 'rating');
$lang_array = array(_MD_HITS, _MD_RATING);

$sort = (isset($_GET['list']) && in_array($_GET['list'], $action_array)) ? $_GET['list'] : 'rate';
$this = $action_array[$sort];
$sortDB = $list_array[$this];

$catarray['imageheader'] = imageheader();
$catarray['toolbar'] = toolbar();
$xoopsTpl->assign('catarray', $catarray);

$arr = array();
$result = $xoopsDB->query("SELECT cid, title FROM " . $xoopsDB->prefix("mydownloads_cat") . " WHERE pid=0");

$e = 0;
$rankings = array();
while (list($cid, $ctitle) = $xoopsDB->fetchRow($result))
{
    $query = "SELECT lid, cid, title, hits, rating, votes, platform FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND (cid=$cid";
    $arr = $mytree->getAllChildId($cid);
    for($i = 0;$i < count($arr);$i++)
    {
        $query .= " or cid=" . $arr[$i] . "";
    } 
    $query .= ") order by " . $sortDB . " DESC";
    $result2 = $xoopsDB->query($query, 10, 0);
    $filecount = $xoopsDB->getRowsNum($result2);

    if ($filecount > 0)
    {
        $rankings[$e]['title'] = $myts->htmlSpecialChars($ctitle);
        $rank = 1;

        while (list($did, $dcid, $dtitle, $hits, $rating, $votes) = $xoopsDB->fetchRow($result2))
        {
            $catpath = $mytree->getPathFromId($dcid, "title");
            $catpath = basename($catpath);

            $dtitle = $myts->htmlSpecialChars($dtitle);
            if ($catpath != $ctitle)
            {
                $dtitle = $myts->htmlSpecialChars($dtitle) . $ctitle;
            } 

            $rankings[$e]['file'][] = array('id' => $did, 'cid' => $dcid, 'rank' => $rank, 'title' => $dtitle, 'category' => $catpath, 'hits' => $hits, 'rating' => number_format($rating, 2), 'votes' => $votes);
            $rank++;
        } 
        $e++;
    } 
} 

$xoopsTpl->assign('lang_sortby' , $lang_array[$this]);
$xoopsTpl->assign('lang_displayedby' , _MD_DISPLAYING);
$xoopsTpl->assign('lang_rank' , _MD_RANK);
$xoopsTpl->assign('lang_title' , _MD_TITLE);
$xoopsTpl->assign('lang_category' , _MD_CATEGORY);
$xoopsTpl->assign('lang_hits' , _MD_HITS);
$xoopsTpl->assign('lang_rating' , _MD_RATING);
$xoopsTpl->assign('lang_vote' , _MD_VOTE);

$xoopsTpl->assign('rankings', $rankings);
include XOOPS_ROOT_PATH . '/footer.php';

include "footer.php";

?>
