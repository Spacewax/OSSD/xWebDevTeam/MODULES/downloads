<?php 
// $Id: brokenfile.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";

if (!empty($_POST['submit']))
{
    global $xoopsModule, $xoopsModuleConfig;

    $sender = (is_object($xoopsUser)) ? $xoopsUser->getVar('uid') : 0;
    $ip = getenv("REMOTE_ADDR");
    $lid = intval($_POST['lid']); 
    // Check if REG user is trying to report twice.
    $result = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_broken") . " WHERE lid=$lid");
    list ($count) = $xoopsDB->fetchRow($result);
    if ($count > 0)
    {
        redirect_header("index.php", 2, _MD_ALREADYREPORTED);
        exit();
    }
    else
    {
        $newid = $xoopsDB->genId($xoopsDB->prefix("mydownloads_broken") . "_reportid_seq");
        $sql = sprintf("INSERT INTO %s (reportid, lid, sender, ip, date) VALUES (%u, %u, %u, '%s', %u)", $xoopsDB->prefix("mydownloads_broken"), $newid, $lid, $sender, $ip, time());
        $xoopsDB->query($sql) or exit();
        $tags = array();
        $tags['BROKENREPORTS_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/admin/index.php?op=listBrokenDownloads';
        $notification_handler = &xoops_gethandler('notification');
        $notification_handler->triggerEvent('global', 0, 'file_broken', $tags);

        /**
         * Send email to the owner of the download stating that it is broken
         */
        $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE lid = $lid AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ")";
        $down_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
        unset($sql);

        $user = new XoopsUser(intval($down_arr['submitter']));
        $subdate = formatTimestamp($down_arr['date'], $xoopsModuleConfig['dateformat']);
        $cid = $down_arr['cid'];
        $title = $down_arr['title'];
        $subject = _MD_BROKENREPORTED;

        $xoopsMailer = &getMailer();
        $xoopsMailer->useMail();
        $template_dir = XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/language/" . $xoopsConfig['language'] . "/mail_template";

        $xoopsMailer->setTemplateDir($template_dir);
        $xoopsMailer->setTemplate('filebroken_notify.tpl');
        $xoopsMailer->setToEmails($user->email());
        $xoopsMailer->setFromEmail($xoopsConfig['adminmail']);
        $xoopsMailer->setFromName($xoopsConfig['sitename']);
        $xoopsMailer->assign("X_UNAME", $user->uname());
        $xoopsMailer->assign("SITENAME", $xoopsConfig['sitename']);
        $xoopsMailer->assign("X_ADMINMAIL", $xoopsConfig['adminmail']);
        $xoopsMailer->assign('X_SITEURL', XOOPS_URL . '/');
        $xoopsMailer->assign("X_TITLE", $title);
        $xoopsMailer->assign("X_SUB_DATE", $subdate);
        $xoopsMailer->assign('X_DOWNLOAD', XOOPS_URL . '/modules/' . $xoopsModule->dirname() . '/singlefile.php?cid=' . $cid . '&amp;lid=' . $lid);
        $xoopsMailer->setSubject($subject);
        //$xoopsMailer->setBody($message);
        $xoopsMailer->send();
        redirect_header("index.php", 2, _MD_THANKSFORINFO);
        exit();
    }
}
else
{
    $xoopsOption['template_main'] = 'mydownloads_brokenfile.html';
    include XOOPS_ROOT_PATH . '/header.php';
    /**
     * Begin Main page Heading etc
     */
    $catarray['imageheader'] = imageheader();
    $xoopsTpl->assign('catarray', $catarray);

    $lid = (isset($HTTP_GET_VARS['lid']) && $HTTP_GET_VARS['lid'] > 0) ? intval($HTTP_GET_VARS['lid']) : 0;
    $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE lid = $lid AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ")";
    $down_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
    unset($sql);

    $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_broken') . " WHERE lid = $lid";
    $broke_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));;

    if (is_array($broke_arr))
    {
        global $xoopsModuleConfig;

        $broken['title'] = trim($down_arr['title']);
        $broken['id'] = trim($broke_arr['reportid']);
        $broken['reporter'] = xoops_getLinkedUnameFromId(intval($broke_arr['sender']));
        $broken['date'] = formatTimestamp($broke_arr['date'], $xoopsModuleConfig['dateformat']);
        $broken['acknowledged'] = ($broke_arr['acknowledged'] == 1) ? _YES : _NO ;
        $broken['confirmed'] = ($broke_arr['confirmed'] == 1) ? _YES : _NO ;
    	
		$xoopsTpl->assign('lang_filetitle', _MD_FILETITLE);
        $xoopsTpl->assign('lang_webmastercon', _MD_WEBMASTERCONFIRM);
        $xoopsTpl->assign('lang_webmasterack', _MD_WEBMASTERACKNOW);
        $xoopsTpl->assign('lang_reporter', _MD_REPORTER);
        $xoopsTpl->assign('lang_sourceid', _MD_RESOURCEID);
        $xoopsTpl->assign('lang_datereported', _MD_DATEREPORTED);
        $xoopsTpl->assign('lang_thanksforreporting', _MD_THANKSFORREPORTING);
        $xoopsTpl->assign('broken', $broken);
        $xoopsTpl->assign('lang_alreadyreported', _MD_RESOURCEREPORTED);
        $xoopsTpl->assign('brokenreport', true);
    }
    else
    {
        $amount = $xoopsDB->getRowsNum($sql);

        if (!is_array($down_arr))
        {
            redirect_header('index.php', 0 , _MD_THISFILEDOESNOTEXIST);
            exit();
        }
        /**
         * file info
         */
        $down['title'] = trim($down_arr['title']);
        $down['homepage'] = $myts->makeClickable(formatURL(trim($down_arr['url'])));
        $time = ($down_arr['updated'] != 0) ? $down_arr['updated'] : $down_arr['published'];
        $down['updated'] = formatTimestamp($time, $xoopsModuleConfig['dateformat']);
        $is_updated = ($down_arr['updated'] != 0) ? _MD_UPDATEDON : _MD_SUBMITDATE;
        $down['publisher'] = xoops_getLinkedUnameFromId(intval($down_arr['submitter']));

        $xoopsTpl->assign('lang_subdate' , $is_updated);
        $xoopsTpl->assign('down', $down);
        $xoopsTpl->assign('lang_reportbroken', _MD_BROKENREPORT);
        $xoopsTpl->assign('lang_filesource', _MD_HOMEPAGEC);
        $xoopsTpl->assign('lang_submitbroken', _MD_SUBMITBROKEN);
        $xoopsTpl->assign('lang_beforesubmit', _MD_BEFORESUBMIT);
        $xoopsTpl->assign('lang_publisher' , _MD_PUBLISHER);
        $xoopsTpl->assign('file_id', $lid);
        $xoopsTpl->assign('lang_thanksforhelp', _MD_THANKSFORHELP);
        $xoopsTpl->assign('lang_forsecurity', _MD_FORSECURITY);
        $xoopsTpl->assign('lang_cancel', _MD_CANCEL);
    }
    include_once XOOPS_ROOT_PATH . '/footer.php';
}

?>
