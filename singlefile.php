<?php 
// $Id: singlefile.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
// Used to view just a single DL file information. Called from the rating pages
$lid = intval($HTTP_GET_VARS['lid']);
$cid = intval($HTTP_GET_VARS['cid']);
$xoopsOption['template_main'] = 'mydownloads_singlefile.html';

$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE lid = $lid AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ")";
$down_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
$amount = $xoopsDB->getRowsNum($sql);

$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_text') . " WHERE lid = $lid";
$down_arr_text = $xoopsDB->fetcharray($xoopsDB->query($sql));
$down_arr['description'] = $down_arr_text['description'];

include XOOPS_ROOT_PATH . "/header.php";

/**
 * Begin Main page Heading etc
 */
$catarray['imageheader'] = imageheader();
$xoopsTpl->assign('catarray', $catarray);

$down['id'] = intval($down_arr['lid']);
$down['cid'] = intval($down_arr['cid']);

$pathstring = "<a href='index.php'>" . _MD_MAIN . "</a>&nbsp;:&nbsp;";
$pathstring .= $mytree->getNicePathFromId($down_arr['cid'], "title", "viewcat.php?op=");
$xoopsTpl->assign('category_path', $pathstring);

$path = $mytree->getPathFromId($down_arr['cid'], "title");
$path = substr($path, 1);
$path = str_replace("/", " <img src='" . XOOPS_URL . "/modules/mydownloads/images/arrow.gif' board='0' alt=''> ", $path);
$down['category'] = $path;

include_once XOOPS_ROOT_PATH . "/modules/mydownloads/include/downloadinfo.php";

$xoopsTpl->assign('show_screenshot', false);
if ($xoopsModuleConfig['useshots'] == 1)
{
    $xoopsTpl->assign('shotwidth', $xoopsModuleConfig['shotwidth']);
    $xoopsTpl->assign('shotheight', $xoopsModuleConfig['shotheight']);
    $xoopsTpl->assign('show_screenshot', true);
}

$sql = "SELECT lid, cid, title FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE submitter = ".$down_arr['submitter']." AND lid != ".$down['id']." AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ")";
$result = $xoopsDB->query($sql, 10, 0);

while ($arr = $xoopsDB->fetchArray($result))
{
	$downuid['title'] = $arr['title'];
	$downuid['lid'] = $arr['lid'];
	$downuid['cid'] = $arr['cid'];
	$xoopsTpl->append('down_uid', $downuid);
}

$sql_review = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_reviews') . " WHERE lid = " . $down_arr['lid'] . " AND submit = 1";
$result_review = $xoopsDB->query($sql_review);
$review_amount = $xoopsDB->getRowsNum($result_review);

$xoopsTpl->assign('lang_user_reviews', $xoopsConfig['sitename'] ." ". _MD_USERREVIEWSTITLE);
$user_reviews = "<a href='review.php.php?lid=".$down_arr['lid']."'>"._MD_USERREVIEWS."</a>";

if ( $review_amount > 0)
{
	$user_reviews = "<a href='review.php?op=list&amp;cid=".$down_arr['cid']."&amp;lid=".$down_arr['lid']."'>"._MD_USERREVIEWS."</a>";
	$xoopsTpl->assign('lang_UserReviews', sprintf($user_reviews, $down_arr['title']));
} else {
	$user_reviews = "<a href='review.php?cid=".$down_arr['cid']."&amp;lid=".$down_arr['lid']."'>"._MD_NOUSERREVIEWS."</a>";
	$xoopsTpl->assign('lang_UserReviews', sprintf($user_reviews, $down_arr['title']));
} 
/*
while ($arr_review = $xoopsDB->fetchArray($result_review))
{
	$down_review['review_id'] = $arr_review['review_id'];
	$down_review['lid'] = $arr_review['lid'];	
	$down_review['title'] = $arr_review['title'];
	$down_review['review'] = $arr_review['review'];
	$down_review['date'] = $arr_review['date'];
	$down_review['uid'] = xoops_getLinkedUnameFromId(intval($arr_review['uid']));

	$review_rating = number_format($arr_review['rated'], 0) / 2;
	$rateimg = "rate$review_rating.gif";
	$down_review['rated'] = $rateimg;

	$xoopsTpl->append('down_review', $down_review);

	$xoopsTpl->assign('lang_review', _MD_TITLE);
}
*/
$xoopsTpl->assign('lang_title', _MD_TITLE);
$xoopsTpl->assign('lang_screenshot', _MD_SCREENSHOT);
$xoopsTpl->assign('lang_screenshot_click', _MD_SCREENSHOTCLICK);
$xoopsTpl->assign('lang_history', _MD_HISTORY);
$xoopsTpl->assign('lang_requirements', _MD_REQUIREMENTS);
$xoopsTpl->assign('lang_features', _MD_FEATURES);
$xoopsTpl->assign('lang_description', _MD_DESCRIPTIONC);
$xoopsTpl->assign('lang_lastupdate', _MD_LASTUPDATEC);
$xoopsTpl->assign('lang_hits', _MD_HITSC);
$xoopsTpl->assign('lang_ratingc', _MD_RATINGC);
$xoopsTpl->assign('lang_email', _MD_EMAILC);
$xoopsTpl->assign('lang_ratethissite', _MD_RATETHISFILE);
$xoopsTpl->assign('lang_reviewfile' , _MD_REVIEWTHISFILE);
$xoopsTpl->assign('lang_reportbroken', _MD_REPORTBROKEN);
$xoopsTpl->assign('lang_tellafriend', _MD_TELLAFRIEND);
$xoopsTpl->assign('lang_modify', _MD_MODIFY);
$xoopsTpl->assign('lang_version' , _MD_VERSION);
$xoopsTpl->assign('lang_dlnow' , _MD_DLNOW);
$xoopsTpl->assign('lang_category' , _MD_CATEGORYC);
$xoopsTpl->assign('lang_size' , _MD_FILESIZE);
$xoopsTpl->assign('lang_platform' , _MD_SUPPORTEDPLAT);
$xoopsTpl->assign('lang_homepage' , _MD_HOMEPAGE);
$xoopsTpl->assign('lang_comments' , _COMMENTS);
$xoopsTpl->assign('lang_price' , _MD_PRICE);
$xoopsTpl->assign('lang_limits' , _MD_LIMITS);
$xoopsTpl->assign('lang_mirror' , _MD_MIRROR);
$xoopsTpl->assign('lang_downloads' , _MD_DOWNLOADHITS);
$xoopsTpl->assign('lang_license' , _MD_DOWNLICENSE);
$xoopsTpl->assign('lang_publisher' , _MD_PUBLISHER);
$xoopsTpl->assign('lang_reviews' , _MD_REVIEWS);
$xoopsTpl->assign('lang_fulldetails' , _MD_VIEWDETAILS);
$xoopsTpl->assign('lang_otherbyuid' , _MD_OTHERBYUID);
$xoopsTpl->assign('lang_downtimes' , _MD_DOWNTIMES);
$xoopsTpl->assign('lang_downloadnow', _MD_DOWNLOADNOW);
$xoopsTpl->assign('down', $down);

include XOOPS_ROOT_PATH . '/include/comment_view.php';
include XOOPS_ROOT_PATH . '/footer.php';

?>
