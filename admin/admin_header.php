<?php
include( "../../../mainfile.php" );
include '../../../include/cp_header.php';
 
include "../include/functions.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";
include_once XOOPS_ROOT_PATH . "/class/xoopslists.php";
include_once XOOPS_ROOT_PATH . "/include/xoopscodes.php";
include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';
include_once XOOPS_ROOT_PATH . "/class/module.errorhandler.php";

	
if (is_object($xoopsUser)) {
    $xoopsModule = XoopsModule::getByDirname("mydownloads");
    if (!$xoopsUser->isAdmin($xoopsModule->mid())) {
        redirect_header(XOOPS_URL . "/", 3, _NOPERM);
        exit();
    } 
} else {
    redirect_header(XOOPS_URL . "/", 1, _NOPERM);
    exit();
}

$myts = &MyTextSanitizer::getInstance();

$imagearray = array('editimg' => "<img src='../images/icon/edit.gif' ALT='Edit'>",
    'deleteimg' => "<img src='../images/icon/delete.gif' ALT='Delete'>",
    'online' => "<img src='../images/icon/on.gif' ALT='Online'>",
    'offline' => "<img src='../images/icon/off.gif' ALT='Offline'>",
    'approved' => "<img src='../images/icon/on.gif' ALT='Approved'>",
    'notapproved' => "<img src='../images/icon/off.gif' ALT='Not Approved'>",
    'relatedfaq' => "<img src='../images/icon/link.gif' ALT='Related Link'>",
    'relatedurl' => "<img src='../images/icon/urllink.gif' ALT='Add Related URL'>",
    'addfaq' => "<img src='../images/icon/add.gif' ALT='Add'>",
    'approve' => "<img src='../images/icon/approve.gif' ALT='Approve'>",
    'statsimg' => "<img src='../images/icon/stats.gif' ALT='Stats'>",
	'ignore' => "<img src='../images/icon/ignore.gif' ALT='Ignore'>"
    );

?>