<?php
/**
 * $Id: upload.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
 * Module: my_downloads
 * Version: v2.0.1
 * Release Date: 20 November 2003
 * Author: Catzwolf
 * Licence: GNU
 */

include("admin_header.php");

$op = '';

if (isset($_POST))
{
    foreach ($_POST as $k => $v)
    {
        ${$k} = $v;
    }
}

if (isset($_GET))
{
    foreach ($_GET as $k => $v)
    {
        ${$k} = $v;
    }
}

if (isset($_GET['rootpath']))
{
    $rootpath = intval($_GET['rootpath']);
}
else
{
    $rootpath = 0;
}

switch ($op)
{
    case "upload":

        global $_POST;

        if ($HTTP_POST_FILES['uploadfile']['name'] != "")
        {
            if (file_exists(XOOPS_ROOT_PATH . "/" . $_POST['uploadpath'] . "/" . $HTTP_POST_FILES['uploadfile']['name']))
            {
                redirect_header("upload.php", 2, _AM_DOWN_IMAGEEXIST);
            }
            $allowed_mimetypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png');
            uploading($allowed_mimetypes, $HTTP_POST_FILES['uploadfile']['name'], "upload.php", 0, $_POST['uploadpath'], 1);
        }
        else
        {
            redirect_header("upload.php", 2 , _AM_DOWN_NOIMAGEEXIST);
        } 
        // exit();
        break;

    case "delfile":

        if (isset($confirm) && $confirm == 1)
        {
            $filetodelete = XOOPS_ROOT_PATH . "/" . $_POST['uploadpath'] . "/" . $_POST['downfile'];
            if (file_exists($filetodelete))
            {
                chmod($filetodelete, 0666);
                if (@unlink($filetodelete))
                {
                    redirect_header("upload.php", 1, _AM_FILEDELETED);
                }
                else
                {
                    redirect_header("upload.php", 1, _AM_ERRORDELETEFILE);
                }
            }
            exit();
        }
        else
        {
            xoops_cp_header();
            xoops_confirm(array('op' => 'delfile', 'uploadpath' => $_POST['uploadpath'], 'downfile' => $_POST['downfile'], 'confirm' => 1), 'upload.php', _AM_DELETEFILE . "<br/><br>" . $_POST['downfile'], "Delete");
        }
        break;

    case "default":
    default:

        $displayimage = '';

        xoops_cp_header();

        Global $xoopsUser, $xoopsUser, $xoopsConfig, $xoopsDB, $xoopsModuleConfig;

        $dirarray = array(1 => $xoopsModuleConfig['catimage'], 2 => $xoopsModuleConfig['screenshots'], 3 => $xoopsModuleConfig['mainimagedir']);
        $namearray = array(1 => _AM_DWN_CATIMAGE , 2 => _AM_DWN_SCREENSHOTS, 3 => _AM_DWN_MAINIMAGEDIR);
        $listarray = array(1 => _AM_CATIMAGE , 2 => _AM_SCREENSHOTS, 3 => _AM_MAINIMAGEDIR);

        adminmenu(_AM_DLADMIN);

        echo "<div><b>" . _AM_SERVERSTATUS . ":</b></div>";

        $safemode = (ini_get('safe_mode')) ? _AM_SAFEMODEISON : _AM_SAFEMODEISOFF;
        $downloads = (ini_get('enable_dl')) ? _AM_UPLOADSON : _AM_UPLOADSOFF;
        echo "<div>" . $safemode . "</div>";
        echo "<div>" . $downloads . "";
        if (ini_get('enable_dl'))
        {
            echo " " . _AM_ANDTHEMAX . " " . ini_get('upload_max_filesize') . "</div>";
        }

        if ($rootpath > 0)
        {
            echo "<p><div><b>" . _AM_UPLOADPATH . "</b> " . XOOPS_ROOT_PATH . "/" . $dirarray[$rootpath] . "</div></p>";
        }
        if (isset($listarray[$rootpath]))
        {
            $pathlist = $listarray[$rootpath];
            $namelist = $namearray[$rootpath];
        }
        else
        {
            $pathlist = '';
            $namelist = '';
        }

        $iform = new XoopsThemeForm(_AM_UPLOADIMAGE . $pathlist, "op", xoops_getenv('PHP_SELF'));
        $iform->setExtra('enctype="multipart/form-data"');

        ob_start();
        $iform->addElement(new XoopsFormHidden('dir', $rootpath));
        getDirSelectOption($namelist, $dirarray, $namearray);
        $iform->addElement(new XoopsFormLabel(_AM_DIRSELECT, ob_get_contents()));
        ob_end_clean();

        if ($rootpath > 0)
        {
            if (!isset($downfile)) $downfile = "blank.png";
            $graph_array = &XoopsLists::getImgListAsArray(XOOPS_ROOT_PATH . "/" . $dirarray[$rootpath]);

            $smallimage_select = new XoopsFormSelect('', 'downfile', $downfile);
            $smallimage_select->addOptionArray($graph_array);
            $smallimage_select->setExtra("onchange='showImgSelected(\"image\", \"downfile\", \"" . $dirarray[$rootpath] . "\", \"\", \"" . XOOPS_URL . "\")'");
            $smallimage_tray = new XoopsFormElementTray(_AM_VEIWIMAGE, '&nbsp;');
            $smallimage_tray->addElement($smallimage_select);
            $smallimage_tray->addElement(new XoopsFormLabel('', "<br /><br /><img src='" . XOOPS_URL . "/" . $dirarray[$rootpath] . "/" . $downfile . "' name='image' id='image' alt='' />"));
            $iform->addElement($smallimage_tray);

            $iform->addElement(new XoopsFormFile(_AM_UPLOADLINKIMAGE, 'uploadfile', $xoopsModuleConfig['maxfilesize']));
            $iform->addElement(new XoopsFormHidden('uploadpath', $dirarray[$rootpath]));
            $iform->addElement(new XoopsFormHidden('rootnumber', $rootpath));

            $dup_tray = new XoopsFormElementTray('', '');
            $dup_tray->addElement(new XoopsFormHidden('op', 'upload'));
            $butt_dup = new XoopsFormButton('', '', _SUBMIT, 'submit');
            $butt_dup->setExtra('onclick="this.form.elements.op.value=\'upload\'"');
            $dup_tray->addElement($butt_dup);

            $butt_dupct = new XoopsFormButton('', '', _DELETE, 'submit');
            $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'delfile\'"');
            $dup_tray->addElement($butt_dupct);
            $iform->addElement($dup_tray);
        }
        $iform->display();
}
xoops_cp_footer();

?>