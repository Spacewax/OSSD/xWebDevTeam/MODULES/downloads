<?php
include 'admin_header.php';

$op = '';

if (!isset($HTTP_POST_VARS['op'])) {
    $op = isset($HTTP_GET_VARS['op']) ? $HTTP_GET_VARS['op'] : 'listBrokenDownloads';
} else {
    $op = $HTTP_POST_VARS['op'];
} 

$lid = (isset($HTTP_GET_VARS['lid'])) ? $HTTP_GET_VARS['lid'] : 0;

switch ($op) {
    case "delBrokenDownloads":
        global $xoopsDB;

        $xoopsDB->queryF("DELETE FROM " . $xoopsDB->prefix("mydownloads_broken") . " WHERE lid = '$lid'");
        $xoopsDB->queryF("DELETE FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid = '$lid'");
        redirect_header("brokendown.php?op=default", 1, _AM_BROKENFILEDELETED);
        exit();
        break;

    case "ignoreBrokenDownloads":
        global $xoopsDB;

        $xoopsDB->queryF("DELETE FROM " . $xoopsDB->prefix("mydownloads_broken") . " WHERE lid = '$lid'");
        redirect_header("brokendown.php?op=default", 1, _AM_BROKENFILEIGNORED);
        exit();
        break;

    case "listBrokenDownloads":
    case "default":

        global $xoopsDB, $imagearray;
        $result = $xoopsDB->query("SELECT * FROM " . $xoopsDB->prefix("mydownloads_broken") . " ORDER BY reportid");
        $totalbrokendownloads = $xoopsDB->getRowsNum($result);

        xoops_cp_header();

        adminmenu(_AM_DLADMIN);
        echo "<h4>" . _AM_BROKENREPORTS . " ($totalbrokendownloads)</h4>";
        echo "<p><div><li>" . _AM_IGNOREDESC . "<br /><li>" . _AM_EDITESC . "<li>" . _AM_DELETEDESC . "</div></p>";

        echo"<table width='100%' border='0' cellspacing='1' cellpadding = '2' class='outer'>";
        echo "<tr align = 'center'>";
        echo "<th width = '3%' align = 'center'>" . _AM_ID . "</th>";
        echo "<th width = '35%' align = 'left'>" . _AM_TITLE . "</th>";
        echo "<th>" . _AM_REPORTER . "</th>";
        echo "<th>" . _AM_FILESUBMITTER . "</th>";
        echo "<th align='center'>" . _AM_ACTION . "</th>";
        echo "</tr>";

        if ($totalbrokendownloads == 0) {
            echo "<tr align = 'center'><td align = 'center' class='head' colspan = '5'>" . _AM_NOBROKEN . "</td></tr>";
        } else {
            while (list($reportid, $lid, $sender, $ip) = $xoopsDB->fetchRow($result)) {
                $result2 = $xoopsDB->query("SELECT cid, title, url, submitter FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid");
                list($cid, $fileshowname, $url, $submitter) = $xoopsDB->fetchRow($result2);
				
				if ($sender != 0) {
                    $result3 = $xoopsDB->query("SELECT uname, email FROM " . $xoopsDB->prefix("users") . " WHERE uid=" . $sender . "");
                    list($sendername, $email) = $xoopsDB->fetchRow($result3);
                } 
			
                $result4 = $xoopsDB->query("SELECT uname, email FROM " . $xoopsDB->prefix("users") . " WHERE uid=" . $sender . "");
                list($ownername, $owneremail) = $xoopsDB->fetchRow($result4);

                echo "<tr align = 'center'>
					<td class = 'head'>$reportid</td>
					<td class = 'even'  align = 'left'><a href='".XOOPS_URL."/modules/mydownloads/visit.php?cid=".$cid."&amp;lid=".$lid."' target=\"_blank\">" . $fileshowname . "</a></td>";
                if ($email == "") {
                    echo "<td class = 'even'>$sendername ($ip)";
                } else {
                    echo "<td class = 'even'><a href=mailto:$email>$sendername</a> ($ip)";
                } 
                if ($owneremail == '') {
                    echo "<td class = 'even'>$ownername";
                } else {
                    echo "<td class = 'even'><a href=mailto:$owneremail>$ownername</a>";
                } 
                echo "</td>";
                echo "<td align='center' class = 'even'>";
                echo "<a href='brokendown.php?op=ignoreBrokenDownloads&lid=$lid'>" . $imagearray['ignore'] . "</a>";
                echo "<a href='index.php?op=Download&lid=" . $lid . "'> " . $imagearray['editimg'] . " </a>";
                echo "<a href='brokendown.php?op=delBrokenDownloads&lid=$lid'>" . $imagearray['deleteimg'] . "</a>";
                echo "</td></tr>";
            } 
        } 
        echo"</table>";
} 
xoops_cp_footer();

?>
