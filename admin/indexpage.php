<?php


include 'admin_header.php';
include_once XOOPS_ROOT_PATH . '/include/xoopscodes.php';

if (isset($HTTP_POST_VARS))
{
    foreach ($HTTP_POST_VARS as $k => $v)
    {
        ${$k} = $v;
    } 
} 

global $xoopsConfig, $xoopstheme, $xoopsDB, $HTTP_POST_VARS, $myts, $xoopsUser;

$op = "";

if (isset($HTTP_POST_VARS['op'])) $op = $HTTP_POST_VARS['op'];

switch($op){
    case "save":

        global $xoopsConfig, $xoopsDB, $HTTP_POST_VARS;

        $indexheading = $HTTP_POST_VARS['indexheading'];
        $indexheader = $HTTP_POST_VARS['indexheader'];
        $indexfooter = $HTTP_POST_VARS['indexfooter'];
        $indeximage = $HTTP_POST_VARS['indeximage'];
        $nohtml = $HTTP_POST_VARS['nohtml'];
        $nosmiley = $HTTP_POST_VARS['nosmiley'];
        $noxcodes = $HTTP_POST_VARS['noxcodes'];
        $noimages = $HTTP_POST_VARS['noimages'];
        $nobreak = $HTTP_POST_VARS['nobreak'];
        $indexheaderalign = $HTTP_POST_VARS['indexheaderalign'];
        $indexfooteralign = $HTTP_POST_VARS['indexfooteralign'];

        $xoopsDB->query("update " . $xoopsDB->prefix("mydownloads_indexpage") . " set indexheading='$indexheading', indexheader='$indexheader', indexfooter='$indexfooter', indeximage='$indeximage', indexheaderalign='$indexheaderalign ', indexfooteralign='$indexfooteralign', nohtml='$nohtml', nosmiley='$nosmiley', noxcodes='$noxcodes', noimages='$noimages', nobreak='$nobreak' ");
        redirect_header("indexpage.php", 1, _AM_DBUPDATED);
        exit();

        break;

    default:

        $result = $xoopsDB->query("SELECT indeximage, indexheading, indexheader, indexfooter, nohtml, nosmiley, noxcodes, noimages, nobreak, indexheaderalign, indexfooteralign FROM " . $xoopsDB->prefix("mydownloads_indexpage") . " ");
        list($indeximage, $indexheading, $indexheader, $indexfooter, $nohtml, $nosmiley, $noxcodes, $noimages, $nobreak, $indexheaderalign, $indexfooteralign) = $xoopsDB->fetchrow($result);

        xoops_cp_header();

        global $xoopsConfig, $xoopsModuleConfig;

        adminmenu(_AM_DLADMIN, 0);

		echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _AM_INDEXPAGECONFIG2 . "</legend>";
        echo "<div style='padding: 8px;'>" . _AM_INDEXPAGECONFIGTXT . "<br />";
		echo "</fieldset>";
     
        include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";
        $sform = new XoopsThemeForm(_AM_MENUS, "op", xoops_getenv('PHP_SELF'));

  		if (!$indeximage) $indeximage = "blank.png";
		$graph_array = &XoopsLists :: getImgListAsArray(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['mainimagedir']);
		$indeximage_select = new XoopsFormSelect('', 'indeximage', $indeximage);
		$indeximage_select -> addOptionArray($graph_array);
		$indeximage_select -> setExtra("onchange='showImgSelected(\"image1\", \"indeximage\", \"" . $xoopsModuleConfig['mainimagedir'] . "\", \"\", \"" . XOOPS_URL . "\")'");
		$indeximage_tray = new XoopsFormElementTray(_AM_SELECT_IMG, '&nbsp;');
		$indeximage_tray -> addElement($indeximage_select);
		$indeximage_tray -> addElement(new XoopsFormLabel('', "<p><img src='" . XOOPS_URL . "/" . $xoopsModuleConfig['mainimagedir'] . "/" . $indeximage . "' name='image1' id='image1' alt='' border = '1'/></p>"));
		$sform -> addElement($indeximage_tray);
       
		$sform->addElement(new XoopsFormText(_AM_INDEXHEADING, 'indexheading', 60, 60, $indexheading), false);
        $sform->addElement(new XoopsFormDhtmlTextArea(_AM_SECTIONHEAD, 'indexheader', $indexheader, 15, 60));
        $headeralign_select = new XoopsFormSelect(_AM_SECTIONHEADALI, "indexheaderalign", $indexheaderalign);
        $headeralign_select->addOptionArray(array("left" => _AM_ISLEFT, "right" => _AM_ISRIGHT, "Center" => _AM_ISCENTER));
        $sform->addElement($headeralign_select);
        $sform->addElement(new XoopsFormDhtmlTextArea(_AM_SECTIONFOOT, 'indexfooter', $indexfooter, 15, 60));
        $footeralign_select = new XoopsFormSelect(_AM_SECTIONFOOTALI, "indexfooteralign", $indexfooteralign);
        $footeralign_select->addOptionArray(array("left" => _AM_ISLEFT, "right" => _AM_ISRIGHT, "Center" => _AM_ISCENTER));
        $sform->addElement($footeralign_select);

        if (!isset($nohtml)) $nohtml = 0;
        $nohtml_radio = new XoopsFormRadioYN(_AM_SECTIONHTML, 'nohtml', $nohtml, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($nohtml_radio);

        if (!isset($nosmiley)) $nosmiley = 0;
        $nosmiley_radio = new XoopsFormRadioYN(_AM_SECTIONSMILEY, 'nosmiley', $nosmiley, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($nosmiley_radio);

        if (!isset($noxcodes)) $noxcodes = 0;
        $noxcode_radio = new XoopsFormRadioYN(_AM_SECTIONXCODE, 'noxcodes', $noxcodes, ' ' . _AM_YES . '', ' ' . _AM_NO . ' ');
        $sform->addElement($noxcode_radio);

        if (!isset($noimages)) $noimages = 0;
        $noimages_radio = new XoopsFormRadioYN(_AM_SECTIONIMAGES, 'noimages', $noimages, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($noimages_radio);

        if (!isset($nobreak)) $nobreak = 1;
        $nobreak_radio = new XoopsFormRadioYN(_AM_SECTIONBREAK, 'nobreak', $nobreak, ' ' . _AM_YES . '', ' ' . _AM_NO . ' ' . _AM_THISWILLREPLACELINEBREAKS . ' ');
        $sform->addElement($nobreak_radio);

        $button_tray = new XoopsFormElementTray('', '');
        $hidden = new XoopsFormHidden('op', 'save');
        $button_tray->addElement($hidden);
        $button_tray->addElement(new XoopsFormButton('', 'post', _AM_SAVECHANGE, 'submit'));
        $sform->addElement($button_tray);
        $sform->display();

        break;
}
xoops_cp_footer();

?>
