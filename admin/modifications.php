<?php 
// $Id: modifications.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include 'admin_header.php';

foreach ( $HTTP_POST_VARS as $k => $v )
{
    $$k = $v;
} 

foreach ( $HTTP_GET_VARS as $k => $v )
{
    $$k = $v;
} 

if ( !isset( $HTTP_POST_VARS['op'] ) )
{
    $op = isset( $HTTP_GET_VARS['op'] ) ? $HTTP_GET_VARS['op'] : 'main';
} 
else
{
    $op = $HTTP_POST_VARS['op'];
} 

$mytree = new XoopsTree( $xoopsDB -> prefix( "mydownloads_cat" ), "cid", "pid" );
switch ( $op )
{
    case "listModReqshow": 
        // listModReqshow( $HTTP_GET_VARS['requestid'] );
        global $xoopsDB, $myts, $mytree, $xoopsModuleConfig;
        
		$requestid = $HTTP_GET_VARS['requestid'];
		
		$result = $xoopsDB -> query( "SELECT requestid, lid, cid, title, url, homepage, version, size, platform, logourl, description, modifysubmitter FROM " . $xoopsDB -> prefix( "mydownloads_mod" ) . " WHERE requestid=" . $requestid . " ORDER BY requestid" );
        $totalmodrequests = $xoopsDB -> getRowsNum( $result );
        
		xoops_cp_header();
        adminmenu( _AM_DLADMIN, 0 );
        echo "<h4>" . _AM_USERMODREQ . " ($totalmodrequests)</h4>";

        if ( $totalmodrequests > 0 )
        {
            $lookup_lid = array();
            while ( list( $requestid, $lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $description, $modifysubmitter ) = $xoopsDB -> fetchRow( $result ) )
            {
                $lookup_lid[$requestid] = $lid;

                $result2 = $xoopsDB -> query( "SELECT cid, title, url, homepage, version, size, platform, logourl, submitter FROM " . $xoopsDB -> prefix( "mydownloads_downloads" ) . " WHERE lid=$lid" );
                $numrows = $xoopsDB -> getRowsNum( $result2 );
                list( $origcid, $origtitle, $origurl, $orighomepage, $origversion, $origsize, $origplatform, $origlogourl, $owner ) = $xoopsDB -> fetchRow( $result2 );

                $result3 = $xoopsDB -> query( "SELECT description FROM " . $xoopsDB -> prefix( "mydownloads_text" ) . " WHERE lid=$lid" );
                list( $origdescription ) = $xoopsDB -> fetchRow( $result3 );
                $result7 = $xoopsDB -> query( "SELECT uname, email FROM " . $xoopsDB -> prefix( "users" ) . " WHERE uid=$modifysubmitter" );
                $result8 = $xoopsDB -> query( "SELECT uname, email FROM " . $xoopsDB -> prefix( "users" ) . " WHERE uid=$owner" );

                $cidtitle = $mytree -> getPathFromId( $cid, "title" );
                $origcidtitle = $mytree -> getPathFromId( $origcid, "title" );
                list( $submittername, $submitteremail ) = $xoopsDB -> fetchRow( $result7 );
                list( $ownername, $owneremail ) = $xoopsDB -> fetchRow( $result8 );
                
				$title = $myts -> htmlSpecialChars( $title );
                $url = $myts -> htmlSpecialChars( $url );
                $homepage = $myts -> htmlSpecialChars( $homepage );
				$version = $myts -> htmlSpecialChars( $version );
                $size = $myts -> htmlSpecialChars( $size );
                $platform = $myts -> htmlSpecialChars( $platform ); 
                $description = $myts -> htmlSpecialChars( $description );
				
				// use original image file to prevent users from changing screen shots file
                $origlogourl = $myts -> htmlSpecialChars( $origlogourl );
                $logourl = $origlogourl;
                $description = $myts -> htmlSpecialChars( $description );
                
				$origurl = $myts -> htmlSpecialChars( $origurl );
                $orighomepage = $myts -> htmlSpecialChars( $orighomepage );
                $origversion = $myts -> htmlSpecialChars( $origversion );
                $origsize = $myts -> htmlSpecialChars( $origsize );
                $origplatform = $myts -> htmlSpecialChars( $origplatform );
                $origdescription = $myts -> htmlSpecialChars( $origdescription );

                if ( $numrows )
                {
                    if ( empty( $ownername ) ) $ownername = "administration";

                    if ( $owneremail == "" )
                    {
                        echo "<div align=left><b>" . _AM_OWNER . "</b> $ownername</div>";
                    } 
                    else
                    {
                        echo "<div align=left><b>" . _AM_OWNER . "</b> <a href=mailto:$owneremail>$ownername</a></div>";
                    } 

                    include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";

                    $sform = new XoopsThemeForm( _AM_ORIGINAL, "storyform", "index.php" );
                    $sform -> addElement( new XoopsFormLabel( _AM_FILETITLE, $origtitle ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_DLURL, $origurl ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_CATEGORYC, $origcidtitle ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_HOMEPAGEC, $orighomepage ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_VERSIONC, $origversion ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_FILESIZEC, $origsize ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_PLATFORMC, $origplatform ) );
                    $sform -> addElement( new XoopsFormLabel( _AM_DESCRIPTIONC, $origdescription ) );
                    if ( $xoopsModuleConfig['useshots'] && !empty( $origlogourl ) )
                    {
                        $sform -> addElement( new XoopsFormLabel( _AM_SHOTIMAGE, "<img src=\"" . XOOPS_URL . "/modules/mydownloads/images/shots/" . $origlogourl . "\" width=\"" . $xoopsModuleConfig['shotwidth'] . "\">" ) );
                    } 
                    else
                    {
                        $sform -> addElement( new XoopsFormLabel( _AM_SHOTIMAGE, '' ) );
                    } 
                    $sform -> display();
                } 
                else
                {
                    echo "<p><div align=left><b>This file as been deleted from the database and the modification should be ignored.</div></p>";
                } 

                if ( $submitteremail == "" )
                {
                    echo "<div align=left><b>" . _AM_SUBMITTER . "</b> $submittername</div>";
                } 
                else
                {
                    echo "<div align=left><b>" . _AM_SUBMITTER . "</b><a href=mailto:$submitteremail>$submittername</a></div>";
                } 

                include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";
                $sform = new XoopsThemeForm( _AM_PROPOSED, "storyform", "modifications.php" );
                $sform -> addElement( new XoopsFormLabel( _AM_FILETITLE, $title ) );
                $sform -> addElement( new XoopsFormLabel( _AM_DLURL, $url ) );
                $sform -> addElement( new XoopsFormLabel( _AM_CATEGORYC, $cidtitle ) );
                $sform -> addElement( new XoopsFormLabel( _AM_HOMEPAGEC, $homepage ) );
                $sform -> addElement( new XoopsFormLabel( _AM_VERSIONC, $version ) );
                $sform -> addElement( new XoopsFormLabel( _AM_FILESIZEC, $size ) );
                $sform -> addElement( new XoopsFormLabel( _AM_PLATFORMC, $platform ) );
                $sform -> addElement( new XoopsFormLabel( _AM_DESCRIPTIONC, $description ) );
                if ( $xoopsModuleConfig['useshots'] && !empty( $logourl ) )
                {
                    $sform -> addElement( new XoopsFormLabel( _AM_SHOTIMAGE, "<img src=\"" . XOOPS_URL . "/modules/mydownloads/images/shots/" . $logourl . "\" width=\"" . $xoopsModuleConfig['shotwidth'] . "\">" ) );
                } 
                else
                {
                    $sform -> addElement( new XoopsFormLabel( _AM_SHOTIMAGE, '' ) );
                } 
                $button_tray = new XoopsFormElementTray( '', '' );
                $button_tray -> addElement( new XoopsFormHidden( 'requestid', $requestid ) );
                $button_tray -> addElement( new XoopsFormHidden( 'lid', $lookup_lid[$requestid] ) );
                $hidden = new XoopsFormHidden( 'op', 'changeModReq' );
                $button_tray -> addElement( $hidden );
                if ( $numrows )
                {
                    $butt_dup = new XoopsFormButton( '', '', _AM_APPROVE, 'submit' );
                    $butt_dup -> setExtra( 'onclick="this.form.elements.op.value=\'changeModReq\'"' );
                    $button_tray -> addElement( $butt_dup );
                } 
                $butt_dupct2 = new XoopsFormButton( '', '', _AM_IGNORE, 'submit' );
                $butt_dupct2 -> setExtra( 'onclick="this.form.elements.op.value=\'ignoreModReq\'"' );
                $button_tray -> addElement( $butt_dupct2 );
                $sform -> addElement( $button_tray );
                $sform -> display();
            } 
        } 
        else
        {
            echo _AM_NOMODREQ;
        } 

        xoops_cp_footer();
        exit();
        break;

    case "changeModReq":
        global $xoopsDB, $HTTP_POST_VARS, $eh, $myts;
        $query = "SELECT lid, cid, title, url, homepage, version, size, platform, logourl, description FROM " . $xoopsDB -> prefix( "mydownloads_mod" ) . " WHERE requestid=" . $HTTP_POST_VARS['requestid'] . "";
        $result = $xoopsDB -> query( $query );
        while ( list( $lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $description ) = $xoopsDB -> fetchRow( $result ) )
        {
            if ( get_magic_quotes_runtime() )
            {
                $title = stripslashes( $title );
                $url = stripslashes( $url );
                $homepage = stripslashes( $homepage );
                $logourl = stripslashes( $logourl );
                $description = stripslashes( $description );
            } 
            $title = addslashes( $title );
            $url = addslashes( $url );
            $homepage = addslashes( $homepage );
            $logourl = addslashes( $logourl );
            $description = addslashes( $description );
            $sql = sprintf( "UPDATE %s SET cid = %u,title = '%s', url = '%s', homepage = '%s', version = '%s', size = %u, platform = '%s', logourl = '%s', status = %u, date = %u WHERE lid = %u", $xoopsDB -> prefix( "mydownloads_downloads" ), $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, 2, time(), $lid );
            $xoopsDB -> query( $sql ) or $eh -> show( "0013" );
            $sql = sprintf( "UPDATE %s SET description = '%s' WHERE lid = %u", $xoopsDB -> prefix( "mydownloads_text" ), $description, $lid );
            $xoopsDB -> query( $sql ) or $eh -> show( "0013" );
            $sql = sprintf( "DELETE FROM %s WHERE requestid = %u", $xoopsDB -> prefix( "mydownloads_mod" ), $HTTP_POST_VARS['requestid'] );
            $xoopsDB -> query( $sql ) or $eh -> show( "0013" );
        } 
        redirect_header( "index.php", 1, _AM_DBUPDATED );
        break;

    case "ignoreModReq":
        global $xoopsDB, $HTTP_POST_VARS, $eh;
        $sql = sprintf( "DELETE FROM %s WHERE requestid = %u", $xoopsDB -> prefix( "mydownloads_mod" ), $HTTP_POST_VARS['requestid'] );
        $xoopsDB -> query( $sql ) or $eh -> show( "0013" );
        redirect_header( "index.php", 1, _AM_MODREQDELETED );
        break;

    case 'main':
    default:

        include_once XOOPS_ROOT_PATH . '/class/pagenav.php';
		include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";
		
		$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_mod"), "requestid", 0);
		
		global $xoopsModuleConfig;
			
        $start = isset( $HTTP_GET_VARS['start'] ) ? intval( $HTTP_GET_VARS['start'] ) : 0;

        global $xoopsDB, $myts, $mytree, $xoopsModuleConfig;
        $sql = "SELECT * FROM " . $xoopsDB -> prefix( "mydownloads_mod" ) . " ORDER BY requestdate DESC" ;
        $result = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'] , $start);
		$totalmodrequests = $xoopsDB -> getRowsNum( $result );

        xoops_cp_header();
        adminmenu( _AM_DLADMIN, 0 );
		echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _AM_USERMODREQ . "</legend>";
        echo "<div style='padding: 8px;'>" . _AM_USERMODREQ . " ($totalmodrequests)</div>";
		echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
        echo "<tr>";
        echo "<th align='center'><b>" . _AM_ID . "</b></th>";
        echo "<th align='left'><b>" . _AM_TITLE . "</b></th>";
        echo "<th align='center'><b>" . _AM_POSTER . "</b></th>";
        echo "<th align='center'><b>" . _AM_DATE . "</b></th>";
		//echo "<th align='center'><b>" . _AM_DOWNLOAD . "</b></th>";
        //echo "<th align='center'><b>" . _AM_SIZE . "</b></th>";
        echo "<th align='center'><b>" . _AM_ACTION . "</b></th>";
        echo "</tr>";

        if ( $totalmodrequests > 0 )
        {
            while (  $down_arr = $xoopsDB->fetchArray($result) )
            {
                $path = $mytree->getNicePathFromId($down_arr['requestid'], "title", "modifications.php?op=listModReqshow&requestid");
				$path = str_replace("/", "", $path);
				$path = str_replace(":", "", trim($path));
				$path = trim($path);
				$title = $path;
                $submitter = xoops_getLinkedUnameFromId( $down_arr['modifysubmitter'] );;
				$requestdate = formatTimestamp($down_arr['requestdate'], $xoopsModuleConfig['dateformat']);
                
				echo "<tr>";
                echo "<td class='head' align='center'>" . $down_arr['requestid'] . "</td>";
                echo "<td class='even' align='left'>" . $title . "</td>";
                echo "<td class='even' align='center'>" . $submitter . "</td>";
                echo "<td class='even' align='center'>" . $requestdate . "</td>";
	            echo "<td class='even' align='center'> <a href='modifications.php?op=listModReqshow&requestid=" . $down_arr['requestid'] . "'>View</a></td>";
                echo "</tr>";
            } 
        } 
        else
        {
            echo "<tr>";
            echo "<td class='head' align='center' colspan= '7'>"._AM_NOMODREQ."</td>";
            echo "</tr>";
        } 
        echo "</table>\n";
        echo "<br /></fieldset>";
		$pagenav = new XoopsPageNav( $totalmodrequests, $xoopsModuleConfig['perpage'], $start, 'start' );
        echo "<div text-align=\"right\">" . $pagenav -> renderNav() . "</div>";
        xoops_cp_footer();
} 

?>