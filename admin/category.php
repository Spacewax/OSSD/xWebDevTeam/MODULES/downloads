<?php 
// $Id: category.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include 'admin_header.php';

$op = '';

if (isset($_POST))
{
    foreach ($_POST as $k => $v)
    {
        ${$k} = $v;
    }
}

if (isset($_GET))
{
    foreach ($_GET as $k => $v)
    {
        ${$k} = $v;
    }
}

function createcat($cid = 0)
{
    global $xoopsDB, $_GET, $myts, $mytree, $xoopsModuleConfig, $totalcats;

    $title = '';
    $imgurl = '';
    $description = '';
    $pid = '';
    $spotlighttop = 0;
    $spotlighthis = 0;
    $heading = _AM_ADDMAIN;
    $totalcats = totalcategory();
    $lid = 0;

    include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';

    if ($cid)
    {
        $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_cat') . " WHERE cid=$cid";
        $cat_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
        $title = $myts->makeTboxData4Edit($cat_arr['title']);
        $imgurl = $myts->makeTboxData4Edit($cat_arr['imgurl']);
        $description = $myts->makeTareaData4Edit($cat_arr['description']);
        $spotlighthis = $myts->makeTareaData4Edit($cat_arr['spotlighthis']);
        $spotlighttop = intval($cat_arr['spotlighttop']);
        $heading = _AM_MODEXISTINGCAT;
    }

    $sform = new XoopsThemeForm($heading, "op", xoops_getenv('PHP_SELF'));
    $sform->setExtra('enctype="multipart/form-data"');
    $gperm_handler = &xoops_gethandler('groupperm');
    $sform->addElement(new XoopsFormText(_AM_TITLEC, 'title', 50, 80, $title), true);
    if ($totalcats > 0)
    {
        $mytreechose = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
        ob_start();
        $mytreechose->makeMySelBox("title", "title", 0 , 1, "pid");
        $sform->addElement(new XoopsFormLabel(_AM_ADDSUBIN, ob_get_contents()));
        ob_end_clean();
    }
    if (!$imgurl) $imgurl = 'blank.png';
    $graph_array = &XoopsLists::getImgListAsArray(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['catimage']);
    $indeximage_select = new XoopsFormSelect('', 'imgurl', $imgurl);
    $indeximage_select->addOptionArray($graph_array);
    $indeximage_select->setExtra("onchange='showImgSelected(\"image\", \"imgurl\", \"" . $xoopsModuleConfig['catimage'] . "\", \"\", \"" . XOOPS_URL . "\")'");
    $indeximage_tray = new XoopsFormElementTray(_AM_IMGURLMAIN, '&nbsp;');
    $indeximage_tray->addElement($indeximage_select);
    $indeximage_tray->addElement(new XoopsFormLabel('', "<br /><br /><img src='" . XOOPS_URL . "/" . $xoopsModuleConfig['catimage'] . "/" . $imgurl . "' name='image' id='image' alt='' />"));
    $sform->addElement($indeximage_tray);
    $sform->addElement(new XoopsFormDhtmlTextArea(_AM_DESCRIPTION, 'description', $description, 15, 60), false);
    $sform->addElement(new XoopsFormHidden('cid', $cid));

    $myspotlight1chose = new XoopsTree($xoopsDB->prefix("mydownloads_downloads"), "lid", 0);
    ob_start();
    $myspotlight1chose->makeMySelBox("title", "title", $spotlighthis , 1, "lid");
    $sform->addElement(new XoopsFormLabel(_AM_SPOTLIGHTTHIS, ob_get_contents()));
    ob_end_clean();

    $spotlighttop_radio = new XoopsFormRadioYN(_AM_SPOTLIGHTTOP, 'spotlighttop', $spotlighttop , ' ' . _AM_YES . '', ' ' . _AM_NO . '');
    $sform->addElement($spotlighttop_radio);

    $button_tray = new XoopsFormElementTray('', '');
    $hidden = new XoopsFormHidden('op', 'addCat');
    $button_tray->addElement($hidden);

    $butt_dup = new XoopsFormButton('', '', _AM_SAVE, 'submit');
    $butt_dup->setExtra('onclick="this.form.elements.op.value=\'addCat\'"');
    $button_tray->addElement($butt_dup);

    $butt_dupct = new XoopsFormButton('', '', _AM_DELETE, 'submit');
    $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'delCat\'"');
    $button_tray->addElement($butt_dupct);
    $sform->addElement($button_tray);
    $sform->display();

    $result2 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_cat") . "");
    list($numrows) = $xoopsDB->fetchRow($result2);
}

if (!isset($_POST['op']))
{
    $op = isset($_GET['op']) ? $_GET['op'] : 'main';
}
else
{
    $op = isset($_POST['op']) ? $_POST['op'] : 'main';
}

switch ($op)
{
    case "move":
        if (!isset($_POST['ok']))
        {
            $cid = (isset($_POST['cid'])) ? $_POST['cid'] : $_GET['cid'];

            xoops_cp_header();
            adminmenu(_AM_DLADMIN, 0);

            include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';
            $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
            $sform = new XoopsThemeForm(_AM_MOVECATEGORY, "move", xoops_getenv('PHP_SELF'));
            ob_start();
            $mytree->makeMySelBox("title", "title", 0 , 1, "target");
            $sform->addElement(new XoopsFormLabel(_AM_MOVEARTICLES, ob_get_contents()));
            ob_end_clean();
            $create_tray = new XoopsFormElementTray('', '');
            $create_tray->addElement(new XoopsFormHidden('source', $cid));
            $create_tray->addElement(new XoopsFormHidden('ok', 1));
            $create_tray->addElement(new XoopsFormHidden('op', 'move'));
            $butt_save = new XoopsFormButton('', '', _AM_MOVE, 'submit');
            $butt_save->setExtra('onclick="this.form.elements.op.value=\'move\'"');
            $create_tray->addElement($butt_save);
            $butt_cancel = new XoopsFormButton('', '', _AM_CANCEL, 'submit');
            $butt_cancel->setExtra('onclick="this.form.elements.op.value=\'cancel\'"');
            $create_tray->addElement($butt_cancel);
            $sform->addElement($create_tray);
            $sform->display();
            xoops_cp_footer();
        }
        else
        {
            global $xoopsDB, $xooopsConfig;

            $source = $_POST['source'];
            $target = $_POST['target'];
            if ($target == $source)
            {
                redirect_header("category.php?op=move&ok=0&cid=$source", 5, _AM_MOVEFAILTAREQUALSOU);
            }

            if (!$target)
            {
                redirect_header("category.php?op=move&ok=0&cid=$source", 5, _AM_MOVEFAILNOTARGET);
            }
            $xoopsDB->queryF("UPDATE " . $xoopsDB->prefix("mydownloads_downloads") . " set cid = " . $target . " WHERE cid =" . $source . "");
            $xoopsDB->queryF("UPDATE " . $xoopsDB->prefix("mydownloads_text") . " set cid = " . $target . " WHERE cid =" . $source . "");
            redirect_header("category.php?op=default", 5, _AM_DELETEARTICLEMOVED);
            exit();
        }
        break;

    case "addCat":

        global $xoopsDB, $_POST, $myts, $HTTP_POST_FILES, $xoopsModuleConfig;

        $cid = (isset($_POST["cid"])) ? $_POST["cid"] : 0;
        $pid = (isset($_POST["pid"])) ? $_POST["pid"] : 0;
        $spotlighthis = (isset($_POST["lid"])) ? $_POST["lid"] : 0;
        $spotlighttop = ($_POST["spotlighttop"] == 1) ? 1 : 0;
        $title = $myts->makeTboxData4Save($_POST["title"]);
        $description = $myts->makeTareaData4Save($_POST["description"]);
        $imgurl = ($_POST["imgurl"] && $_POST["imgurl"] != "blank.png") ? $myts->makeTboxData4Save($_POST["imgurl"]) : "";

        if (!$cid)
        {
            $newid = $xoopsDB->genId($xoopsDB->prefix("mydownloads_cat") . "_cid_seq");
            $sql = "INSERT INTO " . $xoopsDB->prefix("mydownloads_cat") . " (cid, pid, title, imgurl, description, spotlighttop, spotlighthis) VALUES ($newid, $pid, '$title', '$imgurl', '$description', $spotlighttop, $spotlighthis)";
            $xoopsDB->query($sql);

            if ($newid == 0) $newid = $xoopsDB->getInsertId(); 
            // Notify of new category
            global $xoopsModule;
            $tags = array();
            $tags['CATEGORY_NAME'] = $title;
            $tags['CATEGORY_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/viewcat.php?cid=' . $newid;
            $notification_handler = &xoops_gethandler('notification');
            $notification_handler->triggerEvent('global', 0, 'new_category', $tags);
            redirect_header("category.php", 1, _AM_NEWCATADDED);
        }
        else
        {
            $sql = $xoopsDB->queryF("UPDATE " . $xoopsDB->prefix("mydownloads_cat") . " SET title ='$title', imgurl = '$imgurl', pid =$pid, description = '$description', spotlighthis = '$spotlighthis' , spotlighttop = '$spotlighttop' WHERE cid = '$cid'");
            $xoopsDB->query($sql);
            redirect_header("category.php", 1, _AM_DBUPDATED);
        }
        break;

    case "del":
        
		global $xoopsDB, $_GET, $_POST, $mytree, $xoopsModule;
        $cid = isset($_POST['cid']) ? intval($_POST['cid']) : intval($_GET['cid']);
        $ok = isset($_POST['ok']) ? intval($_POST['ok']) : 0;
		$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
		
        if ($ok)
        { 
            // get all subcategories under the specified category
            $arr = $mytree->getAllChildId($cid);
            $lcount = count($arr);
			
			for ($i = 0; $i < $lcount; $i++)
            { 
                // get all downloads in each subcategory
                $result = $xoopsDB->query("SELECT lid FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE cid=" . $arr[$i] . ""); 
                // now for each download, delete the text data and vote ata associated with the download
                while (list($lid) = $xoopsDB->fetchRow($result))
                {
                    $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_text"), $lid);
                    $xoopsDB->query($sql);
                    $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_votedata"), $lid);
                    $xoopsDB->query($sql);
                    $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_downloads"), $lid);
                    $xoopsDB->query($sql); 
                    // delete comments
                    xoops_comment_delete($xoopsModule->getVar('mid'), $lid);
                } 
                // all downloads for each subcategory is deleted, now delete the subcategory data
                $sql = sprintf("DELETE FROM %s WHERE cid = %u", $xoopsDB->prefix("mydownloads_cat"), $arr[$i]);
                $xoopsDB->query($sql);
            } 
            // all subcategory and associated data are deleted, now delete category data and its associated data
            $result = $xoopsDB->query("SELECT lid FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE cid=" . $cid . "");
            while (list($lid) = $xoopsDB->fetchRow($result))
            {
                $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_downloads"), $lid);
                $xoopsDB->query($sql);
                $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_text"), $lid);
                $xoopsDB->query($sql); 
                // delete comments
                xoops_comment_delete($xoopsModule->getVar('mid'), $lid);
                $sql = sprintf("DELETE FROM %s WHERE lid = %u", $xoopsDB->prefix("mydownloads_votedata"), $lid);
                $xoopsDB->query($sql);
            }
            $sql = sprintf("DELETE FROM %s WHERE cid = %u", $xoopsDB->prefix("mydownloads_cat"), $cid);
            if (!$result = $xoopsDB->query($sql))
            {
                trigger_error($error, E_USER_ERROR);
            }
            redirect_header("category.php", 1, _AM_CATDELETED);
            exit();
        }
        else
        {
            xoops_cp_header();
            xoops_confirm(array('op' => 'del', 'cid' => $cid, 'ok' => 1), 'category.php', _AM_WARNING);
            xoops_cp_footer();
        }
        exit();
        break;

    case "modCat":
        $cid = (isset($_POST['cid'])) ? $_POST['cid'] : 0;
        xoops_cp_header();
        adminmenu(_AM_DLADMIN, 0);
        createcat($cid);
        xoops_cp_footer();
        break;
    case "modCatS":
        modCatS();
        break;

    case 'main':
    default:

        Global $xoopsUser, $xoopsDB, $xoopsConfig, $_GET;

        $cid = (isset($_GET['cid'])) ? $_GET['cid'] : 0;

        xoops_cp_header();
        adminmenu(_AM_DLADMIN, 0);

        include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';
        $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
        $sform = new XoopsThemeForm(_AM_MODIFY, "category", xoops_getenv('PHP_SELF'));
        $totalcats = totalcategory();

        if ($totalcats > 0)
        {
            ob_start();
            $sform->addElement(new XoopsFormHidden('cid', ''));
            $mytree->makeMySelBox("title", "title");
            $sform->addElement(new XoopsFormLabel(_AM_MODEXISTINGCAT, ob_get_contents()));
            ob_end_clean();
            $dup_tray = new XoopsFormElementTray('', '');
            $dup_tray->addElement(new XoopsFormHidden('op', 'modCat'));
            $butt_dup = new XoopsFormButton('', '', _AM_MODIFY, 'submit');
            $butt_dup->setExtra('onclick="this.form.elements.op.value=\'modCat\'"');
            $dup_tray->addElement($butt_dup);
            $butt_move = new XoopsFormButton('', '', _AM_MOVEDEL, 'submit');
            $butt_move->setExtra('onclick="this.form.elements.op.value=\'move\'"');
            $dup_tray->addElement($butt_move);
            $butt_dupct = new XoopsFormButton('', '', _DELETE, 'submit');
            $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'del\'"');
            $dup_tray->addElement($butt_dupct);
            $sform->addElement($dup_tray);
            $sform->display();
        }
        createcat(0);
        xoops_cp_footer();
        break;
}

?>
