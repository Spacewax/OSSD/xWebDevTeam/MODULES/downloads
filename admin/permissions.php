<?php
include( "admin_header.php" );
include_once XOOPS_ROOT_PATH . '/class/xoopsform/grouppermform.php';

$op = '';

foreach ( $HTTP_POST_VARS as $k => $v )
{
    ${$k} = $v;
} 

foreach ( $HTTP_GET_VARS as $k => $v )
{
    ${$k} = $v;
} 

switch ( $op )
{
    case "category":
        global $xoopsDB, $xoopsModule;

        $item_list = array();
        $block = array();

        xoops_cp_header();

        adminmenu( _AM_DLADMIN, $extra = '' );
        echo '<h4 align="left">Category Permissions</h4>';

        $form = new XoopsGroupPermForm( 'Permission form for category', $xoopsModule -> getVar( 'mid' ), 'DownCatPerm', 'Select categories that each group is allowed to view' );

        $result = $xoopsDB -> query( "SELECT cid, pid, title FROM " . $xoopsDB -> prefix( "mydownloads_cat" ) . " Order by title" );
        if ( $xoopsDB -> getRowsNum( $result ) )
        {
            while ( list( $cid, $pid, $title) = $xoopsDB -> fetchrow( $result ) )
            {
                $form -> addItem( $cid, $title, $pid );
            } 
            echo $form -> render();
        } 
        else
        {
            echo '<p><div align="left"><b>Cannot set permission\'s: No Categories\'s created yet!</b></div></p>';
        } 
        break;

    case "download":
        global $xoopsDB, $xoopsModule;

        $item_lists = array();
        $block = array();

        xoops_cp_header();
        adminmenu( _AM_DLADMIN, $extra = '' );
        echo '<h4 align="left">Download Permissions</h4>';

        $result = $xoopsDB -> query( "SELECT lid, title FROM " . $xoopsDB -> prefix( "mydownloads_downloads" ) . "" );;
        if ( $xoopsDB -> getRowsNum( $result ) )
        {
            while ( $myrow = $xoopsDB -> fetcharray( $result ) )
            {
                $item_lists['cid'] = $myrow['lid'];
                $item_lists['title'] = $myrow['title'];
                $form2 = new XoopsGroupPermForm( 'Permission form for downloads', $xoopsModule -> getVar( 'mid' ), 'DownFilePerm', 'Select Downloads that each group is allowed to view' );
                $block[] = $item_lists;
                foreach ( $block as $itemlists )
                {
                    $form2 -> addItem( $itemlists['cid'], $itemlists['title'], 0 );
                } 
            } 
            echo $form2 -> render();
        } 
        else
        {
            echo '<p><div align="left"><b>Cannot set permission\'s: No Downloads\'s created yet!</b></div></p>';
        } 
        break;

    case "default":
    default:

        xoops_cp_header();
        adminmenu( _AM_DLADMIN, $extra = '' );

        echo "<p><div><b>" . _AM_SELECTPERMISSIONTYPE . "</b></div></p>";
        echo "<div><a href='permissions.php?op=category'>" . _AM_MODIFYCATPERMISSIONS . "</a></div>";
        echo "<div><a href='permissions.php?op=download'>" . _AM_MODIFYDWNPERMISSIONS . "</a></div>";
} 
xoops_cp_footer();

?>