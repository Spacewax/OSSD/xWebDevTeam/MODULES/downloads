<?php 
// $Id: newdownloads.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include 'admin_header.php';

if (isset($_POST))
{
    foreach ($_POST as $k => $v)
    {
        $$k = $v;
    } 
} 

if (isset($_GET))
{
    foreach ($_GET as $k => $v)
    {
        $$k = $v;
    } 
} 

if (!isset($_POST['op']))
{
    $op = isset($_GET['op']) ? $_GET['op'] : 'main';
} 
else
{
    $op = $_POST['op'];
} 

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");

switch ($op)
{
    case "delNewDownload":
        global $xoopsDB, $_GET, $eh, $xoopsModule, $xoopsModuleConfig;

        $file = XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . basename($_POST['url']);
        if (is_file($file))
        {
            @unlink($file);
        } 
        $xoopsDB->query("DELETE FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid = " . $_POST['lid'] . "");
        redirect_header("newdownloads.php", 1, _AM_FILEDELETED);
        break;

    case "approve":
        global $xoopsDB, $xoopsUser, $xoopsModule, $_POST, $myts, $eh, $xoopsModuleConfig;

        $lid = $_POST['lid'];
        $cid = ($_POST['cid']) ? $_POST['cid'] : 0;
        $status = (!empty($_POST['status'])) ? $_POST['status'] : 1;

        $url = (($_POST["url"]) || ($_POST["url"] != "")) ? $myts->addslashes($_POST["url"]) : '';
        $logourl = ($_POST["logourl"] != "blank.png") ? $myts->addslashes($_POST["logourl"]) : '';
        $title = $myts->addslashes($_POST['title']);
        $homepage = ($_POST["homepage"] != "http://") ? $myts->addslashes(formatURL($_POST["homepage"])) : '';
        $homepagetitle = $myts->addslashes($_POST["homepagetitle"]);
        $version = trim($_POST["version"]);
        $version = (intval($version)) ? $myts->addslashes(intval($version)) : 0;
        $size = ((empty($size) || !is_numeric($size))) ? $myts->addslashes($_POST["size"]) : 0;
        $platform = $myts->addslashes($_POST["platform"]);
        $description = $myts->addslashes($_POST['description']);

        $submitter = $myts->addslashes($_POST[""]);
        $price = $myts->addslashes($_POST["price"]);
        $mirror = $myts->addslashes(formatURL($_POST["mirror"]));
        $license = $myts->addslashes($_POST["license"]);
        $paypalemail = $myts->addslashes($_POST["paypalemail"]);
        $features = $myts->addslashes($_POST["features"]);
        $requirements = $myts->addslashes($_POST["requirements"]);
        $forumid = $myts->addslashes($_POST["forumid"]);

        $updated = ($_POST['up_dated'] == 0) ? 0 : time();
        $offline = ($_POST['offline'] == 0) ? 0 : 1;

        $publishdate = time();

        if ($_POST['publishdateactivate'])
        {
            $publishdate = strtotime($_POST['published']['date']) + $_POST['published']['time'];
        } 
        if ($_POST['clearpublish'])
        {
            $result = $xoopsDB->query("SELECT date FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid");
            list($date) = $xoopsDB->fetchRow($result);
            $publishdate = $date;
        } 

        if ($_POST['expiredateactivate'])
        {
            $expiredate = strtotime($_POST['expired']['date']) + $_POST['expired']['time'];
        } 
        if ($_POST['clearexpire'])
        {
            $expiredate = '0';
        } 
        /**
         * Update the database
         */
        $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_downloads") . " SET cid = $cid, title = '$title', 
			url = '$url', mirror = '$mirror', paypalemail = '$paypalemail', license = '$license', 
			features = '$features', homepage = '$homepage', version = '$version', size = $size, platform = '$platform',
			logourl = '$logourl', status = '$status', price = '$price', requirements = '$requirements', 
			homepagetitle = '$homepagetitle', forumid = '$forumid', limitations = '$limitations', dhistory = '$dhistory', published = '$publishdate', 
			expired = '$expiredate', updated = '$updated', offline = '$offline' 
			WHERE lid = $lid");
        $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_text") . " SET description = '$description' WHERE lid = '$lid'");

        global $xoopsModule;
        $tags = array();
        $tags['FILE_NAME'] = $title;
        $tags['FILE_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/singlefile.php?cid=' . $cid . '&amp;lid=' . $lid;
        $sql = "SELECT title FROM " . $xoopsDB->prefix('mydownloads_cat') . " WHERE cid=" . $cid;
        $result = $xoopsDB->query($sql);
        $row = $xoopsDB->fetchArray($result);
        $tags['CATEGORY_NAME'] = $row['title'];
        $tags['CATEGORY_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/viewcat.php?cid=' . $cid;
        $notification_handler = &xoops_gethandler('notification');
        $notification_handler->triggerEvent('global', 0, 'new_file', $tags);
        $notification_handler->triggerEvent('category', $cid, 'new_file', $tags);
        $notification_handler->triggerEvent('file', $lid, 'approve', $tags);
        redirect_header("newdownloads.php", 1, _AM_NEWDLADDED);
        break;
    /**
     * List downloads waiting for validation
     */
    case 'edit':

        include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';

        global $xoopsDB, $myts, $mytree, $xoopsModuleConfig;

        $cid = 0;
        $title = '';
        $url = 'http://';
        $homepage = 'http://';
        $version = '';
        $size = 0;
        $platform = '';
        $logourl = '';
        $price = 0;
        $description = '';
        $mirror = 'http://';
        $license = '';
        $paypalemail = '';
        $features = '';
        $requirements = '';
        $forumid = 0;
        $limitations = '';
        $dhistory = '';
        $status = 0;
        $is_updated = 0;
        $offline = 0;
		$directory = $xoopsModuleConfig['screenshots'];
		
        $lid = (isset($_GET['lid'])) ? $_GET['lid'] : 0;

        $result = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status = 0");
        list($numrows) = $xoopsDB->fetchRow($result);
        unset ($result);

        $result = $xoopsDB->query("SELECT cid, title, url, homepage, version, size, platform, 
				logourl, submitter, status, license, mirror, price, paypalemail, features, requirements, homepagetitle,
				forumid, limitations, dhistory, published, expired, updated, offline   
				FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid");
        list($cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $submitter, $status, $license,
            $mirror, $price, $paypalemail, $features, $requirements, $homepagetitle, $forumid,
            $limitations, $dhistory, $published, $expired, $updated, $offline) = $xoopsDB->fetchRow($result);

        $result2 = $xoopsDB->query("SELECT description FROM " . $xoopsDB->prefix("mydownloads_text") . " WHERE lid=$lid");
        list($description) = $xoopsDB->fetchRow($result2);

        $title = $myts->makeTboxData4Edit($title);
        $url = $myts->makeTboxData4Edit($url);
        $homepage = $myts->makeTboxData4Edit($homepage);
        $homepagetitle = $myts->makeTboxData4Edit($homepagetitle);
        $version = $myts->makeTboxData4Edit($version);
        $size = $myts->makeTboxData4Edit($size);
        $platform = $myts->makeTboxData4Edit($platform);
        $logourl = $myts->makeTboxData4Edit($logourl);
        $price = $myts->makeTboxData4Edit($price);
        $description = $myts->makeTareaData4Edit($description);
        $mirror = $myts->makeTboxData4Edit($mirror);
        $license = $myts->makeTboxData4Edit($license);
        $paypalemail = $myts->makeTboxData4Edit($paypalemail);
        $features = $myts->makeTboxData4Edit($features);
        $requirements = $myts->makeTareaData4Edit($requirements);
        $limitations = $myts->makeTareaData4Edit($limitations);
        $dhistory = $myts->makeTareaData4Edit($dhistory);

        xoops_cp_header();
        adminmenu(_AM_DLCONF, 0);
        echo "<h4>" . _AM_DLSWAITING . "&nbsp;($numrows)</h4>";

        $sform = new XoopsThemeForm(_AM_SUBMITCATHEAD, "storyform", xoops_getenv('PHP_SELF'));
        $sform->addElement(new XoopsFormLabel(_AM_FILEID, "No: " . $lid));
        $submitter = xoops_getLinkedUnameFromId($submitter);
        $sform->addElement(new XoopsFormLabel(_AM_SUBMITTER, $submitter));
        $sform->addElement(new XoopsFormHidden('submitter', $submitter));

        $sform->addElement(new XoopsFormText(_AM_FILETITLE, 'title', 50, 80, $title), true);
        $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat "), "cid ", "pid");
        $sform->addElement(new XoopsFormText(_AM_DLURL, 'url', 50, 80, $url), false);
        $sform->addElement(new XoopsFormText(_AM_MIRROR, 'mirror', 50, 80, $mirror), false);

        $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
        ob_start();
        $sform->addElement(new XoopsFormHidden('cid', $cid));
        $mytree->makeMySelBox('title', 'title', $cid, 0);
        $sform->addElement(new XoopsFormLabel(_AM_CATEGORYC, ob_get_contents()));
        ob_end_clean();

        $sform->addElement(new XoopsFormText(_AM_HOMEPAGETITLEC, 'homepagetitle', 50, 80, $homepagetitle), false);
        $sform->addElement(new XoopsFormText(_AM_HOMEPAGEC, 'homepage', 50, 80, $homepage), false);
        $sform->addElement(new XoopsFormText(_AM_VERSIONC, 'version', 10, 20, $version), false);
        $sform->addElement(new XoopsFormText(_AM_FILESIZEC, 'size', 10, 20, $size), false);

        $platform_array = $xoopsModuleConfig['platform'];
        $platform_select = new XoopsFormSelect('', 'platform', $platform, '', '', 0);
        $platform_select->addOptionArray($platform_array);
        $platform_tray = new XoopsFormElementTray(_AM_PLATFORMC, '&nbsp;');
        $platform_tray->addElement($platform_select);
        $sform->addElement($platform_tray);

        $license_array = $xoopsModuleConfig['license'];
        $license_select = new XoopsFormSelect('', 'license', $license, '', '', 0);
        $license_select->addOptionArray($license_array);
        $license_tray = new XoopsFormElementTray(_AM_LICENCEC, '&nbsp;');
        $license_tray->addElement($license_select);
        $sform->addElement($license_tray);

        $limitations_array = $xoopsModuleConfig['limitations'];
        $limitations_select = new XoopsFormSelect('', 'limitations', $limitations, '', '', 0);
        $limitations_select->addOptionArray($limitations_array);
        $limitations_tray = new XoopsFormElementTray(_AM_LIMITATIONS, '&nbsp;');
        $limitations_tray->addElement($limitations_select);
        $sform->addElement($limitations_tray);

        $sform->addElement(new XoopsFormText(_AM_PRICEC, 'price', 10, 20, $price), false);
        $sform->addElement(new XoopsFormText(_AM_PAYPALEMAILC, 'paypalemail', 50, 80, $paypalemail), false);

        $sform->addElement(new XoopsFormDhtmlTextArea(_AM_DESCRIPTION, 'description', $description, 15, 60), true);
        $sform->addElement(new XoopsFormTextArea(_AM_KEYFEATURESC, 'features', $features, 7, 60), false);
        $sform->addElement(new XoopsFormTextArea(_AM_REQUIREMENTSC, 'requirements', $requirements, 7, 60), false);
        $sform->addElement(new XoopsFormTextArea(_AM_HISTORYC, 'dhistory', $dhistory, 7, 60), false);

        if (!$logourl) $logourl = "blank.png";
        $graph_array = &XoopsLists::getImgListAsArray(XOOPS_ROOT_PATH . "/modules/mydownloads/images/shots");
        $indeximage_select = new XoopsFormSelect('', 'logourl', $logourl, '', '', 1);
        $indeximage_select->addOptionArray($graph_array);
        $indeximage_select->setExtra("onchange='showImgSelected(\"image3\", \"logourl\", \"/modules/mydownloads/images/shots\", \"\", \"" . XOOPS_URL . "\")'");
        $indeximage_tray = new XoopsFormElementTray(_AM_SHOTIMAGE, '&nbsp;');
        $indeximage_tray->addElement($indeximage_select);
        $indeximage_tray->addElement(new XoopsFormLabel('', "<br /><br /><img src='" . XOOPS_URL . "/modules/mydownloads/images/shots/" . $logourl . "' name='image3' id='image3' alt='' />"));
        $sform->addElement($indeximage_tray);
        
		$sform->insertBreak(sprintf(_AM_MUSTBEVALID, "<b>" . $directory . "</b>"), "even");
        
		ob_start();
        getforum($forumid);
        $sform->addElement(new XoopsFormLabel(_AM_EDITDISCUSSINFORUM, ob_get_contents()));
        ob_end_clean();

        $sform->insertBreak("<b>" . _AM_MENU . "</b>", 'bg3');

         $publishtext = (!$lid && !$published) ? _AM_SETPUBLISHDATE : _AM_SETNEWPUBLISHDATE;
        if ($published > time()) {
            $publishtext = _AM_SETPUBDATESETS;
        } 
        $ispublished = ($published > time()) ? 1 : 0 ;
        $publishdates = ($published > time()) ? _AM_PUBLISHDATESET . formatTimestamp($published, "Y-m-d H:s") : _AM_SETDATETIMEPUBLISH;
        $publishdate_checkbox = new XoopsFormCheckBox('', 'publishdateactivate', $ispublished);
        $publishdate_checkbox->addOption(1, $publishdates . "<br /><br />");

        if ($lid) {
            $sform->addElement(new XoopsFormHidden('was_published', $published));
            $sform->addElement(new XoopsFormHidden('was_expired', $expired));
        } 

        $publishdate_tray = new XoopsFormElementTray(_AM_PUBLISHDATE, '');
        $publishdate_tray->addElement($publishdate_checkbox);
        $publishdate_tray->addElement(new XoopsFormDateTime($publishtext, 'published', 15, $published));
        $publishdate_tray->addElement(new XoopsFormRadioYN(_AM_CLEARPUBLISHDATE, 'clearpublish', 0, ' ' . _AM_YES . '', ' ' . _AM_NO . ''));
        $sform->addElement($publishdate_tray);

        $isexpired = ($expired > time()) ? 1: 0 ;
        $expiredates = ($expired > time()) ? _AM_EXPIREDATESET . formatTimestamp($expired, 'Y-m-d H:s') : _AM_SETDATETIMEEXPIRE;
        $warning = ($published > $expired && $expired > time()) ? _AM_EXPIREWARNING : '';
        $expiredate_checkbox = new XoopsFormCheckBox('', 'expiredateactivate', $isexpired);
        $expiredate_checkbox->addOption(1, $expiredates . "<br /><br />");

        $expiredate_tray = new XoopsFormElementTray(_AM_EXPIREDATE . $warning, '');
        $expiredate_tray->addElement($expiredate_checkbox);
        $expiredate_tray->addElement(new XoopsFormDateTime(_AM_SETEXPIREDATE . "<br />", 'expired', 15, $expired));
        $expiredate_tray->addElement(new XoopsFormRadioYN(_AM_CLEAREXPIREDATE, 'clearexpire', 0, ' ' . _AM_YES . '', ' ' . _AM_NO . ''));
        $sform->addElement($expiredate_tray);

        $filestatus_radio = new XoopsFormRadioYN(_AM_FILESSTATUS, 'status', $status, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($filestatus_radio);

        $up_dated = ($updated == 0) ? 0 : 1;
        $file_updated_radio = new XoopsFormRadioYN(_AM_SETASUPDATED, 'up_dated', $up_dated, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($file_updated_radio);

        $button_tray = new XoopsFormElementTray('', '');
        $button_tray->addElement(new XoopsFormHidden('lid', $lid));
        $hidden = new XoopsFormHidden('op', 'approve');
        $button_tray->addElement($hidden);

        $butt_dup = new XoopsFormButton('', '', _AM_APPROVE, 'submit');
        $butt_dup->setExtra('onclick="this.form.elements.op.value=\'approve\'"');
        $button_tray->addElement($butt_dup);

        $butt_dupct = new XoopsFormButton('', '', _AM_DELETE, 'submit');
        $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'delNewDownload\'"');
        $button_tray->addElement($butt_dupct);

        $butt_dupct2 = new XoopsFormButton('', '', _CANCEL, 'submit');
        $butt_dupct2->setExtra('onclick="this.form.elements.op.value=\'index.php\'"');
        $button_tray->addElement($butt_dupct2);
        $sform->addElement($button_tray);
        $sform->display();
        unset($hidden);
        xoops_cp_footer();
        break; 
    // List downloads waiting for validation
    case 'main':
    default:

        include_once XOOPS_ROOT_PATH . '/class/pagenav.php';
        global $xoopsDB, $myts, $xoopsModuleConfig, $imagearray;

        $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
        $sql = "SELECT lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits , rating, votes, comments   FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status = 0 OR published = 0 ORDER BY lid DESC";
        $result = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'] , $start);
        $sql2 = "SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status = 0 OR published = 0 ORDER BY lid DESC";
        $result2 = $xoopsDB->query($sql2);
		$num = $xoopsDB->getRowsNum($result2);

        xoops_cp_header();
        adminmenu(_AM_DLCONF, 0);
        echo "<h4>" . _AM_DLSWAITING . "&nbsp;($num)</h4>";
        echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
        echo "<tr>";
        echo "<td class='bg3' align='center' width = '3%'><b>" . _AM_ID . "</b></td>";
        echo "<td class='bg3' align='left' width = '30%'><b>" . _AM_TITLE . "</b></td>";
        echo "<td class='bg3' align='center' width = '15%'><b>" . _AM_POSTER . "</b></td>";
        echo "<td class='bg3' align='center' width = '15%'><b>" . _AM_SUBMITDATE . "</b></td>";
        echo "<td class='bg3' align='center' width = '7%'><b>" . _AM_ACTION . "</b></td>";
        echo "</tr>";
        if ($num > 0)
        {
            while (list($lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $submitter, $status, $date, $hits, $rating, $votes, $comments) = $xoopsDB->fetchrow($result))
            {
                $rating = number_format($rating, 2);
                $title = $myts->htmlSpecialChars($title);
                $url = $myts->htmlSpecialChars($url);
                $url = urldecode($url);
                $homepage = $myts->htmlSpecialChars($homepage);
                $version = $myts->htmlSpecialChars($version);
                $size = $myts->htmlSpecialChars($size);
                $platform = $myts->htmlSpecialChars($platform);
                $logourl = $myts->htmlSpecialChars($logourl);
                $submitter = xoops_getLinkedUnameFromId($submitter);
                $datetime = formatTimestamp($date, $xoopsModuleConfig['dateformat']);
                $status = ($status) ? $approved : "<a href='newdownloads.php?op=edit&lid=" . $lid . "'>" . $imagearray['approve'] . "</a>";
                $modify = "<a href='newdownloads.php?op=edit&lid=" . $lid . "'>" . $imagearray['editimg'] . "</a>";
                $delete = "<a href='index.php?op=delNewDownload&lid=" . $lid . "'>" . $imagearray['deleteimg'] . "</a>";

                echo "<tr>";
                echo "<td class='head' align='center'>" . $lid . "</td>";
                echo "<td class='even' align='left' nowrap><a href='newdownloads.php?op=edit&lid=" . $lid . "'>" . $title . "</a></td>";
                echo "<td class='even' align='center' nowrap>$submitter</td>";
                echo "<td class='even' align='center'>" . $datetime . "</td>";
                echo "<td class='even' align='center'  nowrap>$status $modify $delete</td>";
                echo "</tr>";
            } 
        } 
        else
        {
            echo "<tr ><td align = 'center' class='head' colspan = '6'>" . _AM_NOWAITING . "</td></tr>";
        } 
        echo "</table>\n";
        $pagenav = new XoopsPageNav($num, $xoopsModuleConfig['perpage'] , $start, 'start');
        echo '<div text-align="right">' . $pagenav->renderNav() . '</div>';
        xoops_cp_footer();
        break;
} 

?>