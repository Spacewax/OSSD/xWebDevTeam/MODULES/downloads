<?php 
// $Id: index.php,v 1.2 2004/06/05 09:05:03 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include 'admin_header.php';
include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");

function Download()
{
    global $xoopsDB, $HTTP_GET_VARS, $HTTP_POST_VARS, $myts, $mytree, $xoopsModuleConfig;

    $cid = 0;
    $title = '';
    $url = 'http://';
    $homepage = 'http://';
    $homepagetitle = '';
    $version = '';
    $size = 0;
    $platform = '';
    $logourl = '';
    $price = 0;
    $description = '';
    $mirror = 'http://';
    $license = '';
    $paypalemail = '';
    $features = '';
    $requirements = '';
    $forumid = 0;
    $limitations = '';
    $dhistory = '';
    $status = 0;
    $is_updated = 0;
    $offline = 0;
    $published = 0;
    $expired = 0;
    $updated = 0;
    $versiontypes = '';

    if (isset($HTTP_POST_VARS['lid']))
    {
        $lid = $HTTP_POST_VARS['lid'];
    }elseif (isset($HTTP_GET_VARS['lid']))
    {
        $lid = $HTTP_GET_VARS['lid'];
    }
    else
    {
        $lid = 0;
    }
    $directory = $xoopsModuleConfig['screenshots'];

    $result = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_cat") . "");
    list($numrows) = $xoopsDB->fetchRow($result);

    $down_array = '';

    if ($numrows)
    {
        xoops_cp_header();

        adminmenu(_AM_DLADMIN, 0);
        if ($lid)
        {
            $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE lid=" . $lid . "";
            $down_array = $xoopsDB->fetchArray($xoopsDB->query($sql));

            $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_text') . " WHERE lid = " . $down_array['lid'] . "";

            $down_arr_text = $xoopsDB->fetcharray($xoopsDB->query($sql));
            $down_array['description'] = $down_arr_text['description'];

            $lid = $myts->makeTboxData4Edit($down_array['lid']);
            $cid = $myts->makeTboxData4Edit($down_array['cid']);
            $title = $myts->makeTboxData4Edit($down_array['title']);
            $url = $myts->makeTboxData4Edit($down_array['url']);
            $homepage = $myts->makeTboxData4Edit($down_array['homepage']);
            $homepagetitle = $myts->makeTboxData4Edit($down_array['homepagetitle']);
            $version = $myts->makeTboxData4Edit($down_array['version']);
            $size = $myts->makeTboxData4Edit($down_array['size']);
            $platform = $myts->makeTboxData4Edit($down_array['platform']);
            $logourl = $myts->makeTboxData4Edit($down_array['logourl']);
            $price = $myts->makeTboxData4Edit($down_array['price']);
            $description = $myts->makeTareaData4Edit($down_array['description']);
            $mirror = $myts->makeTboxData4Edit($down_array['mirror']);
            $license = $myts->makeTboxData4Edit($down_array['license']); 
            // $paypalemail = $myts->makeTboxData4Edit($down_array['paypalemail']);
            $features = $myts->makeTboxData4Edit($down_array['features']);
            $requirements = $myts->makeTareaData4Edit($down_array['requirements']);
            $limitations = $myts->makeTareaData4Edit($down_array['limitations']);
            $dhistory = $myts->makeTareaData4Edit($down_array['dhistory']);
            $published = $myts->makeTareaData4Edit($down_array['published']);
            $expired = $myts->makeTareaData4Edit($down_array['expired']);
            $updated = $myts->makeTareaData4Edit($down_array['updated']);
            $offline = $myts->makeTareaData4Edit($down_array['offline']);
            $forumid = $myts->makeTareaData4Edit($down_array['forumid']);
            $sform = new XoopsThemeForm(_AM_MODDL, "storyform", xoops_getenv('PHP_SELF'));
        }
        else
        {
            $sform = new XoopsThemeForm(_AM_SUBMITCATHEAD, "storyform", xoops_getenv('PHP_SELF'));
        }

        $sform->setExtra('enctype="multipart/form-data"');
        if ($lid) $sform->addElement(new XoopsFormLabel(_AM_FILEID, "No: " . $lid));

        $sform->addElement(new XoopsFormText(_AM_FILETITLE, 'title', 50, 80, $title), true);
        $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat "), "cid ", "pid");
        $sform->addElement(new XoopsFormText(_AM_DLURL, 'url', 50, 80, $url), false);
        $sform->addElement(new XoopsFormText(_AM_MIRROR, 'mirror', 50, 80, $mirror), false); 
        // $allowed_mimetypes = $xoopsModuleConfig['mimetypes'];
        $allowmimetypes = "";
        foreach ($xoopsModuleConfig['downmimetypes'] as $type)
        {
            $allowmimetypes .= $type . " | ";
        }
        $options_tray = new XoopsFormElementTray(_AM_DUPLOAD, "<br />");
        $upload = new XoopsFormFile('', 'userfile', $xoopsModuleConfig['maxfilesize']);
        $options_tray->addElement($upload);
        $mimetype = new XoopsFormLabel(_AM_ALLOWEDMIME, $allowmimetypes);
        $options_tray->addElement($mimetype);
        $sform->addElement($options_tray);

        $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
        ob_start();
        $sform->addElement(new XoopsFormHidden('cid', $cid));
        $mytree->makeMySelBox('title', 'title', $cid, 0);
        $sform->addElement(new XoopsFormLabel(_AM_CATEGORYC, ob_get_contents()));
        ob_end_clean();

        $sform->addElement(new XoopsFormText(_AM_HOMEPAGETITLEC, 'homepagetitle', 50, 80, $homepagetitle), false);
        $sform->addElement(new XoopsFormText(_AM_HOMEPAGEC, 'homepage', 50, 80, $homepage), false);
        $sform->addElement(new XoopsFormText(_AM_VERSIONC, 'version', 10, 20, $version), false); 
        // $version_array = $xoopsModuleConfig['versiontypes'];
        // $version_select = new XoopsFormSelect('', 'versiontypes', $versiontypes, '', '', 0);
        // $version_select->addOptionArray($version_array);
        // $version_tray = new XoopsFormElementTray(_AM_PLATFORMC, '&nbsp;');
        // $version_tray->addElement($version_select);
        // $sform->addElement($version_tray);
        $sform->addElement(new XoopsFormText(_AM_FILESIZEC, 'size', 10, 20, $size), false);

        $platform_array = $xoopsModuleConfig['platform'];
        $platform_select = new XoopsFormSelect('', 'platform', $platform, '', '', 0);
        $platform_select->addOptionArray($platform_array);
        $platform_tray = new XoopsFormElementTray(_AM_PLATFORMC, '&nbsp;');
        $platform_tray->addElement($platform_select);
        $sform->addElement($platform_tray);

        $license_array = $xoopsModuleConfig['license'];
        $license_select = new XoopsFormSelect('', 'license', $license, '', '', 0);
        $license_select->addOptionArray($license_array);
        $license_tray = new XoopsFormElementTray(_AM_LICENCEC, '&nbsp;');
        $license_tray->addElement($license_select);
        $sform->addElement($license_tray);

        $limitations_array = $xoopsModuleConfig['limitations'];
        $limitations_select = new XoopsFormSelect('', 'limitations', $limitations, '', '', 0);
        $limitations_select->addOptionArray($limitations_array);
        $limitations_tray = new XoopsFormElementTray(_AM_LIMITATIONS, '&nbsp;');
        $limitations_tray->addElement($limitations_select);
        $sform->addElement($limitations_tray);

        $sform->addElement(new XoopsFormText(_AM_PRICEC, 'price', 10, 20, $price), false); 
        // $sform->addElement(new XoopsFormText(_AM_PAYPALEMAILC, 'paypalemail', 50, 80, $paypalemail), false);
        $sform->addElement(new XoopsFormDhtmlTextArea(_AM_DESCRIPTION, 'description', $description, 15, 60), true);
        $sform->addElement(new XoopsFormTextArea(_AM_KEYFEATURESC, 'features', $features, 7, 60), false);
        $sform->addElement(new XoopsFormTextArea(_AM_REQUIREMENTSC, 'requirements', $requirements, 7, 60), false);
        $sform->addElement(new XoopsFormTextArea(_AM_HISTORYC, 'dhistory', $dhistory, 7, 60), false);
        if ($lid && !empty($dhistory))
        {
            $sform->addElement(new XoopsFormTextArea(_AM_HISTORYD, 'dhistoryaddedd', "", 7, 60), false);
        }
        if (!$logourl) $logourl = "blank.png";
        $graph_array = &XoopsLists::getImgListAsArray(XOOPS_ROOT_PATH . "/modules/mydownloads/images/shots");
        $indeximage_select = new XoopsFormSelect('', 'logourl', $logourl, '', '', 1);
        $indeximage_select->addOptionArray($graph_array);
        $indeximage_select->setExtra("onchange='showImgSelected(\"image3\", \"logourl\", \"/modules/mydownloads/images/shots\", \"\", \"" . XOOPS_URL . "\")'");
        $indeximage_tray = new XoopsFormElementTray(_AM_SHOTIMAGE, '&nbsp;');
        $indeximage_tray->addElement($indeximage_select);
        $indeximage_tray->addElement(new XoopsFormLabel('', "<br /><br /><img src='" . XOOPS_URL . "/modules/mydownloads/images/shots/" . $logourl . "' name='image3' id='image3' alt='' />"));
        $sform->addElement($indeximage_tray);

        $sform->insertBreak(sprintf(_AM_MUSTBEVALID, "<b>" . $directory . "</b>"), "even");
        ob_start();
        getforum($forumid);
        $sform->addElement(new XoopsFormLabel(_AM_EDITDISCUSSINFORUM, ob_get_contents()));
        ob_end_clean();

        $publishtext = (!$lid && !$published) ? _AM_SETPUBLISHDATE : _AM_SETNEWPUBLISHDATE;
        if ($published > time())
        {
            $publishtext = _AM_SETPUBDATESETS;
        }
        $ispublished = ($published > time()) ? 1 : 0 ;
        $publishdates = ($published > time()) ? _AM_PUBLISHDATESET . formatTimestamp($published, "Y-m-d H:s") : _AM_SETDATETIMEPUBLISH;
        $publishdate_checkbox = new XoopsFormCheckBox('', 'publishdateactivate', $ispublished);
        $publishdate_checkbox->addOption(1, $publishdates . "<br /><br />");

        if ($lid)
        {
            $sform->addElement(new XoopsFormHidden('was_published', $published));
            $sform->addElement(new XoopsFormHidden('was_expired', $expired));
        }

        $publishdate_tray = new XoopsFormElementTray(_AM_PUBLISHDATE, '');
        $publishdate_tray->addElement($publishdate_checkbox);
        $publishdate_tray->addElement(new XoopsFormDateTime($publishtext, 'published', 15, $published));
        $publishdate_tray->addElement(new XoopsFormRadioYN(_AM_CLEARPUBLISHDATE, 'clearpublish', 0, ' ' . _AM_YES . '', ' ' . _AM_NO . ''));
        $sform->addElement($publishdate_tray);

        $isexpired = ($expired > time()) ? 1: 0 ;
        $expiredates = ($expired > time()) ? _AM_EXPIREDATESET . formatTimestamp($expired, 'Y-m-d H:s') : _AM_SETDATETIMEEXPIRE;
        $warning = ($published > $expired && $expired > time()) ? _AM_EXPIREWARNING : '';
        $expiredate_checkbox = new XoopsFormCheckBox('', 'expiredateactivate', $isexpired);
        $expiredate_checkbox->addOption(1, $expiredates . "<br /><br />");

        $expiredate_tray = new XoopsFormElementTray(_AM_EXPIREDATE . $warning, '');
        $expiredate_tray->addElement($expiredate_checkbox);
        $expiredate_tray->addElement(new XoopsFormDateTime(_AM_SETEXPIREDATE . "<br />", 'expired', 15, $expired));
        $expiredate_tray->addElement(new XoopsFormRadioYN(_AM_CLEAREXPIREDATE, 'clearexpire', 0, ' ' . _AM_YES . '', ' ' . _AM_NO . ''));
        $sform->addElement($expiredate_tray);

        $filestatus_radio = new XoopsFormRadioYN(_AM_FILESSTATUS, 'offline', $offline, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($filestatus_radio);

        $up_dated = ($updated == 0) ? 0 : 1;
        $file_updated_radio = new XoopsFormRadioYN(_AM_SETASUPDATED, 'up_dated', $up_dated, ' ' . _AM_YES . '', ' ' . _AM_NO . '');
        $sform->addElement($file_updated_radio);

        if (!$lid)
        {
            $button_tray = new XoopsFormElementTray('', '');
            $button_tray->addElement(new XoopsFormHidden('status', 2));
            $button_tray->addElement(new XoopsFormHidden('op', 'addDownload'));
            $button_tray->addElement(new XoopsFormButton('', '', _AM_ADD, 'submit'));
            $sform->addElement($button_tray);
        }
        else
        {
            $button_tray = new XoopsFormElementTray('', '');
            $button_tray->addElement(new XoopsFormHidden('lid', $lid));
            $hidden = new XoopsFormHidden('op', 'addDownload');
            $button_tray->addElement($hidden);

            $butt_dup = new XoopsFormButton('', '', _AM_MODIFY, 'submit');
            $butt_dup->setExtra('onclick="this.form.elements.op.value=\'addDownload\'"');
            $button_tray->addElement($butt_dup);

            $butt_dupct = new XoopsFormButton('', '', _AM_DELETE, 'submit');
            $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'delDownload\'"');
            $button_tray->addElement($butt_dupct);

            $butt_dupct2 = new XoopsFormButton('', '', _CANCEL, 'submit');
            $butt_dupct2->setExtra('onclick="this.form.elements.op.value=\'downloadsConfigMenu\'"');
            $button_tray->addElement($butt_dupct2);
            $sform->addElement($button_tray);
        }
        $sform->display();
        unset($hidden);
    }
    else
    {
        redirect_header("category.php?", 1, "You must create a category before you can add a new download");
        exit();
    }

    if ($lid)
    { 
        // Vote data
        $result5 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_votedata") . "");
        list($totalvotes) = $xoopsDB->getRowsNum($result5);
        $result6 = $xoopsDB->query("SELECT ratingid, ratinguser, rating, ratinghostname, ratingtimestamp FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE lid = $lid AND ratinguser != 0 ORDER BY ratingtimestamp DESC");
        $votesreg = $xoopsDB->getRowsNum($result6);
        $result7 = $xoopsDB->query("SELECT ratingid, rating, ratinghostname, ratingtimestamp FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE lid = $lid AND ratinguser = 0 ORDER BY ratingtimestamp DESC");
        $votesanon = $xoopsDB->getRowsNum($result7);

        $totalvotes = ($totalvotes != 0) ? $totalvotes : 0;

        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _AM_DLRATINGS . "</legend>";
        echo "<div style='padding: 8px;'><b>" . _AM_DLRATINGSVOTES . "</b>" . $totalvotes . "<br />";
        printf(_AM_REGUSERVOTES, $votesreg);
        echo "<br />";
        printf(_AM_ANONUSERVOTES, $votesanon);
        echo "</div>";

        echo "<table width=100% cellspacing = 1 cellpadding = '2' class = 'outer'>";
        echo "<tr>";
        echo "<td class = 'bg3'><b>" . _AM_USER . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_IP . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_RATING . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_USERAVG . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_TOTALRATE . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_DATE . "  </b></td>";
        echo "<td align='center' class = 'bg3'><b>" . _AM_DELETE . "</b></td>";
        echo "</tr>";

        if ($votesreg == 0)
        {
            echo "<tr><td align='center' colspan='7' class = 'even'><b>" . _AM_NOREGVOTES . "</b></td></tr>";
        }
        while (list($ratingid, $ratinguser, $rating, $ratinghostname, $ratingtimestamp) = $xoopsDB->fetchRow($result6))
        {
            $result2 = $xoopsDB->query("SELECT rating FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE ratinguser = $ratinguser");
            $uservotes = $xoopsDB->getRowsNum($result2);
            $formatted_date = formatTimestamp($ratingtimestamp, $xoopsModuleConfig['dateformat']);
            $useravgrating = 0;
            while (list($rating2) = $xoopsDB->fetchRow($result2))
            {
                $useravgrating = $useravgrating + $rating2;
            }
            $useravgrating = $useravgrating / $uservotes;
            $useravgrating = number_format($useravgrating, 1);
            $ratinguname = XoopsUser::getUnameFromId($ratinguser);

            echo "<tr><td class = 'head'>$ratinguname</td>";
            echo "<td class = 'even'>$ratinghostname</td>";
            echo "<td class = 'odd'>$rating</td>";
            echo "<td class = 'even'>$useravgrating</td>";
            echo "<td class = 'odd'>$uservotes</td>";
            echo "<td class = 'even'>$formatted_date</td>";
            echo "<td class = 'odd' align=\"center\">";
            echo "<a href='index.php?op=delVote&lid=" . $lid . "&rid=" . $ratingid . "'>Delete</a>";
            echo "</td></tr>";
        }
        echo "</table>";

        echo "<br />";

        echo "<table width=100% cellspacing = 1 cellpadding = '2' class = 'outer'>";
        echo "<tr>";
        echo "<td class = 'bg3' colspan=2><b>" . _AM_IP . "  </b></td>";
        echo "<td class = 'bg3' colspan=3><b>" . _AM_RATING . "  </b></td>";
        echo "<td class = 'bg3'><b>" . _AM_DATE . "  </b></td>";
        echo "<td align='center' class = 'bg3'><b>" . _AM_DELETE . "</b></td>";
        echo "</tr>";

        if ($votesanon == 0)
        {
            echo "<tr><td colspan='7' align='center' class = 'even'><b>" . _AM_NOUNREGVOTES . "</b></td></tr>";
        }
        while (list($ratingid, $rating, $ratinghostname, $ratingtimestamp) = $xoopsDB->fetchRow($result7))
        {
            $formatted_date = formatTimestamp($ratingtimestamp, $xoopsModuleConfig['dateformat']);
            echo "<td colspan='2' class = 'head'>$ratinghostname</td>";
            echo "<td colspan='3' class = 'even'>$rating</td>";
            echo "<td class = 'odd'>$formatted_date</td>";
            echo "<td class = 'even' align='center'>";
            echo "<a href='index.php?op=delVote&lid=" . $lid . "&rid=" . $ratingid . "'>Delete</a>";
            echo "</td></tr>";
        }
        echo "</table>";
        echo "</fieldset>";
    }
    xoops_cp_footer();
}

function delVote()
{
    global $xoopsDB, $HTTP_GET_VARS;
    $xoopsDB->queryF("DELETE FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE ratingid = " . $HTTP_GET_VARS['rid'] . "");
    updaterating($HTTP_GET_VARS['lid']);
    redirect_header("index.php", 1, _AM_VOTEDELETED);
}

function addDownload()
{
    global $xoopsDB, $xoopsUser, $xoopsModule, $HTTP_POST_VARS, $myts, $eh, $HTTP_POST_FILES, $xoopsModuleConfig;

    $lid = (!empty($HTTP_POST_VARS['lid'])) ? $HTTP_POST_VARS['lid'] : 0;
    $cid = (!empty($HTTP_POST_VARS['cid'])) ? $HTTP_POST_VARS['cid'] : 0;
    $status = (!empty($HTTP_POST_VARS['status'])) ? $HTTP_POST_VARS['status'] : 2;
    /**
     * Define URL
     */

    if (empty($HTTP_POST_FILES['userfile']['name']) && $HTTP_POST_VARS["url"] && $HTTP_POST_VARS["url"] != "" && $HTTP_POST_VARS["url"] != "http://")
    {
        $url = ($HTTP_POST_VARS["url"] != "http://") ? $myts->addslashes(formatURL($HTTP_POST_VARS["url"])) : '';
        $size = ((empty($size) || !is_numeric($size))) ? $myts->addslashes($HTTP_POST_VARS["size"]) : 0; 
        // if ($fp = @fopen ("$url", "r"))
        // {
        // 
        // } else {
        // redirect_header( xoops_getenv('PHP_SELF'), 1 , _AM_FILENOTEXIST);
        // }
        // @fclose($fp);
    }
    else
    {
        Global $HTTP_POST_FILES;
        $down = uploading($HTTP_POST_FILES, '', 0, 0);
        $url = $down['url'];
        $size = $down['size'];
    }
    /**
     * Get data from form
     */
    $logourl = ($HTTP_POST_VARS["logourl"] != "blank.png") ? $myts->addslashes($HTTP_POST_VARS["logourl"]) : '';
    $title = $myts->addslashes(trim($HTTP_POST_VARS["title"]));
    if (!empty($HTTP_POST_VARS["homepage"]) || $HTTP_POST_VARS["homepage"] != "http://")
    {
        $homepage = $myts->addslashes(formatURL(trim($HTTP_POST_VARS["homepage"])));
        $homepagetitle = $myts->addslashes(trim($HTTP_POST_VARS["homepagetitle"]));
    }
    else
    {
        $homepage = '';
        $homepagetitle = '';
    }
    $version = trim($HTTP_POST_VARS["version"]);
    $version = (!empty($version)) ? $myts->addslashes($version) : 0;
    $platform = $myts->addslashes(trim($HTTP_POST_VARS["platform"]));
    $description = $myts->addslashes(trim($HTTP_POST_VARS["description"]));
    $submitter = $xoopsUser->uid();
    $price = $myts->addslashes(trim($HTTP_POST_VARS["price"]));
    $mirror = $myts->addslashes(formatURL(trim($HTTP_POST_VARS["mirror"])));
    $license = $myts->addslashes(trim($HTTP_POST_VARS["license"])); 
    // $paypalemail = $myts->addslashes($HTTP_POST_VARS["paypalemail"]);
    $paypalemail = '';
    $features = $myts->addslashes(trim($HTTP_POST_VARS["features"]));
    $requirements = $myts->addslashes(trim($HTTP_POST_VARS["requirements"]));
    $forumid = (isset($HTTP_POST_VARS["forumid"]) && $HTTP_POST_VARS["forumid"] > 0) ? intval($HTTP_POST_VARS["forumid"]) : 0;
    $limitations = (isset($HTTP_POST_VARS["limitations"])) ? $myts->addslashes($HTTP_POST_VARS["limitations"]) : '';
    $dhistory = (isset($HTTP_POST_VARS["dhistory"])) ? $myts->addslashes($HTTP_POST_VARS["dhistory"]) : '';
    $dhistoryhistory = (isset($HTTP_POST_VARS["dhistoryaddedd"])) ? $myts->addslashes($HTTP_POST_VARS["dhistoryaddedd"]) : '';
    if ($lid > 0 && !empty($dhistoryhistory))
    {
        $dhistory = $dhistory . "\n\n";
        $time = time();
        $dhistory .= _AM_HISTORYVERS . $version . _AM_HISTORDATE . formatTimestamp($time, $xoopsModuleConfig['dateformat']) . "\n\n";
        $dhistory .= $dhistoryhistory;
    }
    $updated = ($_POST['up_dated'] == 0) ? 0 : time();
    $offline = ($_POST['offline'] == 1) ? 1 : 0;
    if (!$lid)
    {
        $date = time();
        $publishdate = time();
    }
    else
    {
        $publishdate = $_POST['was_published'];
        $expiredate = $_POST['was_expired'];
    }

    if (isset($_POST['publishdateactivate']))
    {
        $publishdate = strtotime($_POST['published']['date']) + $_POST['published']['time'];
    }
    if ($_POST['clearpublish'])
    {
        $result = $xoopsDB->query("SELECT date FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid");
        list($date) = $xoopsDB->fetchRow($result);
        $publishdate = $date;
    }

    if (isset($_POST['expiredateactivate']))
    {
        $expiredate = strtotime($_POST['expired']['date']) + $_POST['expired']['time'];
    }
    if ($_POST['clearexpire'])
    {
        $expiredate = '0';
    }
    /**
     * Update or insert download data into database
     */
    if (!$lid)
    {
        $date = time();
        $publishdate = time();
        $xoopsDB->query("INSERT INTO " . $xoopsDB->prefix("mydownloads_downloads") . " 
			(lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits, rating, 
			votes, comments, price, mirror, license, paypalemail, features, requirements, homepagetitle, forumid,
			limitations, dhistory, published, expired, updated, offline) VALUES 
			('', $cid, '$title', '$url', '$homepage', '$version', $size, '$platform', '$logourl', '$submitter', '$status', 
			'$date', 0, 0, 0, 0, '$price', '$mirror', '$license', '$paypalemail', '$features', '$requirements', 
			'$homepagetitle', '$forumid', '$limitations', '$dhistory', '$publishdate', 0, '$updated', '$offline')");
        $newid = $xoopsDB->getInsertId();
        $xoopsDB->query("INSERT INTO " . $xoopsDB->prefix("mydownloads_text") . " (lid, description) VALUES ($newid, '$description' )");
        $tags = array();
        $tags['FILE_NAME'] = $title;
        $tags['FILE_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/singlefile.php?cid=' . $cid . '&amp;lid=' . $newid;
        $sql = "SELECT title FROM " . $xoopsDB->prefix("mydownloads_cat") . " WHERE cid=" . $cid;
        $result = $xoopsDB->query($sql);
        $row = $xoopsDB->fetchArray($result);
        $tags['CATEGORY_NAME'] = $row['title'];
        $tags['CATEGORY_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/viewcat.php?cid=' . $cid;
        $notification_handler = &xoops_gethandler('notification');
        $notification_handler->triggerEvent('global', 0, 'new_file', $tags);
        $notification_handler->triggerEvent('category', $cid, 'new_file', $tags);
    }
    else
    {
        $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_downloads") . " SET cid = $cid, title = '$title', 
			url = '$url', mirror = '$mirror', paypalemail = '$paypalemail', license = '$license', 
			features = '$features', homepage = '$homepage', version = '$version', size = $size, platform = '$platform',
			logourl = '$logourl', status = '$status', price = '$price', requirements = '$requirements', 
			homepagetitle = '$homepagetitle', forumid = '$forumid', limitations = '$limitations', dhistory = '$dhistory', published = '$publishdate', 
			expired = '$expiredate', updated = '$updated', offline = '$offline' WHERE lid = $lid");
        $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_text") . " SET description = '$description' WHERE lid = '$lid'");
    }
    /**
     * Send notifications
     */

    $message = (!$lid) ? "New download added to database" : "Download updated" ;
    redirect_header("index.php?op=downloadsConfigMenu", 1, $message);
} 
// Page start here
if (isset($HTTP_POST_VARS))
{
    foreach ($HTTP_POST_VARS as $k => $v)
    {
        $$k = $v;
    }
}

if (isset($HTTP_GET_VARS))
{
    foreach ($HTTP_GET_VARS as $k => $v)
    {
        $$k = $v;
    }
}

if (!isset($HTTP_POST_VARS['op']))
{
    $op = isset($HTTP_GET_VARS['op']) ? $HTTP_GET_VARS['op'] : 'main';
}
else
{
    $op = $HTTP_POST_VARS['op'];
}

switch ($op)
{
    case "addDownload":
        addDownload();
        break;
    case "Download":
        Download();
        break;
    case "delDownload":

        global $xoopsDB, $HTTP_POST_VARS, $xoopsModule, $xoopsModuleConfig;
        $confirm = (isset($confirm)) ? 1 : 0;
        if ($confirm)
        {
            $file = XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . basename($HTTP_POST_VARS['url']);
            if (is_file($file))
            {
                @unlink($file);
            }
            $xoopsDB->query("DELETE FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid = " . $HTTP_POST_VARS['lid'] . "");
            $xoopsDB->query("DELETE FROM " . $xoopsDB->prefix("mydownloads_text") . " WHERE lid = " . $HTTP_POST_VARS['lid'] . "");
            $xoopsDB->query("DELETE FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE lid = " . $HTTP_POST_VARS['lid'] . ""); 
            // delete comments
            xoops_comment_delete($xoopsModule->getVar('mid'), $HTTP_POST_VARS['lid']);
            redirect_header("index.php", 1, sprintf(_AM_FILEDELETED, $title));
            exit();
        }
        else
        {
            $lid = (isset($HTTP_POST_VARS['lid'])) ? $HTTP_POST_VARS['lid'] : $lid;
            $result = $xoopsDB->query("SELECT lid, title, url FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid = $lid");
            list($lid, $title, $url) = $xoopsDB->fetchrow($result);
            xoops_cp_header();
            xoops_confirm(array('op' => 'delDownload', 'lid' => $lid, 'confirm' => 1, 'title' => $title, 'url' => $url), 'index.php', _AM_DELETETHISDOWNLOAD . "<br /><br>" . $title, _DELETE);
            xoops_cp_footer();
        }
        break;
    case "delVote":
        delVote();
        break;
    case "del_review":

        global $xoopsDB, $HTTP_POST_VARS, $xoopsModule;
        $confirm = (isset($confirm)) ? 1 : 0;
        if ($confirm)
        {
            $xoopsDB->query("DELETE FROM " . $xoopsDB->prefix("mydownloads_reviews") . " WHERE review_id = " . $HTTP_POST_VARS['review_id'] . "");
            redirect_header("index.php", 1, sprintf(_AM_FILEDELETED, $title));
            exit();
        }
        else
        {
            $review_id = (isset($HTTP_POST_VARS['review_id'])) ? $HTTP_POST_VARS['review_id'] : $review_id;
            $sql = "SELECT review_id, title FROM " . $xoopsDB->prefix("mydownloads_reviews") . " WHERE review_id = $review_id";
            $result = $xoopsDB->query($sql);
            list($review_id, $title) = $xoopsDB->fetchrow($result);
            xoops_cp_header();
            xoops_confirm(array('op' => 'del_review', 'review_id' => $review_id, 'confirm' => 1, 'title' => $title), 'index.php', _AM_DELETETHISDOWNLOAD . "<br /><br>" . $title, _DELETE);
            xoops_cp_footer();
        }
        break;
    case "approve_review":

        global $xoopsDB;
        $review_id = isset($HTTP_GET_VARS['review_id']) ? intval($HTTP_GET_VARS['review_id']) : 0;
        $sql = "UPDATE " . $xoopsDB->prefix("mydownloads_reviews") . " SET submit = 1 WHERE review_id = '$review_id'";
        $result = $xoopsDB->queryF($sql);
        $error = "<a href='javascript:history.go(-1)'>Return to where you last where</a><br /><br />";
        $error .= "Could not retrive review data: <br /><br />";
        $error .= $sql;
        if (!$result)
        {
            trigger_error($error, E_USER_ERROR);
        }
        redirect_header("index.php?op=reviews", 1, _AM_REVIEW_UPDATED);
        exit();
        break;
    case "edit_review":

        $confirm = (isset($confirm)) ? 1 : 0;
        if ($confirm)
        {
            $review_id = intval($HTTP_POST_VARS['review_id']);
            $title = $myts->addSlashes(trim($HTTP_POST_VARS['title']));
            $review = $myts->addSlashes(trim($HTTP_POST_VARS['review']));
            $rated = intval($HTTP_POST_VARS['rated']);
            $confirm = intval($HTTP_POST_VARS['confirm']);
            $xoopsDB->queryF("UPDATE " . $xoopsDB->prefix("mydownloads_reviews") . " 
			SET title = '$title', review = '$review', rated = '$rated', 
			 WHERE review_id = '$review_id'");
            redirect_header("index.php", 1, _AM_REVIEW_UPDATE);
            exit();
        }
        else
        {
            $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_reviews') . " WHERE review_id = " . $_GET['review_id'] . "" ;
            $arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
            xoops_cp_header();
            adminmenu(_AM_DLCONF, 0);
            echo "<div align = 'left'>" . _AM_REV__SNEWMNAMEDESC . "</div>";
            $sform = new XoopsThemeForm(_AM_REV_SUBMITREV, "reviewform", xoops_getenv('PHP_SELF'));
            $sform->addElement(new XoopsFormText(_AM_REV_TITLE, 'title', 30, 40, $arr['title']), true);
            $rating_select = new XoopsFormSelect(_AM_REV_RATING, "rated", $arr['rated']);
            $rating_select->addOptionArray(array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10));
            $sform->addElement($rating_select);
            $sform->addElement(new XoopsFormDhtmlTextArea(_AM_REV_DESCRIPTION, 'review', $arr['review'], 15, 60), true);
            $sform->addElement(new XoopsFormHidden("lid", $arr['lid']));
            $sform->addElement(new XoopsFormHidden("review_id", $arr['review_id']));
            $sform->addElement(new XoopsFormHidden("confirm", 1));
            $button_tray = new XoopsFormElementTray('', '');
            $button_tray->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
            $sform->addElement($button_tray);
            $sform->display();
            xoops_cp_footer();
        }
        break;
    case "reviews":

        include_once XOOPS_ROOT_PATH . '/class/pagenav.php';
        global $xoopsDB, $myts, $xoopsModuleConfig, $imagearray;
        $start = isset($HTTP_GET_VARS['start']) ? intval($HTTP_GET_VARS['start']) : 0;
        $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_reviews') . " WHERE submit = 0 ORDER BY review_id" ;
        $result = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'], $start);
        $num = $xoopsDB->getRowsNum($result);
        $error = "<a href='javascript:history.go(-1)'>Return to where you last where</a><br /><br />";
        $error .= "Could not retrive review data: <br /><br />";
        $error .= $sql;
        if (!$result)
        {
            trigger_error($error, E_USER_ERROR);
        }

        xoops_cp_header();
        adminmenu(_AM_DLCONF, 0);
        echo "<h4>" . _AM_NEWREVIEWS . "&nbsp;($num)</h4>";
        echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
        echo "<tr>";
        echo "<td class='bg3' align='center' width = '3%'><b>" . _AM_ID . "</b></td>";
        echo "<td class='bg3' align='left' width = '30%'><b>" . _AM_TITLE . "</b></td>";
        echo "<td class='bg3' align='center' width = '15%'><b>" . _AM_POSTER . "</b></td>";
        echo "<td class='bg3' align='center' width = '15%'><b>" . _AM_SUBMITDATE . "</b></td>";
        echo "<td class='bg3' align='center' width = '7%'><b>" . _AM_ACTION . "</b></td>";
        echo "</tr>";
        if ($num)
        {
            while ($review_array = $xoopsDB->fetchArray($result))
            {
                $review_id = intval($review_array['review_id']);
                $sql2 = "SELECT title FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid = " . $review_array['lid'] . "" ;
                list($title) = $xoopsDB->fetchrow($result2 = $xoopsDB->query($sql2));
                $title = $myts->htmlSpecialChars($title);
                $lid = $myts->htmlSpecialChars($review_array['lid']);
                $submitter = xoops_getLinkedUnameFromId($review_array['uid']);
                $datetime = formatTimestamp($review_array['date'], $xoopsModuleConfig['dateformat']);
                $status = (intval($review_array['submit'])) ? $approved : "<a href='index.php?op=approve_review&review_id=" . $review_id . "'>" . $imagearray['approve'] . "</a>";
                $modify = "<a href='index.php?op=edit_review&review_id=" . $review_id . "'>" . $imagearray['editimg'] . "</a>";
                $delete = "<a href='index.php?op=del_review&review_id=" . $review_id . "'>" . $imagearray['deleteimg'] . "</a>";
                echo "<tr>";
                echo "<td class='head' align='center'>" . $review_id . "</td>";
                echo "<td class='even' align='left' nowrap><a href='index.php?op=Download&lid=" . $lid . "'>" . $title . "</a></td>";
                echo "<td class='even' align='center' nowrap>$submitter</td>";
                echo "<td class='even' align='center'>" . $datetime . "</td>";
                echo "<td class='even' align='center'  nowrap>$status $modify $delete</td>";
                echo "</tr>";
            }
        }
        else
        {
            echo "<tr ><td align = 'center' class='head' colspan = '6'>" . _AM_NOWAITINGREVIEWS . "</td></tr>";
        }
        echo "</table>\n";
        $pagenav = new XoopsPageNav($num, $xoopsModuleConfig['perpage'] , $start, 'start');
        echo '<div text-align="right">' . $pagenav->renderNav() . '</div>';
        xoops_cp_footer();
        break;

    case 'main':
    default:

        Global $xoopsUser, $xoopsDB, $xoopsConfig;
        include_once XOOPS_ROOT_PATH . '/class/pagenav.php';
        $start = isset($HTTP_GET_VARS['start']) ? intval($HTTP_GET_VARS['start']) : 0;
        $start1 = isset($HTTP_GET_VARS['start1']) ? intval($HTTP_GET_VARS['start1']) : 0;
        $start2 = isset($HTTP_GET_VARS['start2']) ? intval($HTTP_GET_VARS['start2']) : 0;
        $start3 = isset($HTTP_GET_VARS['start3']) ? intval($HTTP_GET_VARS['start3']) : 0;
        $start4 = isset($HTTP_GET_VARS['start4']) ? intval($HTTP_GET_VARS['start4']) : 0;
        $totalcats = totalcategory();
        $result = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_broken") . "");
        list($totalbrokendownloads) = $xoopsDB->fetchRow($result);
        $result2 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_mod") . "");
        list($totalmodrequests) = $xoopsDB->fetchRow($result2); 
        // $totalmodrequests_count = $xoopsDB->getRowsNum($result2);
        $result3 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status = 0");
        list($totalnewdownloads) = $xoopsDB->fetchRow($result3);
        $result4 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status > 0");
        list($totaldownloads) = $xoopsDB->fetchRow($result4);
        $result5 = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_reviews") . " WHERE submit = 0");
        list($newreviews) = $xoopsDB->fetchRow($result5);
        
		xoops_cp_header();
        adminmenu(_AM_DLADMIN, 0);
        $result = $xoopsDB->query("SELECT COUNT(*) FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE status > 0");
        list($numrows) = $xoopsDB->fetchRow($result);
        
		echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _AM_DOWNSUMMARY . "</legend>";
        echo "<div style='padding: 8px;'>" . sprintf(_AM_THEREARE, "<b>" . $totalcats . "</b>", "<b>" . $numrows . "</b>") . "<br />";
        echo "" . _AM_NEWFILESVAL . " <b>$totalnewdownloads</b><br />";
        echo "" . _AM_MODIFIEDFILEVAL . " <b>$totalmodrequests</b><br />";
        echo "" . _AM_BROKENFILEREQUESTS . " <b>$totalbrokendownloads</b><br />";
        echo "" . _AM_NEWREVIEWS . " <b>$newreviews</b></div>";
        echo "</fieldset><br />";
        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _AM_SERVERSTATUS . "</legend>";
        echo "<div style='padding: 8px;'>";
        if (ini_get('enable_dl')) echo "" . _AM_UPLOADSON . "";
        else echo "" . _AM_UPLOADSOFF . ""; 
        // ini_set("upload_max_filesize", "2M");
        // ini_set("memory_limit", "16M");
        // ini_set("post_max_size", "16M");
        $maxsersize = ini_get('upload_max_filesize');
        $maxpostsize = ini_get('post_max_size'); 
        // $maxmemlimitsize = ini_get('memory_limit');
        echo "<div>" . _MD_MAXUPLOADSERVER . " <b>" . $maxsersize . "</b></div>"; 
        // echo "<div>"._MD_MAXMEMLIMIT." <b>".$maxmemlimitsize."</b></div>";
        echo "<div>" . _MD_MAXPOSTSIZE . " <b>" . $maxpostsize . "</b></div>";
        echo "</div>";
        echo "</fieldset>";
        if ($totalcats > 0)
        {
            include_once XOOPS_ROOT_PATH . '/class/xoopsformloader.php';
            $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
            $sform = new XoopsThemeForm(_AM_MODIFY, "category", "category.php");
            ob_start();
            $sform->addElement(new XoopsFormHidden('cid', ''));
            $mytree->makeMySelBox("title", "cid");
            $sform->addElement(new XoopsFormLabel(_AM_MODCAT, ob_get_contents()));
            ob_end_clean();
            $dup_tray = new XoopsFormElementTray('', '');
            $dup_tray->addElement(new XoopsFormHidden('op', 'modCat'));
            $butt_dup = new XoopsFormButton('', '', _AM_MODIFY, 'submit');
            $butt_dup->setExtra('onclick="this.form.elements.op.value=\'modCat\'"');
            $dup_tray->addElement($butt_dup);
            $butt_dupct = new XoopsFormButton('', '', _DELETE, 'submit');
            $butt_dupct->setExtra('onclick="this.form.elements.op.value=\'del\'"');
            $dup_tray->addElement($butt_dupct);
            $sform->addElement($dup_tray);
            $sform->display();
        }

        if ($numrows > 0)
        {
            $sql = "SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " 
				WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") 
				AND offline = 0 ORDER BY lid DESC" ;
            $published_array = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'], $start);
            $published_array_count = $xoopsDB->getRowsNum($xoopsDB->query($sql));

            downlistheader(_AM_PUBLISHEDDOWN);
            if ($published_array_count > 0)
            {
                while ($published = $xoopsDB->fetchArray($published_array))
                {
                    downlistbody($published);
                }
            }
            else
            {
                downlistfooter();
            }
            downlistpagenav(count($published_array), $start, 'art');
            /**
             * Auto Publish
             */
            $sql = "SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " 
				WHERE published > " . time() . " ORDER BY lid DESC" ;
            $auto_publish_array = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'], $start2);
            $auto_publish_count = $xoopsDB->getRowsNum($xoopsDB->query($sql));
            downlistheader(_AM_AUTOPUBLISHEDDOWN);
            if ($auto_publish_count > 0)
            {
                while ($auto_publish = $xoopsDB->fetchArray($auto_publish_array))
                {
                    downlistbody($auto_publish);
                }
            }
            else
            {
                downlistfooter();
            }
            downlistpagenav($auto_publish_count, $start2, 'art2');
            /**
             * Auto expire FAQ
             */
            $sql = "SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " 
				WHERE expired > " . time() . " ORDER BY lid DESC" ;
            $auto_expire_array = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'], $start3);
            $auto_expire_count = $xoopsDB->getRowsNum($xoopsDB->query($sql));
            downlistheader(_AM_AUTOEXPIREDDOWN);
            if ($auto_expire_count > 0)
            {
                while ($auto_expire = $xoopsDB->fetchArray($auto_expire_array))
                {
                    faqlistbody($auto_expire);
                }
            }
            else
            {
                downlistfooter();
            }
            downlistpagenav($auto_expire_count, $start3, 'art3');
            /**
             * Offline FAQ
             */
            $sql = "SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE 
				offline = 1 ORDER BY lid DESC" ;
            $offline_array = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'], $start4);
            $offline_count = $xoopsDB->getRowsNum($xoopsDB->query($sql));
            downlistheader(_AM_OFFLINEDOWN);
            if ($offline_count > 0)
            {
                while ($is_offline = $xoopsDB->fetchArray($offline_array))
                {
                    downlistbody($is_offline);
                }
            }
            else
            {
                downlistfooter();
            }
            downlistpagenav($offline_count, $start4, 'art4');
        }
        xoops_cp_footer();
        break;
}

?>
