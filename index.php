<?php 
// $Id: index.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";

global $xoopsModuleConfig, $xoopsModule, $xoopsUser;

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");

include XOOPS_ROOT_PATH . "/header.php";
$xoopsOption['template_main'] = 'mydownloads_index.html';
/**
 * Begin Main page Heading etc
 */
$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_indexpage') . " ";
$head_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
$catarray['imageheader'] = imageheader();
$catarray['indexheading'] = $myts->displayTarea($head_arr['indexheading']);
$catarray['indexheaderalign'] = $head_arr['indexheaderalign'];
$catarray['indexfooteralign'] = $head_arr['indexfooteralign'];
$catarray['indexheader'] = $myts->displayTarea($head_arr['indexheader'], $head_arr['nohtml'], $head_arr['nosmiley'], $head_arr['noxcodes'], $head_arr['noimages'], $head_arr['nobreak']);
$catarray['indexfooter'] = $myts->displayTarea($head_arr['indexfooter'], $head_arr['nohtml'], $head_arr['nosmiley'], $head_arr['noxcodes'], $head_arr['noimages'], $head_arr['nobreak']);
$catarray['letters'] = letters();
$catarray['toolbar'] = toolbar();
$xoopsTpl->assign('catarray', $catarray);
/**
 * End main page Headers
 */

/**
 * Begin Main page download info
 */
$listings = totallistings();

$result = $xoopsDB->query("SELECT * FROM " . $xoopsDB->prefix("mydownloads_cat") . " WHERE pid = 0 ORDER BY title");
$hasitems = $xoopsDB->getRowsNum($result);

$groups = (is_object($xoopsUser)) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
$module_id = $xoopsModule->getVar('mid');
$gperm_handler = &xoops_gethandler('groupperm');

$count = 1;
$chcount = 0;
while ($myrow = $xoopsDB->fetchArray($result))
{
    $subtotaldownload = 0;
	$totaldownload = getTotalItems($myrow['cid']);

    if ($hasitems > 0 || $xoopsModuleConfig['emptycats'] == 1)
    {
        if ($gperm_handler->checkRight('DownCatPerm', $myrow['cid'] , $groups, $module_id))
        {
            $title = $myts->makeTboxData4Show($myrow['title']);
            $description = $myts->makeTareaData4Show($myrow['description']);
            $indicator = isnewimage($myrow['cid']);
            /**
             * get child category objects
             */
            $arr = array();
            $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
            $arr = $mytree->getFirstChild($myrow['cid'], "title");
            $space = 0;
            $chcount = 0;
            $subcategories = "";

            foreach($arr as $ele)
            {
                if ($gperm_handler->checkRight('DownCatPerm', $ele['cid'] , $groups, $xoopsModule->getVar('mid')))
                {
                    $result3 = $xoopsDB->query("SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE CID = " . $ele['cid'] . "");
                    $hassubitems = $xoopsDB->getRowsNum($result3);
                    $subtotaldownload = getTotalItems($ele['cid'], 1);
                    $indicator = isnewimage($ele['cid']);
                    if ($xoopsModuleConfig['subcats'])
                    {
                        $chtitle = $myts->makeTboxData4Show($ele['title']);
                        if ($chcount > 5)
                        {
                            $subcategories .= "...";
                            break;
                        } 
                        if ($space > 0) $subcategories .= ", ";
                        $subcategories .= "<a href=\"" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/viewcat.php?cid=" . $ele['cid'] . "\">" . $chtitle . "</a>";
                        $space++;
                        $chcount++;
                    } 
                } 
            } 
            if ($myrow['imgurl'] && $myrow['imgurl'] != 'blank.png')
            {
                if (!$xoopsModuleConfig['usethumbs'])
                {
                    $imgurl = XOOPS_URL . "/" . $xoopsModuleConfig['catimage'] . "/" . $myts->makeTboxData4Show($myrow['imgurl']);
                } 
                else
                {
                    $imgurl = xoops_createthumb($myts->makeTboxData4Show($myrow['imgurl']) , XOOPS_ROOT_PATH, "/" . $xoopsModuleConfig['catimage'], "/thumbs/", $xoopsModuleConfig['shotheight'], $xoopsModuleConfig['shotwidth'], 90);
                    $imgurl = XOOPS_URL . "/" . $xoopsModuleConfig['catimage'] . "/thumbs/" . basename($imgurl);
                } 
            } 
            else
            {
                $imgurl = $indicator['image'];
            } 

            $xoopsTpl->append('categories', array('image' => $imgurl, 'id' => $myrow['cid'], 'title' => $title, 'description' => $description, 'subcategories' => $subcategories, 'totaldownloads' => $totaldownload, 'subtotaldownloads' => $subtotaldownload, 'count' => $count, 'alttext' => $indicator['alttext']));
            $xoopsTpl->assign('lang_categorymain', _MD_MAINLISTING);
            $count++;
        } 
    } 
} 


$listings = totallistings();
$catlisting = ($count = $count -1) + $chcount;

if ($catlisting == 0)
{
    $lang_ThereAre = _MD_THEREISNO;
} elseif ($catlisting == 1)
{
    $lang_ThereAre = _MD_THEREIS;
} 
else
{
    $lang_ThereAre = _MD_THEREARE;
} 
$xoopsTpl->assign('legend', array('legendempty' => _MD_LEGENDTEXTNONEW,
        'legendnew' => _MD_LEGENDTEXTNEW,
        'legendthree' => _MD_LEGENDTEXTNEWTHREE,
        'legendthisweek' => _MD_LEGENDTEXTTHISWEEK,
        'legendlastweek' => _MD_LEGENDTEXTNEWLAST)
    );
$xoopsTpl->assign('modulename', $xoopsModule->dirname());
$xoopsTpl->assign('lang_thereare', sprintf($lang_ThereAre, "<b>" . $catlisting . "</b>", "<b>" . $listings . "</b>"));
include XOOPS_ROOT_PATH . "/modules/mydownloads/footer.php";

?>
