<?php 
// $Id: newlist.php,v 1.2 2004/06/05 09:05:05 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";

include XOOPS_ROOT_PATH . "/header.php";
$xoopsOption['template_main'] = 'mydownloads_newlistindex.html';

global $xoopsDB, $xoopsModule, $xoopsConfig, $xoopsUser, $xoopsModuleConfig;

$groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
$module_id = $xoopsModule->getVar('mid');
$gperm_handler = &xoops_gethandler('groupperm');

$imageheader = imageheader();
$xoopsTpl->assign('imageheader', $imageheader);

$counter = 0;
$allweekdownloads = 0;

while ($counter <= 7-1)
{
    $newdownloaddayRaw = (time() - (86400 * $counter));
    $newdownloadday = date("d-M-Y", $newdownloaddayRaw);
    $newdownloadView = date("F d, Y", $newdownloaddayRaw);
    $newdownloadDB = formatTimestamp($newdownloaddayRaw, "s");
    $totaldownloads = 0;
    $result = $xoopsDB->query("SELECT lid, cid, published, updated FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0");
    while ($myrow = $xoopsDB->fetcharray($result))
    {
        $published = ($myrow['updated'] > 0) ? $myrow['updated'] : $myrow['published'];
        if ($gperm_handler->checkRight('DownCatPerm', $myrow['cid'], $groups, $module_id))
        {
            if ($gperm_handler->checkRight('DownFilePerm', $myrow['lid'], $groups, $module_id))
            {
                if (formatTimestamp($published, "s") == $newdownloadDB)
                {
                    $totaldownloads++;
                } 
            } 
        } 
    } 
    $counter++;
    $allweekdownloads = $allweekdownloads + $totaldownloads;
} 

$counter = 0;
while ($counter <= 30-1)
{
    $newdownloaddayRaw = (time() - (86400 * $counter));
    $newdownloadDB = formatTimestamp($newdownloaddayRaw, "s");
    $totaldownloads = 0;
    $result = $xoopsDB->query("SELECT lid, cid, published, updated FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0");
    while ($myrow = $xoopsDB->fetcharray($result))
    {
        $published = ($myrow['updated'] > 0) ? $myrow['updated'] : $myrow['published'];
        if ($gperm_handler->checkRight('DownCatPerm', $myrow['cid'], $groups, $module_id))
        {
            if ($gperm_handler->checkRight('DownFilePerm', $myrow['lid'], $groups, $module_id))
            {
                if (formatTimestamp($published, "s") == $newdownloadDB)
                {
                    $totaldownloads++;
                } 
            } 
        } 
    } 
    if (!isset($allmonthdownloads)) $allmonthdownloads = 0;;
    $allmonthdownloads = $allmonthdownloads + $totaldownloads;
    $counter++;
} 
$xoopsTpl->assign('allweekdownloads', $allweekdownloads);
$xoopsTpl->assign('allmonthdownloads', $allmonthdownloads);

/**
 * List Last VARIABLE Days of Downloads
 */
if (!isset($HTTP_GET_VARS['newdownloadshowdays']))
{
    $newdownloadshowdays = 7;
} 
else
{
    $newdownloadshowdays = $HTTP_GET_VARS['newdownloadshowdays'];
} 

$xoopsTpl->assign('newdownloadshowdays', $newdownloadshowdays);

$counter = 0;
$allweekdownloads = 0;
while ($counter <= $newdownloadshowdays-1)
{
    $newdownloaddayRaw = (time() - (86400 * $counter));
    $newdownloadday = formatTimestamp($newdownloaddayRaw, "d-M-Y");
    $newdownloadView = formatTimestamp($newdownloaddayRaw, "F d, Y");
    $newdownloadDB = formatTimestamp($newdownloaddayRaw, "s");
    $totaldownloads = 0;

    $result = $xoopsDB->query("SELECT lid, cid, published, updated FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0");
    while ($myrow = $xoopsDB->fetcharray($result))
    {
        $published = ($myrow['updated'] > 0) ? $myrow['updated'] : $myrow['published'];

        if ($gperm_handler->checkRight('DownCatPerm', $myrow['cid'], $groups, $module_id))
        {
            if ($gperm_handler->checkRight('DownFilePerm', $myrow['lid'], $groups, $module_id))
            {
                if (formatTimestamp($myrow['published'], "s") == $newdownloadDB)
                {
                    $totaldownloads++;
                } 
            } 
        } 
    } 
    $counter++;
    $allweekdownloads = $allweekdownloads + $totaldownloads;
    $dailydownloads['newdownloadday'] = $dailydownloads['newdownloadView'] = $newdownloadView;
    $dailydownloads['newdownloaddayRaw'] = $newdownloaddayRaw;
    $dailydownloads['totaldownloads'] = $totaldownloads;
    $xoopsTpl->append('dailydownloads', $dailydownloads);
} 
$counter = 0;
$allmonthdownloads = 0;

$xoopsTpl->assign('lang_newdownload', _MD_NEWDOWNLOADS);
$xoopsTpl->assign('lang_totalnewdownloads', _MD_TOTALNEWDOWNLOADS);
$xoopsTpl->assign('lang_lastweek', _MD_LASTWEEK);
$xoopsTpl->assign('lang_last30days', _MD_LAST30DAYS);
$xoopsTpl->assign('lang_show', _MD_SHOW);
$xoopsTpl->assign('lang_1week', _MD_1WEEK);
$xoopsTpl->assign('lang_2week', _MD_2WEEKS);
$xoopsTpl->assign('lang_30days', _MD_30DAYS);
$xoopsTpl->assign('lang_dtotalforlast', _MD_DTOTALFORLAST);
$xoopsTpl->assign('lang_days', _MD_DAYS);

$xoopsTpl->assign('lang_description', _MD_DESCRIPTIONC);
$xoopsTpl->assign('lang_lastupdate', _MD_LASTUPDATEC);
$xoopsTpl->assign('lang_hits', _MD_HITSC);
$xoopsTpl->assign('lang_ratingc', _MD_RATINGC);
$xoopsTpl->assign('lang_email', _MD_EMAILC);
$xoopsTpl->assign('lang_ratethissite', _MD_RATETHISFILE);
$xoopsTpl->assign('lang_reviewfile' , _MD_REVIEWTHISFILE);
$xoopsTpl->assign('lang_reportbroken', _MD_REPORTBROKEN);
$xoopsTpl->assign('lang_tellafriend', _MD_TELLAFRIEND);
$xoopsTpl->assign('lang_modify', _MD_MODIFY);
$xoopsTpl->assign('lang_version' , _MD_VERSION);
$xoopsTpl->assign('lang_dlnow' , _MD_DLNOW);
$xoopsTpl->assign('lang_category' , _MD_CATEGORYC);
$xoopsTpl->assign('lang_size' , _MD_FILESIZE);
$xoopsTpl->assign('lang_platform' , _MD_SUPPORTEDPLAT);
$xoopsTpl->assign('lang_homepage' , _MD_HOMEPAGE);
$xoopsTpl->assign('lang_comments' , _COMMENTS);
$xoopsTpl->assign('lang_price' , _MD_PRICE);
$xoopsTpl->assign('lang_limits' , _MD_LIMITS);
$xoopsTpl->assign('lang_mirror' , _MD_MIRROR);
$xoopsTpl->assign('lang_downloads' , _MD_DOWNLOADHITS);
$xoopsTpl->assign('lang_license' , _MD_DOWNLICENSE);
$xoopsTpl->assign('lang_publisher' , _MD_PUBLISHER);
$xoopsTpl->assign('lang_reviews' , _MD_REVIEWS);
$xoopsTpl->assign('lang_fulldetails' , _MD_VIEWDETAILS);
$xoopsTpl->assign('lang_sortby', _MD_SORTBY);
$xoopsTpl->assign('lang_latestlistings' , _MD_LATESTLIST);

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " ";
$sql .= "WHERE published > 0 AND published <= " . time() . " 
		AND (expired = 0 OR expired > " . time() . ") AND offline = 0 
		ORDER BY published DESC";
$xoopsTpl->assign('show_categort_title', true);

$result = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'] , 0);
$amount = $xoopsDB->getRowsNum($sql);

while ($down_arr = $xoopsDB->fetchArray($result))
{
    if ($gperm_handler->checkRight('DownFilePerm', $down_arr['lid'], $groups, $xoopsModule->getVar('mid')))
    {
        include XOOPS_ROOT_PATH . "/modules/mydownloads/include/downloadinfo.php";
    } 
} 

include XOOPS_ROOT_PATH . "/modules/mydownloads/footer.php";

?>