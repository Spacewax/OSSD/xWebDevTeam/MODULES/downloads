<?php 
// $Id: review.php,v 1.1 2004/06/05 09:05:05 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
// include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";
$myts = &MyTextSanitizer::getInstance(); // MyTextSanitizer object

Global $xoopsModuleConfig;

if (isset($_POST))
{
    foreach ($_POST as $k => $v)
    {
        ${$k} = $v;
    }
}

if (isset($_GET))
{
    foreach ($_GET as $k => $v)
    {
        ${$k} = $v;
    }
}

if (empty($xoopsUser) && !$xoopsModuleConfig['anonpost'])
{
    redirect_header(XOOPS_URL . "/user.php", 1, _MD_MUSTREGFIRST);
    exit();
}

switch (isset($op) && !empty($op) )
{
    case "list";

        global $xoopsDB, $xoopsModuleConfig, $myts;
        $cid = isset($HTTP_GET_VARS['cid']) ? $HTTP_GET_VARS['cid'] : 0;
        $lid = isset($HTTP_GET_VARS['lid']) ? $HTTP_GET_VARS['lid'] : 0;
        $start = isset($HTTP_GET_VARS['start']) ? intval($HTTP_GET_VARS['start']) : 0;

        include XOOPS_ROOT_PATH . "/header.php";
        $xoopsOption['template_main'] = 'mydownloads_reviews.html';

        $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_indexpage') . " ";
        $head_arr = $xoopsDB->fetchArray($xoopsDB->query($sql));
        $catarray['imageheader'] = imageheader();
        $catarray['letters'] = letters();
        $catarray['toolbar'] = toolbar();
        $xoopsTpl->assign('catarray', $catarray);

        $sql_review = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_reviews') . " WHERE lid = " . intval($lid) . " AND submit = 1 ORDER by date";
        $result_review = $xoopsDB->query($sql_review, 5, $start);
        $result_count = $xoopsDB->query($sql_review);
        $review_amount = $xoopsDB->getRowsNum($result_count);

        $sql = "SELECT title, lid, cid FROM " . $xoopsDB->prefix('mydownloads_downloads') . " WHERE lid = " . $lid . "";
        $down_arr_text = $xoopsDB->fetcharray($xoopsDB->query($sql));
        $down_arr['title'] = $myts->htmlSpecialChars($myts->stripSlashesGPC($down_arr_text['title']));
		$down_arr['cid'] = intval($down_arr_text['cid']);
		$down_arr['lid'] = intval($down_arr_text['lid']);
        
		$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_text') . " WHERE lid = " . $lid . "";
        $down_arr_text = $xoopsDB->fetcharray($xoopsDB->query($sql));
        $down_arr['description'] = $myts->displayTarea($down_arr_text['description'], 1,1,1,1,0);
		$xoopsTpl->assign('down_arr', $down_arr);
		
        while ($arr_review = $xoopsDB->fetchArray($result_review))
        {
            $down_review['review_id'] = intval($arr_review['review_id']);
            $down_review['lid'] = intval($arr_review['lid']);
            $down_review['title'] = $myts->censorstring($arr_review['title']);
            $down_review['title'] = $myts->htmlSpecialChars($myts->stripSlashesGPC($down_review['title']));
            $down_review['review'] = $myts->censorstring($arr_review['review']);
            $down_review['review'] = $myts->displayTarea($down_review['review'], 0, 0, 0, 0, 0);
            $down_review['date'] = formatTimestamp($arr_review['date'], $xoopsModuleConfig['dateformat']);
            $down_review['submitter'] = xoops_getLinkedUnameFromId(intval($arr_review['uid']));

            $review_rating = round(number_format($arr_review['rated'], 0) / 2);
            $rateimg = "rate$review_rating.gif";
            $down_review['rated_img'] = $rateimg;
            $xoopsTpl->append('down_review', $down_review);
        }

        $xoopsTpl->assign('lang_pages', _MD_PAGES);
        $xoopsTpl->assign('lang_title', _MD_TITLE);
        $xoopsTpl->assign('lang_rating', _MD_RATEDRESOURCE);
        $xoopsTpl->assign('lang_reviewer', _MD_REVIEWER);
        $xoopsTpl->assign('lang_review_title', _MD_REVIEWTITLE);
		$xoopsTpl->assign('lang_review_found', sprintf(_MD_REVIEWTOTAL, $review_amount));
		$xoopsTpl->assign('lang_reviewfile' , _MD_REVIEWTHISFILE2);
		include_once XOOPS_ROOT_PATH . '/class/pagenav.php';

        $pagenav = new XoopsPageNav($review_amount, 5 , $start, 'start', 'op=list&cid=' . $cid . '&lid=' . $lid . '', 1);
        $navbar['navbar'] = $pagenav->renderNav();
        $xoopsTpl->assign('navbar', $navbar);

        include XOOPS_ROOT_PATH . '/footer.php';
        break;

    case "default";
    default:
        if (!empty($_POST['submit']))
        {
            $uid = !empty($xoopsUser) ? $xoopsUser->getVar('uid') : 0;

            $title = $myts->addslashes(trim($_POST["title"]));
            $review = $myts->addslashes(trim($_POST["review"]));
            $lid = intval(trim($_POST["lid"]));
            $rated = intval(trim($_POST["rated"]));
            $date = time();
            $submit = ($xoopsModuleConfig['autoapprove']) ? 1 : 0 ;

            $newid = $xoopsDB->genId($xoopsDB->prefix("mydownloads_reviews") . "_review_id");
            $sql = "INSERT INTO " . $xoopsDB->prefix("mydownloads_reviews") . " (review_id, lid, title, review, submit, date, uid, rated) VALUES ('', $lid, '$title', '$review', '$submit', $date, $uid, $rated)";
            $result = $xoopsDB->query($sql);

            if (!$result)
            {
                $error = _AM_WF_ERROR_CREATCHANNEL . $sql;
                trigger_error($error, E_USER_ERROR);
            }
            else
            {
                if ($xoopsModuleConfig['autoapprove'] == 1)
                {
                    redirect_header("index.php", 2, _MD_REVIEWINFO . "<br />" . _MD_ISAPPROVED . "");
                }
                else
                {
                    redirect_header("index.php", 2, _MD_REVIEWINFO . "<br />" . _MD_ISNOTAPPROVED . "");
                }
            }
        }
        else
        {
            include XOOPS_ROOT_PATH . "/header.php";
            include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";

            echo "<p><div align = 'center'>" . imageheader() . "</div></p>";
            echo "<div align = 'left'>" . _MD_REV__SNEWMNAMEDESC . "</div>";

            $sform = new XoopsThemeForm(_MD_REV_SUBMITREV, "reviewform", xoops_getenv('PHP_SELF'));
            $sform->addElement(new XoopsFormText(_MD_REV_TITLE, 'title', 30, 40), true);
            $rating_select = new XoopsFormSelect(_MD_REV_RATING, "rated", '');
            $rating_select->addOptionArray(array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10));
            $sform->addElement($rating_select);
            $sform->addElement(new XoopsFormDhtmlTextArea(_MD_REV_DESCRIPTION, 'review', '', 15, 60), true);
            $sform->addElement(new XoopsFormHidden("lid", $_GET['lid']));
            $button_tray = new XoopsFormElementTray('', '');
            $button_tray->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
            $sform->addElement($button_tray);
            $sform->display();
            include XOOPS_ROOT_PATH . '/footer.php';
        }
}

?>
