<?php 
// $Id: submit.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";
include_once XOOPS_ROOT_PATH . "/include/xoopscodes.php";

$myts = &MyTextSanitizer::getInstance(); // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");

Global $xoopsModuleConfig;

foreach ($_POST as $k => $v)
{
    ${$k} = $v;
}

foreach ($_GET as $k => $v)
{
    ${$k} = $v;
}

if (empty($xoopsUser) && !$xoopsModuleConfig['anonpost'])
{
    redirect_header(XOOPS_URL . "/user.php", 1, _MD_MUSTREGFIRST);
    exit();
}

if (!$xoopsModuleConfig['submissions'])
{
    redirect_header("index.php", 1, _MD_NOTALLOWESTOSUBMIT);
    exit();
}

if (isset($_POST['submit']) && !empty($_POST['submit']))
{
    $submitter = !empty($xoopsUser) ? $xoopsUser->getVar('uid') : 0;
    $notify = !empty($_POST['notify']) ? 1 : 0;
	$lid = (!empty($_POST['lid'])) ? intval($_POST['lid']) : 0 ;
    $cid = (!empty($_POST['cid'])) ? intval($_POST['cid']) : 0 ;
	
	if ( empty($HTTP_POST_FILES['userfile']['name']) && $HTTP_POST_VARS["url"] && $HTTP_POST_VARS["url"] != "" && $HTTP_POST_VARS["url"] != "http://")
    {
    	$url = ($HTTP_POST_VARS["url"] != "http://") ? $myts->addslashes(formatURL($HTTP_POST_VARS["url"])) : '';
        $size = ((empty($size) || !is_numeric($size))) ? $myts->addslashes($HTTP_POST_VARS["size"]) : 0;
    	if ($fp = @fopen("$url", "r"))
    	{
			
		} else {
			redirect_header( xoops_getenv('PHP_SELF'), 1 , _AM_FILENOTEXIST);
		}
		@fclose($fp);
	}
    else
    {
		
		Global $HTTP_POST_FILES;
		$down = uploading($HTTP_POST_FILES,'', 0, 0);         
    	$url = formatURL($HTTP_POST_VARS["url"]);
		$size = intval($HTTP_POST_VARS["size"]);
	}	
	/*
    if (($_POST["URL"]) and ($_POST["URL"] != "") and ($_POST["URL"] != "http://"))
    {
        $url = $myts->makeTboxData4Save(formatURL($_POST["URL"]));
        $size = intval($_POST["size"]);
    }
    else
    {
        Global $HTTP_POST_FILES, $xoopsModuleConfig;
        include_once XOOPS_ROOT_PATH . "/class/uploader.php";

        $allowed_mimetypes = (is_array($xoopsModuleConfig['downmimetypes'])) ? $xoopsModuleConfig['downmimetypes'] : '';
        $maxfilesize = $xoopsModuleConfig['maxfilesize'];
        $maxfilewidth = $xoopsModuleConfig['maximgwidth'];
        $maxfileheight = $xoopsModuleConfig['maximgheight'];
        $uploaddir = XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/";
        $url = XOOPS_URL . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name']);
        $uploader = new XoopsMediaUploader($uploaddir, $allowed_mimetypes, $maxfilesize, $maxfilewidth, $maxfileheight);

        if ($uploader->fetchMedia(strtolower($_POST['xoops_upload_file'][0])))
        {
            if (!$uploader->upload())
            {
                $error = $uploader->getErrors();
                redirect_header("submit.php", 1, $error);
            }
            else
            {
                if (is_file(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name'])))
                {
                    $size = filesize(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name']));
                }
            }
        }
        else
        {
            $error = $uploader->getErrors();
            redirect_header("submit.php", 1, $error);
        }
    }*/

    $title = $myts->makeTboxData4Save($_POST["title"]);
    $homepage = $myts->makeTboxData4Save($_POST["homepage"]);
    $version = $myts->makeTboxData4Save($_POST["version"]);
    $platform = $myts->makeTboxData4Save($_POST["platform"]);
    $description = $myts->makeTareaData4Save($_POST["message"]);

    $price = $myts->addslashes(trim($HTTP_POST_VARS["price"]));
    $mirror = $myts->addslashes(formatURL(trim($HTTP_POST_VARS["mirror"])));
    $license = $myts->addslashes(trim($HTTP_POST_VARS["license"]));
    $paypalemail = $myts->addslashes($HTTP_POST_VARS["paypalemail"]);
    $features = $myts->addslashes(trim($HTTP_POST_VARS["features"]));
    $requirements = $myts->addslashes(trim($HTTP_POST_VARS["requirements"]));
    $forumid = (isset($HTTP_POST_VARS["forumid"]) && $HTTP_POST_VARS["forumid"] > 0) ? intval($HTTP_POST_VARS["forumid"]) : 0;
    $limitations = (isset($HTTP_POST_VARS["limitations"])) ? $myts->addslashes($HTTP_POST_VARS["limitations"]) : '';
    $dhistory = (isset($HTTP_POST_VARS["dhistory"])) ? $myts->addslashes($HTTP_POST_VARS["dhistory"]) : '';
    $dhistoryhistory = (isset($HTTP_POST_VARS["dhistoryaddedd"])) ? $myts->addslashes($HTTP_POST_VARS["dhistoryaddedd"]) : '';    
	if ($lid > 0 && !empty($dhistoryhistory))
	{
		$dhistory = $dhistory."\n\n"; 
		$time = time();
		$dhistory .= "<b>".formatTimestamp($time, $xoopsModuleConfig['dateformat'])."</b>\n\n";
		$dhistory .= $dhistoryhistory;
	}
	$updated = (isset($_POST['up_dated']) && $_POST['up_dated'] == 0) ? 0 : time();
    $offline = (isset($_POST['offline']) && $_POST['offline'] == 1) ? 1 : 0;
    $date = time();
    $publishdate = 0;

    $logourl = '';
    if ((isset($HTTP_POST_FILES['logourl']['name']) && !empty($HTTP_POST_FILES['logourl']['name'])))
    {
        $allowed_mimetypes = $allowed_mimetypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png');
        $maxfilesize = $xoopsModuleConfig['maxfilesize'];
        $maxfilewidth = $xoopsModuleConfig['maximgwidth'];
        $maxfileheight = $xoopsModuleConfig['maximgheight'];
        $uploaddir = XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['screenshots'] . "/";
        $logourl = strtolower($HTTP_POST_FILES['logourl']['name']);

        $uploader = new XoopsMediaUploader($uploaddir, $allowed_mimetypes, $maxfilesize, $maxfilewidth, $maxfileheight);

        if ($uploader->fetchMedia($_POST['xoops_upload_file'][1]))
        {
            if (!$uploader->upload())
            {
                $errors = $uploader->getErrors();
                redirect_header("index.php?op=downloadsConfigMenu", 1, $errors);
            }
            else
            {
            }
        }
        else
        {
            $errors = $uploader->getErrors();
            redirect_header("index.php?op=downloadsConfigMenu", 1, $errors);
        }
    }

    $newid = $xoopsDB->genId($xoopsDB->prefix("mydownloads_downloads") . "_lid_seq");
    $status = ($xoopsModuleConfig['autoapprove'] == 1) ? $xoopsModuleConfig['autoapprove'] : 0 ;

    //$sql = sprintf("INSERT INTO %s (lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits, rating, votes, comments) VALUES (%u, %u, '%s', '%s', '%s', '%s', %u, '%s', '%s', %u, %u, %u, %u, %u, %u, %u)", 
	//$xoopsDB->prefix("mydownloads_downloads"), $newid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $submitter, $status, $date, 0, 0, 0, 0);
    $sql = "INSERT INTO " . $xoopsDB->prefix("mydownloads_downloads") . " 
			(lid, cid, title, url, homepage, version, size, platform, logourl, submitter, status, date, hits, rating, 
			votes, comments, price, mirror, license, paypalemail, features, requirements, homepagetitle, forumid,
			limitations, dhistory, published, expired, updated, offline) VALUES 
			('', $cid, '$title', '$url', '$homepage', '$version', $size, '$platform', '$logourl', '$submitter', 0, 
			'$date', 0, 0, 0, 0, '$price', '$mirror', '$license', '$paypalemail', '$features', '$requirements', 
			'$homepagetitle', '$forumid', '$limitations', '$dhistory', '$publishdate', 0, '$updated', '$offline')";
  

    $result = $xoopsDB->query($sql);
    
	if (!$result){
	echo "oh shit";
	
	}
	
	
	if ($newid == 0)
    {
        $newid = $xoopsDB->getInsertId();
    }
    
	
	$sql = sprintf("INSERT INTO %s (lid, description) VALUES (%u, '%s')", $xoopsDB->prefix("mydownloads_text"), $newid, $description);
    $xoopsDB->query($sql) or $eh->show("0013"); 
    // Notify of new link (anywhere) and new link in category
    $notification_handler = &xoops_gethandler('notification');
    $tags = array();
    $tags['FILE_NAME'] = $title;
    $tags['FILE_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/singlefile.php?cid=' . $cid . '&lid=' . $newid;
    $sql = "SELECT title FROM " . $xoopsDB->prefix("mydownloads_cat") . " WHERE cid=" . $cid;
    $result = $xoopsDB->query($sql);
    $row = $xoopsDB->fetchArray($result);
    $tags['CATEGORY_NAME'] = $row['title'];
    $tags['CATEGORY_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/viewcat.php?cid=' . $cid;

    if ($xoopsModuleConfig['autoapprove'] == 1)
    {
        $notification_handler->triggerEvent('global', 0, 'new_file', $tags);
        $notification_handler->triggerEvent('category', $cid, 'new_file', $tags);
        redirect_header("index.php", 2, _MD_RECEIVED . "<br />" . _MD_ISAPPROVED . "");
    }
    else
    {
        $tags['WAITINGFILES_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/admin/index.php?op=listNewDownloads';
        $notification_handler->triggerEvent('global', 0, 'file_submit', $tags);
        $notification_handler->triggerEvent('category', $cid, 'file_submit', $tags);
        if ($notify)
        {
            include_once XOOPS_ROOT_PATH . '/include/notification_constants.php';
            $notification_handler->subscribe('file', $newid, 'approve', XOOPS_NOTIFICATION_MODE_SENDONCETHENDELETE);
        }
        redirect_header("index.php", 2, _MD_RECEIVED);
    }
    exit();
}
else
{
    Global $HTTP_POST_FILES, $xoopsModuleConfig;

    $cid = 0;
    $title = '';
    $url = 'http://';
    $homepage = 'http://';
    $homepagetitle = '';
    $version = '';
    $size = 0;
    $platform = '';
    $logourl = '';
    $price = 0;
    $description = '';
    $mirror = 'http://';
    $license = '';
    $paypalemail = '';
    $features = '';
    $requirements = '';
    $forumid = 0;
    $limitations = '';
    $dhistory = '';
    $status = 0;
    $is_updated = 0;
    $offline = 0;
    $published = 0;
    $expired = 0;
    $updated = 0;

    if ($xoopsModuleConfig['showdisclaimer'] && !isset($_GET['agree']))
    {
        include XOOPS_ROOT_PATH . "/header.php";
        echo "<p><div align = 'center'>" . imageheader() . "</div></p>";
        echo "<h4 align = 'left'>" . _MD_DISCLAIMERAGREEMENT . "</h4>";
        echo "<p><div>" . $myts->displayTarea($xoopsModuleConfig['disclaimer'], 0, 1, 1, 1, 1) . "</div></p>";
        echo "<form action='submit.php' method='post'>";
        echo "<center><b>" . _MD_DOYOUAGREE . "</b><br /><br />";
        echo "<input type = 'button' onclick = 'location=\"submit.php?agree=1\"' class='formButton' value='" . _MD_AGREE . "' />";
        echo "&nbsp;";
        echo "<input type = 'button' onclick = 'location=\"index.php\"' class='formButton' value='" . _CANCEL . "' />";
        echo "</form>";
        include XOOPS_ROOT_PATH . "/footer.php";
        exit();
    }

    include XOOPS_ROOT_PATH . "/header.php";
    include_once XOOPS_ROOT_PATH . "/class/xoopstree.php" ;
    include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";

    echo "<p><div align = 'center'>" . imageheader() . "</div></p>";
    echo "<div align = 'left'>" . _MD_SUB_SNEWMNAMEDESC . "</div>";

    $sform = new XoopsThemeForm(_MD_SUBMITCATHEAD, "storyform", xoops_getenv('PHP_SELF'));
    $sform->setExtra('enctype="multipart/form-data"');
    $sform->addElement(new XoopsFormText(_MD_FILETITLE, 'title', 50, 80, $title), true);
    $sform->addElement(new XoopsFormText(_MD_DLURL, 'url', 50, 80, "http://"), false);
    if ($xoopsModuleConfig['useruploads'])
    {
        $sform->addElement(new XoopsFormFile(_MD_DLURL, 'userfile', $xoopsModuleConfig['maxfilesize']), false);
    }
    $sform->addElement(new XoopsFormText(_MD_MIRROR, 'mirror', 50, 80, $mirror), false);

    $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat "), "cid ", "pid");
    $mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
    ob_start();
    $sform->addElement(new XoopsFormHidden('cid', $cid));
    $mytree->makeMySelBox('title', 'title', $cid, 0);
    $sform->addElement(new XoopsFormLabel(_MD_CATEGORYC, ob_get_contents()));
    ob_end_clean();

    $sform->addElement(new XoopsFormText(_MD_HOMEPAGETITLEC, 'homepagetitle', 50, 80, $homepagetitle), false);
    $sform->addElement(new XoopsFormText(_MD_HOMEPAGEC, 'homepage', 50, 80, $homepage), false);
    $sform->addElement(new XoopsFormText(_MD_VERSIONC, 'version', 10, 20, $version), false);
		//$version_array = $xoopsModuleConfig['versiontypes'];
        //$version_select = new XoopsFormSelect('', 'versiontypes', $versiontypes, '', '', 0);
       // $version_select->addOptionArray($version_array);
        //$version_tray = new XoopsFormElementTray(_AM_PLATFORMC, '&nbsp;');
        //$version_tray->addElement($version_select);
        //$sform->addElement($version_tray);

    $sform->addElement(new XoopsFormText(_MD_FILESIZEC, 'size', 10, 20, $size), false);

    $platform_array = $xoopsModuleConfig['platform'];
    $platform_select = new XoopsFormSelect('', 'platform', $platform, '', '', 0);
    $platform_select->addOptionArray($platform_array);
    $platform_tray = new XoopsFormElementTray(_MD_PLATFORMC, '&nbsp;');
    $platform_tray->addElement($platform_select);
    $sform->addElement($platform_tray);

    $license_array = $xoopsModuleConfig['license'];
    $license_select = new XoopsFormSelect('', 'license', $license, '', '', 0);
    $license_select->addOptionArray($license_array);
    $license_tray = new XoopsFormElementTray(_MD_LICENCEC, '&nbsp;');
    $license_tray->addElement($license_select);
    $sform->addElement($license_tray);

    $limitations_array = $xoopsModuleConfig['limitations'];
    $limitations_select = new XoopsFormSelect('', 'limitations', $limitations, '', '', 0);
    $limitations_select->addOptionArray($limitations_array);
    $limitations_tray = new XoopsFormElementTray(_MD_LIMITATIONS, '&nbsp;');
    $limitations_tray->addElement($limitations_select);
    $sform->addElement($limitations_tray);
	$sform->addElement(new XoopsFormText(_MD_PRICEC, 'price', 10, 20, $price), false);
    $sform->addElement(new XoopsFormDhtmlTextArea(_MD_DESCRIPTION, 'message', '', 15, 60), true);
    $sform->addElement(new XoopsFormTextArea(_MD_KEYFEATURESC, 'features', $features, 7, 60), false);
    $sform->addElement(new XoopsFormTextArea(_MD_REQUIREMENTSC, 'requirements', $requirements, 7, 60), false);
    $sform->addElement(new XoopsFormTextArea(_MD_HISTORYC, 'dhistory', $dhistory, 7, 60), false);
    if ($lid && !empty($dhistory))
    {
        $sform->addElement(new XoopsFormTextArea(_AM_HISTORYC, 'dhistoryaddedd', "", 7, 60), false);
    }
    $sform->addElement(new XoopsFormFile(_MD_DUPLOADSCRSHOT, 'logourl', $xoopsModuleConfig['maxfilesize']), false);

    $option_tray = new XoopsFormElementTray(_MD_OPTIONS, '<br />');
    $notify_checkbox = new XoopsFormCheckBox('', 'notifypub');
    $notify_checkbox->addOption(1, _MD_NOTIFYAPPROVE);
    $option_tray->addElement($notify_checkbox);
    $sform->addElement($option_tray);
    $button_tray = new XoopsFormElementTray('', '');

    $button_tray->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
    $sform->addElement($button_tray);
    $sform->display();
    include XOOPS_ROOT_PATH . '/footer.php';
}

?>
