<?php 
// $Id: visit.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------ //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";

global $xoopsUser;

$myts = &MyTextSanitizer::getInstance(); // MyTextSanitizer object
$agreed = (isset($_GET['agree'])) ? $_GET['agree'] : 0;

if ($xoopsModuleConfig['showdisclaimer'] && $agreed == 0)
{
    $lid = intval($_GET['lid']);
    $cid = intval($_GET['cid']);

    include XOOPS_ROOT_PATH . "/header.php";
    echo "<div align = 'center'>" . imageheader() . "</div>";
    echo "<h4 align = 'left'>" . _MD_DISCLAIMERAGREEMENT . "</h4>";
    echo "<div>" . $myts->displayTarea($xoopsModuleConfig['downdisclaimer'], 0, 1, 1, 1, 1) . "</div>";
    echo "<form action='visit.php' method='post'>";
    echo "<center><b>" . _MD_DOYOUAGREE . "</b><br /><br />";
    echo "<input type = 'button' onclick = 'location=\"visit.php?agree=1&lid=$lid&cid=$cid\"' class='formButton' value='" . _MD_AGREE . "' />";
    echo "&nbsp;";
    echo "<input type = 'button' onclick = 'location=\"index.php\"' class='formButton' value='" . _CANCEL . "' />";
    echo "<input type='hidden' name='lid' value='1'";
    echo "<input type='hidden' name='cid' value='1'";
    echo "</form>";
    include XOOPS_ROOT_PATH . "/footer.php";
    exit();
}
else
{
    $lid = intval($_GET['lid']);
    $cid = intval($_GET['cid']);

    if ($xoopsModuleConfig['check_host'])
    {
        $goodhost = 0;
        $referer = parse_url(xoops_getenv('HTTP_REFERER'));
        $referer_host = $referer['host'];
        foreach ($xoopsModuleConfig['referers'] as $ref)
        {
            if (!empty($ref) && preg_match("/" . $ref . "/i", $referer_host))
            {
                $goodhost = "1";
                break;
            }
        }
        if (!$goodhost)
        {
            redirect_header(XOOPS_URL . "/modules/mydownloads/singlefile.php?cid=$cid&amp;lid=$lid", 20, _MD_NOPERMISETOLINK);
            exit();
        }
    }

    $isadmin = (!empty($xoopsUser) && $xoopsUser->isAdmin($xoopsModule->mid())) ? true : false;

    if ($isadmin == false)
    {
        $sql = sprintf("UPDATE " . $xoopsDB->prefix("mydownloads_downloads") . " SET hits = hits+1 WHERE lid =$lid");
        $xoopsDB->queryF($sql);
    }

    $result = $xoopsDB->query("SELECT url FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid");
    list($url) = $xoopsDB->fetchRow($result);

    if ($fp = @fopen("$url", "r"))
    {
        include XOOPS_ROOT_PATH . "/header.php";
        $url = $myts->oopsHtmlSpecialChars($url);
        if (!headers_sent())
        {
            echo "<div align = 'center'>" . imageheader() . "</div>";
            echo "<h4><img src=\"".XOOPS_URL."/modules/mydownloads/images/icon/downloads.gif\" align=\"absmiddle\"> " . _MD_DOWNINPROGRESS . "</h4>";
            echo "<div>" . _MD_DOWNSTARTINSEC . "</div><br />";
            echo "<div>" . _MD_DOWNNOTSTART . "";
            echo "<a href=" . $url . ">" . _MD_CLICKHERE . "</a>.";
            echo "</div>";
            header("Refresh: 3; url=$url");
            Header("Cache-control: private");
        } 
        // echo "<html><head><meta http-equiv=\"Refresh\" content=\"0; URL=" . $myts->oopsHtmlSpecialChars($url) . "\"></meta></head><body></body></html>";
        @fclose($fp);
        include XOOPS_ROOT_PATH . "/footer.php";
        exit();
    }
    else
    {
        include XOOPS_ROOT_PATH . "/header.php";
        echo "<div align = 'center'>" . imageheader() . "</div>";
        echo "<h4 align = 'left'>" . _MD_BROKENFILE . "</h4>";
        echo "<div>" . _MD_PLEASEREPORT . "";
        echo "<a href=\"" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/brokenfile.php?lid=$lid\">" . _MD_CLICKHERE . "</a>";
        echo "</div>";
        include XOOPS_ROOT_PATH . "/footer.php";
    }
    exit();
}

?>
