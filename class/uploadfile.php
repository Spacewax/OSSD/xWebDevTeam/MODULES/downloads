<?php

include_once 'common.php';
include_once WFS_ROOT_PATH . '/class/mimetype.php';
include_once WFS_ROOT_PATH . '/include/groupaccess.php';

mt_srand((double) microtime() * 1000000);

class UploadFile
{
    var $filepath;
	var $fieldName;
    var $fileName;
    var $filesize;
    var $minetype;
    var $originalName;
    var $ext;
    var $allowedMinetype;
    var $bannedMinetype;
    var $maxImageWidth;
    var $maxImageHight;
    var $maxFilesize; // Byte
    var $mode; // file mode of Unix Style
    var $stripSpaces; // 0 or 1
    var $stripChars; // 0 or 1
    var $bannedChars;
    var $addExt; // 0 or 1
    var $errors = array();

    function UploadFile ($fieldName = "uploadfile")
    {

        $this->fieldName = "uploadfile";
        $this->allowedMinetype = array();
        $this->bannedMinetype = array();
        $this->maxImageWidth = 0;
        $this->maxImageHight = 0;
        $this->maxFilesize = 2097152;
        $this->addExt = 1;
        $this->mode = 644;
        $this->stripSpaces = 1;
        $this->bannedChars = 1;
        $this->StripChars = "";

        if (is_array($fieldName))
        {
            foreach($fieldName as $key => $value)
            {
                $this->$key = $value;
            }
        }
        else
        {
            $this->fieldName = $fieldName;
        }
    }
    /**
     * setup vars for this upload else default is used
     */
    function setFilePath($value)
    {
        $this->filePath = $value;
    }
	
    function setAllowedMinetype($value)
    {
        $this->allowedMinetype = $value;
    }

    function setBannedMinetype($value)
    {
        $this->bannedMinetype = $value;
    }

    function setMaxImageWidth($value)
    {
        $this->maxImageWidth = intval($value);
    }

    function setMaxImageHight($value)
    {
        $this->maxImageHight = intval($value);
    }

    function setMaxFilesize($value)
    {
        $this->maxFilesize = intval($value);
    }

    function setAddExt($value)
    {
        $this->addExt = $value;
    }

    function setMode($value)
    {
        $this->mode = $value;
    }

    function setStripSpaces($value)
    {
        $this->stripSpaces = $value;
    }

    function setBannedChar($value)
    {
        $this->bannedChars = $value;
    }

    function setStripChar($value)
    {
        $this->stripChars = $value;
    }

    function loadPostVars()
    {
        global $HTTP_POST_FILES;
        
        if (!isset($HTTP_POST_FILES[$this->fieldName]))
        {
            $this->setErrors(_AM_FILENONAMECHOOSEN);
            return false;
        }
        else
        {
            $this->fileName = $HTTP_POST_FILES[$this->fieldName]['tmp_name'];
            $this->fileSize = $HTTP_POST_FILES[$this->fieldName]['size'];
            $this->mineType = $HTTP_POST_FILES[$this->fieldName]['type'];
			$this->originalName = $HTTP_POST_FILES[$this->fieldName]['name'];
			$this->ext = ltrim(strrchr($this->originalName, '.'), '.');

            if (empty($this->originalName))
            {
                $this->setErrors(_AM_FILENONAMECHOOSEN);
                return false;
            }
            if (!file_exists(WFS_FILE_PATH) || !is_dir(WFS_FILE_PATH))
            {
                $this->setErrors(sprintf(_AM_FILEPATHNOTWRITE, WFS_FILE_PATH));
            }
            if (!is_writable(WFS_FILE_PATH) && !chmod(WFS_FILE_PATH, 0777))
            {
                $this->setErrors(sprintf(_AM_FILEPATHNOTWRITE, WFS_FILE_PATH));
            }
            if ($this->fileSize == 0)
            {
                $this->setErrors(sprintf(_AM_INVALIDFILESIZE, $this->fileSize));
            }
            if ($this->fileName == '' || !is_uploaded_file($this->fileName))
            {
                $this->setErrors(_AM_ERRORUPLOADINGFILE);
            }
            return true;
        }
    }

    function getFileName()
    {
        return $this->fileName;
    }

    function getFilesize()
    {
        return $this->fileSize;
    }

    function getMinetype()
    {
		return $this->mineType;
    }

    function getOriginalName()
    {
        return $this->originalName;
    }

    function getExt()
    {
        $mimetype = new mimetype();

		$this->ext = $mimetype->getExt($this->mineType);
		return $this->ext;
    }

    function readFile()
    {
        if (!is_readable($this->fileName))
        {
            $this->setErrors(_AM_FILEISNOTREADABLE);
            return FALSE;
        }
        return file($this->fileName);
    } 
    // File checking from here
    /**
     * UploadFile::isAllowedImageSize()
     * 
     * @return 
     */
    function isAllowedImageSize()
    {
        global $xoopsModuleConfig, $xoopsUser;

        if ($xoopsUser->isAdmin() && $xoopsModuleConfig['noimgsizecheck']) return TRUE;
        if ($this->maxImageWidth == 0 || $this->maxImageHight == 0) return TRUE;
		
		$size = GetImageSize($this->fileName);
        if (($size[0] > $this->maxImageWidth) || ($size[1] > $this->maxImageHight))
        {
            $this->setErrors(sprintf(_AM_IMGESIZETOBIG, $xoopsModuleConfig['imgheight'], $xoopsModuleConfig['imgwidth'], $size[0], $size[1]));
            return false;
        }
        return true;
    }

    /**
     * UploadFile::isAllowedFileSize()
     * 
     * @return 
     */
    function isAllowedFileSize()
    {
        global $xoopsModuleConfig, $xoopsUser; 
        if ($xoopsUser->isAdmin() && $xoopsModuleConfig['nomaxfilesize']) return TRUE;
        if ($this->fileSize > $this->maxFilesize)
        {
            $this->setErrors(sprintf(_AM_FILESIZEGRTMAX, myfilesize($this->fileSize), $this->fileSize, myfilesize($this->maxFilesize), $this->maxFilesize));
            return false;
        }
        return true;
    }

    /**
     * UploadFile::isAllowedMineType()
     * 
     * @return 
     */
    function isAllowedMineType()
    {
        global $xoopsModuleConfig, $xoopsUser; 
        if ($xoopsUser->isAdmin() && $xoopsModuleConfig['adminmimecheck']) return TRUE;
        $mimetype = new mimetype();
        foreach($this->allowedMinetype as $type )
        {
			if ($this->mineType == $mimetype->privFindType($type))
            {
                return true;
            }
        }
        $this->setErrors(sprintf(_AM_NOTALLOWEDMINETYPE, $this->mineType, $this->ext) );
        return false;
    }

    /**
     * UploadFile::isNoOverwrite()
     * 
     * @return 
     */
    function isNoOverwrite()
    {
        if ($this->fileSize > $this->maxFilesize)
        {
            return false;
        }
        return false;
    }

    function isAllowedChars()
    {
		switch ($this->bannedChars)
        {
            case 0: 
                // strict
                $restriction = '/[^a-zA-Z0-9\_\-]/';
                break;
            case 1: 
                // medium
                $restriction = '/[^a-zA-Z0-9\_\-\<\>\,\.\$\%\#\@\!\\\'\"]/';
                break;
            case 2: 
                // loose
                $restriction = '/[\000-\040]/';
                break;
        }
		if (preg_match($restriction, $this->originalName))
        {
            $this->setErrors(_AM_INVALIDCHARCS);
            return false;
        }
        return true;
    }

    function isRealExt()
    {
        $mimetype = new mimetype();

		$this->realext = $mimetype->getExt($this->mineType, $this->ext);
		if ($this->realext != $this->ext)
		{
            $this->setErrors(sprintf(_AM_EXTNOTSAME, $this->mineType, $this->ext) );
            return false;		
		}
		return true;
    }	
	
    /**
     * HTML OUTPUT Will change to use XoopsFormClass instead
     */
    function formStart($action = "./", $name = "", $extra = "")
    {
        $ret = "<form enctype='multipart/form-data' method='post'";
        $ret .= " action='" . $action . "'";
        if (!empty($name)) $ret .= " name='" . $name . "'" . " id='" . $name . "'";
        if (!empty($extra)) $ret .= " " . $extra;
        $ret .= ">";
        return $ret;
    }

    function formMax()
    { 
        // return true;
    }

    function formField()
    {
        return "<input type='file' name='" . $this->fieldName . "' id='" . $this->fieldName . "'>";
    }

    function formSubmit($value = "UPLOAD", $name = "", $extra = "")
    {
        $ret = "<input type='submit' value='" . $value . "'";
        if (!empty($name)) $ret .= " name='" . $name . "' id='" . $name . "'";
        if (!empty($extra)) $ret .= " " . $extra;
        $ret .= ">";

        return $ret;
    }

    function formEnd()
    {
        return "</form>";
    }
    /**
     * End of HTML output
     */

    /**
     * UploadFile::doUpload()
     * 
     * @param  $distfilename 
     * @return 
     */
    function doUpload($distfilename)
    {
        Global $xoopsModuleConfig, $xoopsUser;
		
		$this->isRealExt();
		$this->isAllowedMineType();
        $this->isAllowedImageSize();
        $this->isAllowedFileSize();
        $this->isAllowedChars();

		//echo $distfilename;
		
		if (!empty($this->ext) && $this->addExt == 1) $distfilename .= "." . $this->ext;		
		
        if ($this->stripSpaces) {
			$distfilename = preg_replace("/\s/", "", $distfilename);
			$this->originalName = preg_replace("/\s/", "", $this->originalName);
        }

		if (!empty($this->stripChars))
		{
			 $distfilename = preg_replace("/".$this->stripChars."/", "", $distfilename);
			 $this->originalName = preg_replace("/".$this->stripChars."/", "", $this->originalName);
		}

		if (count($this->errors) > 0)
        {
            return false;
        }
	
        if (!$this->doMove($this->filePath."/".$distfilename))
        {
            $this->setErrors(sprintf(_AM_FAILEDUPLOADING, $this->fileName));
            return false;
        }
        return true;
    }

    function doMove($distfilename)
    {
        if (!move_uploaded_file($this->fileName, $distfilename)) return false;
        if (!empty($this->mode) && is_numeric($this->mode)) chmod($distfilename, octdec($this->mode)) ;
        touch($distfilename);
		$this->fileName = $distfilename;
        return $distfilename;
    }

    /**
     * UploadFile::doUploadToRandumFile()
     * 
     * @param  $distpath 
     * @param string $prefix 
     * @return 
     */
    function doUploadToRandumFile($prefix = "")
    {
        //$ext = (!empty($this->ext) && $this->addExt) ? "." . $this->ext : "";

        for($i = 0; $i < 10; $i++)
        {
            $temp_filename = $prefix . mt_rand(100000, 999999);
            return $this->doUpload($temp_filename);
        }
        return false;
    }

    /**
     * UploadFile::doUploadImage()
     * 
     * @param  $distpath 
     * @param string $filename 
     * @param integer $exti 
     * @return 
     */
    function doUploadImage($temp_filename = "", $exti = 0)
    { 
        // Global $xoopsModuleConfig, $xoopsModule;
        //if (!empty($temp_filename)) $filename = strtolower($temp_filename);

        //$ext = "";
        //$ext = $this->setAddExt($exti);
        return $this->doUpload($temp_filename);
    }

    function setErrors($error)
    {
        $this->errors[] = trim($error);
    }

    /**
     * UploadFile::getErrors()
     * 
     * @param boolean $ashtml 
     * @return 
     */
    function &getErrors($ashtml = true)
    {
        $ret = '';
        if (count($this->errors) > 0)
        {
            $ret = _AM_ERRORSRETURNED;
            foreach ($this->errors as $error)
            {
                $ret .= $error . '<br /><br />';
            }
            $ret .= "<br />";
        }
        return $ret;
    }
}

?>