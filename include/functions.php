<?php 
// $Id: functions.php,v 1.2 2004/06/05 09:05:05 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------ //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
function toolbar()
{
    global $xoopsModuleConfig, $xoopsUser;

    //$submissions = 0;
    $submissions = ($xoopsModuleConfig['submissions']) ? 1 : 0;
    if (!is_object($xoopsUser))
    {
        $submissions = ($xoopsModuleConfig['anonpost']) ? 1 : 0;
    }
    $toolbar = "[ ";
    if ($submissions == 1)
    {
        $toolbar .= "<a href='submit.php'>" . _MD_SUBMITDOWNLOAD . "</a> | ";
    }
    //$toolbar .= "<a href='newlist.php'>" . _MD_LATESTLIST . "</a> | <a href='topten.php?hit=1'>" . _MD_PLATFORM . "</a> | <a href='topten.php?list=hit'>" . _MD_POPULARITY . "</a> | <a href='topten.php?list=rate'>" . _MD_TOPRATED . "</a> ]";
    $toolbar .= "<a href='newlist.php'>" . _MD_LATESTLIST . "</a> | <a href='topten.php?list=hit'>" . _MD_POPULARITY . "</a> | <a href='topten.php?list=rate'>" . _MD_TOPRATED . "</a> ]";

    return $toolbar;
}

function newdownloadgraphic($time, $status)
{
    $count = 7;
    $new = '';
    $startdate = (time() - (86400 * $count));
    if ($startdate < $time)
    {
        if ($status == 1)
        {
            $new = "&nbsp;<img src=\"" . XOOPS_URL . "/modules/mydownloads/images/newred.gif\" align=\"middle\" alt=\"" . _MD_NEWTHISWEEK . "\" />";
        }elseif ($status == 2)
        {
            $new = "&nbsp;<img src=\"" . XOOPS_URL . "/modules/mydownloads/images/update.gif\" align=\"middle\" alt=\"" . _MD_UPTHISWEEK . "\" />";
        }
    }
    return $new;
}

function popgraphic($hits)
{
    global $xoopsModuleConfig;
    if ($hits >= $xoopsModuleConfig['popular'])
    {
        $pop = "&nbsp;<img src =\"" . XOOPS_URL . "/modules/mydownloads/images/pop.gif\" align=\"middle\" alt=\"" . _MD_POPULAR . "\" />";
    }
    else
    {
        $pop = '';
    }
    return $pop;
} 
// Reusable Link Sorting Functions
function convertorderbyin($orderby)
{
    switch (trim($orderby))
    {
        case "titleA":
            $orderby = "title ASC";
            break;
        case "dateA":
            $orderby = "published ASC";
            break;
        case "hitsA":
            $orderby = "hits ASC";
            break;
        case "ratingA":
            $orderby = "rating ASC";
            break;
        case "titleD":
            $orderby = "title DESC";
            break;
        case "hitsD":
            $orderby = "hits DESC";
            break;
        case "ratingD":
            $orderby = "rating DESC";
            break;
        case"dateD":
        default:
            $orderby = "published DESC";
            break;
    }
    return $orderby;
}
function convertorderbytrans($orderby)
{
    if ($orderby == "hits ASC") $orderbyTrans = _MD_POPULARITYLTOM;
    if ($orderby == "hits DESC") $orderbyTrans = _MD_POPULARITYMTOL;
    if ($orderby == "title ASC") $orderbyTrans = _MD_TITLEATOZ;
    if ($orderby == "title DESC") $orderbyTrans = _MD_TITLEZTOA;
    if ($orderby == "published ASC") $orderbyTrans = _MD_DATEOLD;
    if ($orderby == "published DESC") $orderbyTrans = _MD_DATENEW;
    if ($orderby == "rating ASC") $orderbyTrans = _MD_RATINGLTOH;
    if ($orderby == "rating DESC") $orderbyTrans = _MD_RATINGHTOL;
    return $orderbyTrans;
}
function convertorderbyout($orderby)
{
    if ($orderby == "title ASC") $orderby = "titleA";
    if ($orderby == "published ASC") $orderby = "dateA";
    if ($orderby == "hits ASC") $orderby = "hitsA";
    if ($orderby == "rating ASC") $orderby = "ratingA";
    if ($orderby == "title DESC") $orderby = "titleD";
    if ($orderby == "published DESC") $orderby = "dateD";
    if ($orderby == "hits DESC") $orderby = "hitsD";
    if ($orderby == "rating DESC") $orderby = "ratingD";
    return $orderby;
}

function PrettySize($size)
{
    $mb = 1024 * 1024;
    if ($size > $mb)
    {
        $mysize = sprintf ("%01.2f", $size / $mb) . " MB";
    }elseif ($size >= 1024)
    {
        $mysize = sprintf ("%01.2f", $size / 1024) . " KB";
    }
    else
    {
        $mysize = sprintf(_MD_NUMBYTES, $size);
    }
    return $mysize;
} 
// updates rating data in itemtable for a given item
function updaterating($sel_id)
{
    global $xoopsDB;
    $query = "select rating FROM " . $xoopsDB->prefix("mydownloads_votedata") . " WHERE lid = " . $sel_id . "";
    $voteresult = $xoopsDB->query($query);
    $votesDB = $xoopsDB->getRowsNum($voteresult);
    $totalrating = 0;
    while (list($rating) = $xoopsDB->fetchRow($voteresult))
    {
        $totalrating += $rating;
    }
    $finalrating = $totalrating / $votesDB;
    $finalrating = number_format($finalrating, 4);
    $sql = sprintf("UPDATE %s SET rating = %u, votes = %u WHERE lid = %u", $xoopsDB->prefix("mydownloads_downloads"), $finalrating, $votesDB, $sel_id);
    $xoopsDB->query($sql);
} 
// returns the total number of items in items table that are accociated with a given table $table id
function getTotalItems($sel_id)
{
    global $xoopsDB, $mytree, $xoopsModule, $xoopsUser;

    $groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
    $gperm_handler = &xoops_gethandler('groupperm');

    $count = 0;
    $arr = array();
    $query = "select lid from " . $xoopsDB->prefix("mydownloads_downloads") . " where offline = 0 AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND cid=" . $sel_id . "";
    $result = $xoopsDB->query($query);
    while (list($lid) = $xoopsDB->fetchRow($result))
    {
        if ($gperm_handler->checkRight('DownFilePerm', $lid , $groups, $xoopsModule->getVar('mid')))
        {
            $count++;
        }
    }
    $arr = $mytree->getAllChildId($sel_id);
    $size = count($arr);
    $thing = 0;

    for($i = 0;$i < count($arr);$i++)
    {
        $query2 = "select lid from " . $xoopsDB->prefix("mydownloads_downloads") . " where offline = 0 AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND cid=" . $arr[$i] . "";

        $result2 = $xoopsDB->query($query2);
        while (list($lid) = $xoopsDB->fetchRow($result2))
        {
            if ($gperm_handler->checkRight('DownFilePerm', $lid , $groups, $xoopsModule->getVar('mid')))
            {
                $thing++;
            }
        }

        $count += $thing;
    }
    return $count;
}

function imageheader()
{
    global $xoopsDB, $xoopsModule, $xoopsModuleConfig;

    $result = $xoopsDB->query("SELECT indeximage, indexheading FROM " . $xoopsDB->prefix("mydownloads_indexpage ") . " ");
    list($indeximage, $indexheading) = $xoopsDB->fetchrow($result);
    $image = displayimage($indeximage, "index.php", $xoopsModuleConfig['mainimagedir'], $indexheading);
    return $image;
}

function displayimage($image = 'blank.png', $path = '', $imgsource = '', $alttext = '')
{
    global $xoopsConfig, $xoopsUser, $xoopsModule;

    $showimage = '';

    /**
     * Check to see if link is given
     */
    if ($path)
    {
        $showimage = "<a href=" . $path . ">";
    }

    /**
     * checks to see if the file is valid else displays default blank image
     */

    if (!is_dir(XOOPS_ROOT_PATH . "/" . $imgsource . "/" . $image) && file_exists(XOOPS_ROOT_PATH . "/" . $imgsource . "/" . $image))
    {
        $showimage .= "<img src=" . XOOPS_URL . "/" . $imgsource . "/" . $image . " border='0' alt=" . $alttext . " /></a>";
    }
    else
    {
        if ($xoopsUser && $xoopsUser->isAdmin($xoopsModule->mid()))
        {
            $showimage .= "<img src=images/brokenimg.png border='0' alt='" . _MD_ISADMINNOTICE . "' /></a>";
        }
        else
        {
            $showimage .= "<img src=images/blank.png border='0' alt=" . $alttext . " /></a>";
        }
    }
    clearstatcache();
    return $showimage;
}

function down_createthumb($name, $root, $path, $savepath, $new_w = 100, $new_h = 100, $quality = 90)
{
    $savefile = $path . $savepath . $new_w . "x" . $new_h . "_" . $name;
    $savepath = $root . $savefile; 
    // echo "Checking save path ".$savepath."<br>";
    if (!file_exists($savepath))
    { 
        // echo "Creating Image  ".$new_w."<br>";
        // Get image location
        $image_path = $root . $path . "/" . $name; 
        // Load image
        $img = null;
        $ext = end(explode('.', $image_path));
        if ($ext == 'jpg' || $ext == 'jpeg')
        {
            $img = @imagecreatefromjpeg($image_path);
        }
        else if ($ext == 'png')
        {
            $img = @imagecreatefrompng($image_path); 
            // Only if your version of GD includes GIF support
        }
        else if ($ext == 'gif')
        {
            $img = @imagecreatefrompng($image_path);
        } 
        // If an image was successfully loaded, test the image for size
        if ($img)
        { 
            // Get image size and scale ratio
            $width = imagesx($img);
            $height = imagesy($img);
            $scale = min($new_w / $width, $new_h / $height); 
            // If the image is larger than the max shrink it
            if ($scale < 1)
            {
                $new_width = floor($scale * $width);
                $new_height = floor($scale * $height); 
                // Create a new temporary image
                $tmp_img = imagecreatetruecolor($new_width, $new_height); 
                // Copy and resize old image into new image
                imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagedestroy($img);
                $img = $tmp_img;
            }
        } 
        // Create error image if necessary
        if (!$img)
        {
            $img = imagecreate($new_w, $new_h);
            imagecolorallocate($img, 0, 0, 0);
            $c = imagecolorallocate($img, 70, 70, 70);
            imageline($img, 0, 0, $new_w, $new_h, $c2);
            imageline($img, $new_w, 0, 0, $new_h, $c2);
        } 
        // output the image as a file to the output stream
        // header("Content-type: image/jpeg");
        Imagejpeg($img, $savepath, $quality); 
        // echo "Image saved to ".$savepath."<br>";
    } 
    // echo "File link is ".$savefile;
    return $savefile;
}

function totallistings($lidid = 0)
{
    global $xoopsDB, $xoopsModule, $xoopsUser;

    $groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
    $gperm_handler = &xoops_gethandler('groupperm');

    $listings = 0;
    $sql = "SELECT lid, cid FROM " . $xoopsDB->prefix("mydownloads_downloads ") . " WHERE status > 0 AND offline = 0 AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ")";

    if ($lidid)
    {
        $sql .= "AND cid = " . $lidid . "";
    }
    $result = $xoopsDB->query($sql);

    while (list($lid, $cid) = $xoopsDB->fetchRow($result))
    {
        if ($gperm_handler->checkRight('DownCatPerm', $cid , $groups, $xoopsModule->getVar('mid')))
        {
            if ($gperm_handler->checkRight('DownFilePerm', $lid , $groups, $xoopsModule->getVar('mid')))
            {
                $listings++;
            }
        }
    }
    return $listings;
}

function totalcategory($cidid = 0)
{
    global $xoopsDB, $xoopsModule, $xoopsUser;

    $groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
    $gperm_handler = &xoops_gethandler('groupperm');

    $catlisting = 0;
    $sql = "SELECT cid FROM " . $xoopsDB->prefix("mydownloads_cat") . " ";

    if ($cidid)
    {
        $sql .= "WHERE cid = " . $cidid . "";
        $result = $xoopsDB->query($sql);

        while (list($cid) = $xoopsDB->fetchRow($result))
        {
            if ($gperm_handler->checkRight('DownCatPerm', $cid , $groups, $xoopsModule->getVar('mid')))
            {
                $catlisting++;
            }
        }
    }
    else
    {
        $result = $xoopsDB->query($sql);
        $catlisting = $xoopsDB->getRowsNum($result);
    }
    return $catlisting;
}


function letters()
{
    $letterchoice = "<div>" . _MD_BROWSETOTOPIC . "</div>";
    $letterchoice .= "[  ";
    $alphabet = array ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
    $num = count($alphabet) - 1;
    $counter = 0;
    while (list(, $ltr) = each($alphabet))
    {
        $letterchoice .= "<a href='viewcat.php?list=$ltr'>$ltr</a>";
        if ($counter == round($num / 2))
            $letterchoice .= " ]<br>[ ";
        elseif ($counter != $num)
            $letterchoice .= "&nbsp;|&nbsp;";
        $counter++;
    }
    $letterchoice .= " ]";
    return $letterchoice;
}

function isnewimage($sel_id)
{
    global $xoopsDB;

    $published = 0;

    $oneday = (time() - (86400 * 1));
    $threedays = (time() - (86400 * 3));
    $week = (time() - (86400 * 7));

    $result2 = $xoopsDB->query("SELECT published FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE cid = $sel_id AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") LIMIT 1");
    list($published) = $xoopsDB->fetchRow($result2);

    if ($published > 0 && $published < $week)
    {
        $indicator['image'] = "images/icon/download4.jpg";
        $indicator['alttext'] = "New Submitted Before Last Week";
    }elseif ($published >= $week && $published < $threedays)
    {
        $indicator['image'] = "images/icon/download3.jpg";
        $indicator['alttext'] = "New Submitted Within This week";
    }elseif ($published >= $threedays && $published < $oneday)
    {
        $indicator['image'] = "images/icon/download2.jpg";
        $indicator['alttext'] = "New Submitted Within Last Three days";
    }elseif ($published >= $oneday)
    {
        $indicator['image'] = "images/icon/download1.jpg";
        $indicator['alttext'] = "New Submitted Today";
    }
    else
    {
        $indicator['image'] = "images/icon/download.jpg";
        $indicator['alttext'] = "No Files Yet";
    }
    return $indicator;
} 
// GetDownloadTime()
// This function is used to show some different download times
// BCMATH-Support in PHP needed!
// (c)02.04.04 by St@neCold, stonecold@csgui.de, http://www.csgui.de
function GetDownloadTime($size = 0, $gmodem = 1, $gisdn = 1, $gdsl = 1, $gslan = 0, $gflan = 0)
{
    $aflag = array();
    $amtime = array();
    $artime = array();
    $ahtime = array();
    $asout = array();
    $aflag = array($gmodem, $gisdn, $gdsl, $gslan, $gflan);
    $amtime = array($size / 6300 / 60, $size / 7200 / 60, $size / 86400 / 60, $size / 1125000 / 60, $size / 11250000 / 60);
    $amname = array('Modem(56k)', 'ISDN(64k)', 'DSL(768k)', 'LAN(10M)', 'LAN(100M');
    for($i = 0;$i < 5;$i++)
    {
        $artime[$i] = bcmod($amtime[$i], 60);
    }
    for($i = 0;$i < 5;$i++)
    {
        $ahtime[$i] = sprintf(' %2.0f', $amtime[$i] / 60);
    }
    if ($size <= 0) $dltime = 'N/A';
    else
    {
        for($i = 0;$i < 5;$i++)
        {
            if (!$aflag[$i]) $asout[$i] = '';
            else
            {
                if (($amtime[$i] * 60) < 1) $asout[$i] = sprintf(' : %4.2fs', $amtime[$i] * 60);
                else
                {
                    if ($amtime[$i] < 1) $asout[$i] = sprintf(' : %2.0fs', round($amtime[$i] * 60));
                    else
                    {
                        if ($ahtime[$i] == 0) $asout[$i] = sprintf(' : %5.1fmin', $amtime[$i]);
                        else $asout[$i] = sprintf(' : %2.0fh%2.0fmin', $ahtime[$i], $artime[$i]);
                    }
                }
                $asout[$i] = "<b>" . $amname[$i] . "</b>" . $asout[$i];
                if ($i < 4) $asout[$i] = $asout[$i] . ' | ';
            }
        }
        $dltime = '';
        for($i = 0;$i < 5;$i++)
        {
            $dltime = $dltime . $asout[$i];
        }
    }
    return $dltime;
}

function retmime()
{
	global $xoopsModuleConfig, $xoopsModule;
	
	include_once XOOPS_ROOT_PATH.'/modules/'.$xoopsModule->dirname().'/class/mimetype.php';
  
	$mimetype = new mimetype();
	
    foreach($xoopsModuleConfig['downmimetypes'] as $retext)
	{
		$downmimetype[] = $mimetype->privFindType($retext);
	}
	return $downmimetype;
}

function adminmenu($header = '', $menu = '', $extra = '', $scount = 5)
{
    global $xoopsConfig, $xoopsModule;

    if (isset($_SERVER['PHP_SELF'])) $thispage = basename($_SERVER['PHP_SELF']);
    $op = (isset($_GET['op'])) ? $op = "?op=" . $_GET['op'] : '';

    if (empty($menu)) {
        /**
         * You can change this part to suit your own module. Defining this here will save you form having to do this each time.
         */
        $menu = array(
            _AM_GENERALSET => "" . XOOPS_URL . "/modules/system/admin.php?fct=preferences&amp;op=showmod&amp;mod=" . $xoopsModule->getVar('mid') . "",

            _AM_FINDEX => "index.php",
            _AM_FINDEXPAGE => "indexpage.php",
            _AM_ADDNEWCAT => "category.php",
            _AM_ADDNEWDOWN => "index.php?op=Download",
            _AM_DLSWAITING => "newdownloads.php",
            _AM_BROKENREPORTS => "brokendown.php",
            _AM_MODREQUESTS => "modifications.php",
            _AM_PERMISSIONS => "permissions.php",
            _AM_UPLOADIMGFILE => "upload.php",
            _AM_REVIEWSMENU => "index.php?op=reviews",
			_AM_BLOCKADMIN => "myblocksadmin.php"
			);
    } 

    if (!is_array($menu)) {
        echo "<table width = '100%' cellpadding= '2' cellspacing= '1' class='outer'>";
        echo "<tr><td class = even align = center><b>No menu items within the menu</b></td></tr></table><br />";
        return false;
    } 

    $oddnum = array(1 => "1", 3 => "3", 5 => "5", 7 => "7", 9 => "9", 11 => "11", 13 => "13"); 
    // number of rows per menu
    $menurows = count($menu) / $scount; 
    // total amount of rows to complete menu
    $menurow = ceil($menurows) * $scount; 
    // actual number of menuitems per row
    $rowcount = $menurow / ceil($menurows);
    $count = 0;
    for ($i = count($menu); $i < $menurow; $i++) {
        $tempArray = array(1 => null);
        $menu = array_merge($menu, $tempArray);
        $count++;
    } 

    /**
     * Sets up the width of each menu cell
     */
    $width = 100 / $scount;
    $width = ceil($width);

    $menucount = 0;
    $count = 0;
    /**
     * Menu table output
     */
    echo "<h3>" . $header . "</h3>";
    echo "<table width = '100%' cellpadding= '2' cellspacing= '1' class='outer'><tr>";

    /**
     * Check to see if $menu is and array
     */
    if (is_array($menu)) {
        $classcounts = 0;
        $classcol[0] = "even";

        for ($i = 1; $i < $menurow; $i++) {
            $classcounts++;
            if ($classcounts >= $scount) {
                if ($classcol[$i-1] == 'odd') {
                    $classcol[$i] = ($classcol[$i-1] == 'odd' && in_array($classcounts, $oddnum)) ? "even" : "odd";
                } else {
                    $classcol[$i] = ($classcol[$i-1] == 'even' && in_array($classcounts, $oddnum)) ? "odd" : "even";
                } 
                $classcounts = 0;
            } else {
                $classcol[$i] = ($classcol[$i-1] == 'even') ? "odd" : "even";
            } 
        } 
        unset($classcounts);

        foreach ($menu as $menutitle => $menulink) {
            if ($thispage . $op == $menulink) {
                $classcol[$count] = "outer";
            } 
            echo "<td class='" . $classcol[$count] . "' align='center' valign='middle' width= $width%>";
            if (is_string($menulink)) {
                echo "<a href='" . $menulink . "'>" . $menutitle . "</a></td>";
            } else {
                echo "&nbsp;</td>";
            } 
            $menucount++;
            $count++;
            /**
             * Break menu cells to start a new row if $count > $scount
             */
            if ($menucount >= $scount) {
                echo "</tr>";
                $menucount = 0;
            } 
        } 
        echo "</table><br />";
        unset($count);
        unset($menucount);
    } 
    if ($extra) {
        echo "<div>$extra</div>";
    } 
}
/*
function PrettySize($size)
{
    $mb = 1024 * 1024;
    if ($size > $mb) {
        $mysize = sprintf ("%01.2f", $size / $mb) . " MB";
    } elseif ($size >= 1024) {
        $mysize = sprintf ("%01.2f", $size / 1024) . " KB";
    } else {
        $mysize = sprintf(_MD_NUMBYTES, $size);
    } 
    return $mysize;
} 
*/
function getDirSelectOption($selected, $dirarray, $namearray)
{ 
    // global $workd;
    echo "<select size='1' name='workd' onchange='location.href=\"upload.php?rootpath=\"+this.options[this.selectedIndex].value'>";
    echo "<option value=''>--------------------------------------</option>";
    foreach($namearray as $namearray => $workd) {
        if ($workd === $selected) {
            $opt_selected = "selected";
        } else {
            $opt_selected = "";
        } 
        echo "<option value='" . htmlspecialchars($namearray, ENT_QUOTES) . "' $opt_selected>" . $workd . "</option>";
    } 
    echo "</select>";
} 

function filesarray($filearray)
{
    $files = array();
    $dir = opendir($filearray);

    while (($file = readdir($dir)) !== false) {
        if ((!preg_match("/^[.]{1,2}$/", $file) && preg_match("/[.htm|.html|.xhtml]$/i", $file) && !is_dir($file))) {
            if (strtolower($file) != 'cvs' && !is_dir($file)) {
                $files[$file] = $file;
            } 
        } 
    } 
    closedir($dir);
    asort($files);
    reset($files);
    return $files;
} 

function uploading($allowed_mimetypes = '', $HTTP_POST_FILES, $redirecturl = "index.php", $num = 0, $uploaddir, $redirect = 0)
{
    include_once XOOPS_ROOT_PATH . "/class/uploader.php";
	
    global $xoopsConfig, $xoopsModuleConfig, $HTTP_POST_VARS;
	if (empty($allowed_mimetypes))
	{
		$allowed_mimetypes = retmime();
	}
	$uploaddir = XOOPS_ROOT_PATH . "/" . $uploaddir . "/";
	
    $maxfilesize = $xoopsModuleConfig['maxfilesize'];
    $maxfilewidth = $xoopsModuleConfig['maximgwidth'];
    $maxfileheight = $xoopsModuleConfig['maximgheight'];

    $uploader = new XoopsMediaUploader($uploaddir, $allowed_mimetypes, $maxfilesize, $maxfilewidth, $maxfileheight);

    if ($uploader->fetchMedia($HTTP_POST_VARS['xoops_upload_file'][0])) {
        if (!$uploader->upload()) {
            $errors = $uploader->getErrors();
            redirect_header($redirecturl, 1, $errors);
        } else {
            if ($redirect) {
                redirect_header($redirecturl, 1 , "File Uploaded");
            } else {
		        $down['url'] = formatURL(XOOPS_URL . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name']));
                if (is_file(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name'])))
                {
                    $down['size'] = filesize(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['uploaddir'] . "/" . strtolower($HTTP_POST_FILES['userfile']['name']));
                } else {
					$down['size'] = 0;
				}
				return $down;					
			}
        } 
    } else {
        $errors = $uploader->getErrors();
        redirect_header($redirecturl, 1, $errors);
    } 
} 

function getforum($forumid)
{
    global $xoopsDB, $xoopsConfig;

    echo "<select name='forumid'>";
    echo "<option value='0'>----------------------</option>";
    $result = $xoopsDB->query("SELECT forum_name, forum_id FROM " . $xoopsDB->prefix("bb_forums") . " ORDER BY forum_id");
    while (list($forum_name, $forum_id) = $xoopsDB->fetchRow($result)) {
        if ($forum_id == $forumid) {
            $opt_selected = "selected='selected'";
        } else {
            $opt_selected = "";
        } 
        echo "<option value='" . $forum_id . "' $opt_selected>" . $forum_name . "</option>";
    } 
    echo "</select></div>";
    return $forumid;
} 

function downlistheader($heading)
{
    echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . $heading . "</legend><br />";
    echo "<table width='100%' cellspacing=1 cellpadding=2 border=0 class = outer>";
    echo "<tr>";
    echo "<th align='center'><b>" . _AM_ID . "</b></th>";
    echo "<th align='left'><b>" . _AM_TITLE . "</b></th>";
    echo "<th align='center'><b>" . _AM_POSTER . "</b></th>";
    echo "<th align='center'><b>" . _AM_SUBMITDATE . "</b></th>";
    echo "<th align='center'><b>" . _AM_ONLINE . "</b></th>";
    echo "<th align='center'><b>" . _AM_STATUS . "</b></th>";
    echo "<th align='center'><b>" . _AM_ACTION . "</b></th>";
    echo "</tr>";
} 

function downlistbody($published)
{
    global $myts, $imagearray; 
    
	$lid = $published['lid'];
	$cid = $published['cid']; 
    $title = "<a href='../singlefile.php?cid=".$published['cid']."&lid=" . $published['lid'] . "'>" . $myts->htmlSpecialChars(trim($published['title'])) . "</a>";;
    $submitter = xoops_getLinkedUnameFromId(intval($published['submitter']));
	$publish = formatTimestamp($published['published'], 's');
    $status = ($published['published'] > 0) ? $imagearray['online'] : "<a href='newdownloads.php'>" . $imagearray['offline'] . "</a>";
    $offline = ($published['offline'] == 0) ? $imagearray['online'] : $imagearray['offline'];
   	$modify = "<a href='index.php?op=Download&lid=" . $lid . "'>" . $imagearray['editimg'] . "</a>";
    $delete = "<a href='index.php?op=delDownload&lid=" . $lid . "'>" . $imagearray['deleteimg'] . "</a>";

    echo "<tr>";
    echo "<td class='head' align='center'>" . $lid . "</td>";
    echo "<td class='even' align='left'>" . $title . "</td>";
    echo "<td class='even' align='center'>" . $submitter . "</td>";
    echo "<td class='even' align='center'>" . $publish . "</td>";
    echo "<td class='even' align='center'>" . $offline . "</td>";
    echo "<td class='even' align='center'>" . $status . "</td>";
    echo "<td class='even' align='center' width = 10% nowrap>$modify $delete</td>";
    echo "</tr>";
	unset($published);
} 

function downlistfooter()
{
    echo "<tr>";
    echo "<td class='head' align='center' colspan= '7'>" . _AM_NODOWNLOADSFOUND . "</td>";
    echo "</tr>";
} 

function downlistpagenav($pubrowamount, $start, $art = "art")
{
    global $xoopsModuleConfig;

    echo "</table>\n"; 
    // Display Page Nav if published is > total display pages amount.
    $page = ($pubrowamount > $xoopsModuleConfig['perpage']) ? '<b>Page: </b>' : '';
    $pagenav = new XoopsPageNav($pubrowamount, $xoopsModuleConfig['perpage'], $start, 'st' . $art);
    echo '<div align="right" style="padding: 8px;">' . $page . '' . $pagenav->renderNav() . '</div>';
    echo "</fieldset><br />";
} 
?>