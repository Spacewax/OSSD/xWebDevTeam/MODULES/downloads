<?php

/**
 * 
 * @version $Id: downloadinfo.php,v 1.1 2004/06/05 09:05:05 mithyt2 Exp $
 * @copyright 2003
 */

$down['id'] = intval($down_arr['lid']);
$down['cid'] = intval($down_arr['cid']);

$sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_text') . " WHERE lid = " . $down_arr['lid'] . "";
$down_arr_text = $xoopsDB->fetcharray($xoopsDB->query($sql));

$down_arr['description'] = $down_arr_text['description'];

$path = $mytree->getPathFromId($down_arr['cid'], "title");
$path = substr($path, 1);
$path = basename($path);
$path = str_replace("/", "", $path);
$down['category'] = $path;

$rating = number_format($down_arr['rating'], 0) / 2;
$rateimg = "rate$rating.gif";
$down['rateimg'] = $rateimg;
$down['votes'] = ($down_arr['votes'] == 1) ? _MD_ONEVOTE : sprintf(_MD_NUMVOTES, $down_arr['votes']);
$down['hits'] = intval($down_arr['hits']);
$xoopsTpl->assign('lang_dltimes', sprintf(_MD_DLTIMES, $down['hits']));

$down['title'] = $down_arr['title'];
$down['url'] = $down_arr['url'];
$down['screenshot'] = XOOPS_URL . "/" . $xoopsModuleConfig['screenshots'] . "/" . $myts->makeTboxData4Show($down_arr['logourl']);

$down['logourl'] = '';
if ($down_arr['logourl'] && $down_arr['logourl'] != "blank.png")
{
    if (file_exists(XOOPS_ROOT_PATH . "/" . $xoopsModuleConfig['screenshots'] . "/" . $myts->makeTboxData4Show($down_arr['logourl'])))
    {
		if ($xoopsModuleConfig['usethumbs'] == 1)
        {
            $down['logourl'] = down_createthumb($down_arr['logourl'], XOOPS_ROOT_PATH, "/" . $xoopsModuleConfig['screenshots'], "/thumbs/", $xoopsModuleConfig['shotheight'], $xoopsModuleConfig['shotwidth'], 100);
            $down['logourl'] = XOOPS_URL . "/" . $xoopsModuleConfig['screenshots'] . "/thumbs/" . basename($down['logourl']);
        }
        else
        {
            $down['logourl'] = XOOPS_URL . "/" . $xoopsModuleConfig['screenshots'] . "/" . $myts->makeTboxData4Show($down_arr['logourl']);
        }
    }
}
$down['screenshot_url'] = trim($xoopsModuleConfig['screenshots']);
$down['homepage'] = (!$down_arr['homepage'] || $down_arr['homepage'] == "http://") ? '' : $myts->makeTboxData4Show(trim($down_arr['homepage']));
if ($down['homepage'] && !empty($down['homepage']))
{
    $down['homepagetitle'] = (empty($down_arr['homepagetitle'])) ? trim($down['homepage']) : $myts->makeTboxData4Show(trim($down_arr['homepagetitle']));
    $down['homepage'] = "<a href=\"" . $down['homepage'] . "\" target=\"_BLANK\">" . $down['homepagetitle'] . "</a>";
}
else
{
    $down['homepage'] = _MD_NOTSPECIFIED;
}

$down['mirror'] = ($down_arr['mirror'] == "http://") ? '' : $myts->makeTboxData4Show(trim($down_arr['mirror']));
$down['mirror'] = ($down['mirror']) ? "<a href=\"" . $down['mirror'] . "\" target=\"_BLANK\">" . _MD_MIRRORSITE . "</a>" : _MD_NOTSPECIFIED;
$down['comments'] = $down_arr['comments'];
$down['version'] = $down_arr['version'];
$down['downtime'] = GetDownloadTime(intval($down_arr['size']),1,1,1,1,0);

$down['downtime'] = str_replace("|", "<br />", $down['downtime']);
$down['size'] = PrettySize($myts->makeTboxData4Show($down_arr['size']));

$time = ($down_arr['updated'] != 0) ? $down_arr['updated'] : $down_arr['published'];
$down['updated'] = formatTimestamp($time, $xoopsModuleConfig['dateformat']);
$is_updated = ($down_arr['updated'] != 0) ? _MD_UPDATEDON : _MD_SUBMITDATE;
$xoopsTpl->assign('lang_subdate' , $is_updated);

$down['description'] = $myts->makeTareaData4Show($down_arr['description'], 0); //no html
$down['price'] = ($down_arr['price'] != 0) ? intval($down_arr['price']) : _MD_PRICEFREE;
$down['limitations'] = (empty($down_arr['limitations'])) ? _MD_NOTSPECIFIED : $myts->makeTboxData4Show(trim($xoopsModuleConfig['limitations'][$down_arr['limitations']]));
$down['license'] = (empty($down_arr['license'])) ? _MD_NOTSPECIFIED : $myts->makeTboxData4Show(trim($xoopsModuleConfig['license'][$down_arr['license']]));
$down['publisher'] = xoops_getLinkedUnameFromId(intval($down_arr['submitter']));
$down['platform'] = $myts->makeTboxData4Show($xoopsModuleConfig['platform'][$down_arr['platform']]);
$down['history'] = $myts->makeTareaData4Show($down_arr['dhistory']);
$down['features'] = '';
if ($down_arr['features'])
{
    $downfeatures = explode('|', trim($down_arr['features']));
    foreach ($downfeatures as $bi)
    {
        $down['features'][] = $bi;
    }
}

$down['requirements'] = '';
if ($down_arr['requirements'])
{
    $downrequirements = explode('|', trim($down_arr['requirements']));
    foreach ($downrequirements as $bi)
    {
        $down['requirements'][] = $bi;
    }
}
$down['mail_subject'] = rawurlencode(sprintf(_MD_INTFILEFOUND, $xoopsConfig['sitename']));
$down['mail_body'] = rawurlencode(sprintf(_MD_INTFILEFOUND, $xoopsConfig['sitename']) . ':  ' . XOOPS_URL . '/modules/mydownloads/singlefile.php?cid=' . $down_arr['cid'] . '&amp;lid=' . $down_arr['lid']); 
// $new = newdownloadgraphic($time, $status);
// $pop = popgraphic($hits);
$down['isadmin'] = (!empty($xoopsUser) && $xoopsUser->isAdmin($xoopsModule->mid())) ? true : false;

$down['adminlink'] = '';
if ($down['isadmin'] == true)
{
    $down['adminlink'] = '[ <a href="' . XOOPS_URL . '/modules/mydownloads/admin/index.php?op=Download&lid=' . $down_arr['lid'] . '">' . _MD_EDIT . '</a> | ';
    $down['adminlink'] .= '<a href="' . XOOPS_URL . '/modules/mydownloads/admin/index.php?op=delDownload&lid=' . $down_arr['lid'] . '">' . _MD_DELETE . '</a> ]';
}
$votestring = ($down_arr['votes'] == 1) ? _MD_ONEVOTE : sprintf(_MD_NUMVOTES, $down_arr['votes']);
$is_updated = ($down_arr['updated'] > 0) ? _MD_UPDATEDON : _MD_SUBMITDATE;
$xoopsTpl->assign('lang_subdate' , $is_updated);
if (is_object($xoopsUser) && $down['isadmin'] != true)
{
    $down['useradminlink'] = ($xoopsUser->getvar('uid') == $down_arr['submitter']) ? true : false;
}

$sql2 = "SELECT rated FROM " . $xoopsDB->prefix('mydownloads_reviews') . " WHERE lid = " . $down_arr['lid'] . " AND submit = 1";
$results = $xoopsDB->query($sql2);
$numrows = $xoopsDB->getRowsNum($results);

$down['reviews_num'] = ($numrows) ? $numrows : 0;

$finalrating = 0;
$totalrating = 0;

while ($review_text = $xoopsDB->fetchArray($results))
{
    $totalrating += $review_text['rated'];
}

if ($down['reviews_num'] > 0)
{
    $finalrating = $totalrating / $down['reviews_num'];
    $finalrating = round(number_format($finalrating, 0) / 2);
}
$down['review_rateimg'] = "rate$finalrating.gif";;
$xoopsTpl->append('file', $down);
?>