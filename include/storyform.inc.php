<?php 
// $Id: storyform.inc.php,v 1.2 2004/06/05 09:05:05 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------ //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php" ;
include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";
$sform = new XoopsThemeForm( _MD_SUBMITCATHEAD, "storyform", xoops_getenv( 'PHP_SELF' ) );

$sform->addElement( new XoopsFormText( _MD_FILETITLE, 'title', 50, 80 ), true );

$mytree = new XoopsTree( $xoopsDB->prefix( "mydownloads_cat " ), "cid ", "pid" );

$sform->addElement( new XoopsFormText( _MD_DLURL, 'URL', 50, 80, "http://" ), false );
//$sform->addElement( new XoopsFormFile( _MD_DLURL, 'imgfile', 2000000 ), false );

ob_start();
//echo "<form enctype='multipart/form-data' action='_URL_' method='POST'>";
echo "<input type='hidden' name='MAX_FILE_SIZE' value='30000'>";
echo "<input name='userfile' type='file'>";
//echo "</form>";
$sform->addElement( new XoopsFormLabel( _MD_CATEGORYC, ob_get_contents() ) );
ob_end_clean();


ob_start();
$sform->addElement( new XoopsFormHidden( 'cid', '' ) );
$mytree->makeMySelBox( "title ", "cid" );
$sform->addElement( new XoopsFormLabel( _MD_CATEGORYC, ob_get_contents() ) );
ob_end_clean();



$sform->addElement( new XoopsFormText( _MD_HOMEPAGEC, 'homepage', 50, 80 ), false );
$sform->addElement( new XoopsFormText( _MD_VERSIONC, 'version', 10, 20 ), false );
$sform->addElement( new XoopsFormText( _MD_FILESIZEC, 'size', 10, 20 ), false );
$sform->addElement( new XoopsFormText( _MD_PLATFORMC, 'platform', 50, 80 ), false );
$sform->addElement( new XoopsFormDhtmlTextArea( _MD_DESCRIPTION, 'message', '', 15, 60 ), true );

$option_tray = new XoopsFormElementTray( _MD_OPTIONS, '<br />' );

$notify_checkbox = new XoopsFormCheckBox( '', 'notifypub' );
$notify_checkbox->addOption( 1, _MD_NOTIFYAPPROVE );
$option_tray->addElement( $notify_checkbox );

$sform->addElement( $option_tray );
$button_tray = new XoopsFormElementTray( '', '' );
$button_tray->addElement( new XoopsFormButton( '', 'submit', _SUBMIT, 'submit' ) );
$sform->addElement( $button_tray );
$sform->display();

?>