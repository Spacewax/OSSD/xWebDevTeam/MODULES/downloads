# phpMyAdmin SQL Dump
# version 2.5.5-pl1
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: May 28, 2004 at 04:08 PM
# Server version: 3.23.56
# PHP Version: 4.3.4
# 
# Database : `xoops2`
# 

# --------------------------------------------------------

#
# Table structure for table `mydownloads_broken`
#

CREATE TABLE mydownloads_broken (
  reportid int(5) NOT NULL auto_increment,
  lid int(11) NOT NULL default '0',
  sender int(11) NOT NULL default '0',
  ip varchar(20) NOT NULL default '',
  date varchar(11) NOT NULL default '0',
  confirmed enum('0','1') NOT NULL default '0',
  acknowledged enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (reportid),
  KEY lid (lid),
  KEY sender (sender),
  KEY ip (ip)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_broken`
#

# --------------------------------------------------------

#
# Table structure for table `mydownloads_cat`
#

CREATE TABLE mydownloads_cat (
  cid int(5) unsigned NOT NULL auto_increment,
  pid int(5) unsigned NOT NULL default '0',
  title varchar(50) NOT NULL default '',
  imgurl varchar(150) NOT NULL default '',
  description varchar(255) NOT NULL default '',
  spotlighttop int(11) NOT NULL default '0',
  spotlighthis int(11) NOT NULL default '0',
  PRIMARY KEY  (cid),
  KEY pid (pid)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_cat`
#

# --------------------------------------------------------

#
# Table structure for table `mydownloads_downloads`
#

CREATE TABLE mydownloads_downloads (
  lid int(11) unsigned NOT NULL auto_increment,
  cid int(5) unsigned NOT NULL default '0',
  title varchar(100) NOT NULL default '',
  url varchar(250) NOT NULL default 'http://',
  homepage varchar(100) NOT NULL default '',
  version varchar(20) NOT NULL default '',
  size int(8) NOT NULL default '0',
  platform varchar(50) NOT NULL default '',
  logourl varchar(60) NOT NULL default '',
  submitter int(11) NOT NULL default '0',
  status tinyint(2) NOT NULL default '0',
  date int(10) NOT NULL default '0',
  hits int(11) unsigned NOT NULL default '0',
  rating double(6,4) NOT NULL default '0.0000',
  votes int(11) unsigned NOT NULL default '0',
  comments int(11) unsigned NOT NULL default '0',
  license varchar(255) NOT NULL default '',
  mirror varchar(255) NOT NULL default '',
  price varchar(10) NOT NULL default 'Free',
  paypalemail varchar(255) NOT NULL default '',
  features text NOT NULL,
  requirements text NOT NULL,
  homepagetitle varchar(255) NOT NULL default '',
  forumid int(11) NOT NULL default '0',
  limitations varchar(255) NOT NULL default '30 day trial',
  dhistory text NOT NULL,
  published int(10) NOT NULL default '0',
  expired int(10) NOT NULL default '0',
  updated int(11) NOT NULL default '0',
  offline tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (lid),
  KEY cid (cid),
  KEY status (status),
  KEY title (title(40))
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_downloads`
#

# --------------------------------------------------------

#
# Table structure for table `mydownloads_indexpage`
#

CREATE TABLE mydownloads_indexpage (
  indeximage varchar(255) NOT NULL default 'blank.png',
  indexheading varchar(255) NOT NULL default 'WF-Sections',
  indexheader text NOT NULL,
  indexfooter text NOT NULL,
  nohtml tinyint(8) NOT NULL default '1',
  nosmiley tinyint(8) NOT NULL default '1',
  noxcodes tinyint(8) NOT NULL default '1',
  noimages tinyint(8) NOT NULL default '1',
  nobreak tinyint(4) NOT NULL default '0',
  indexheaderalign varchar(25) NOT NULL default 'left',
  indexfooteralign varchar(25) NOT NULL default 'center',
  FULLTEXT KEY indexheading (indexheading),
  FULLTEXT KEY indexheader (indexheader),
  FULLTEXT KEY indexfooter (indexfooter)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_indexpage`
#

INSERT INTO mydownloads_indexpage VALUES ('logo.png', 'My_downloads', 'My_downloads heading 2', 'My_downloads Footer', 0, 1, 1, 0, 0, 'left', 'Center');

# --------------------------------------------------------

#
# Table structure for table `mydownloads_mod`
#

CREATE TABLE mydownloads_mod (
  requestid int(11) NOT NULL auto_increment,
  lid int(11) unsigned NOT NULL default '0',
  cid int(5) unsigned NOT NULL default '0',
  title varchar(100) NOT NULL default '',
  url varchar(250) NOT NULL default '',
  homepage varchar(100) NOT NULL default '',
  version varchar(20) NOT NULL default '',
  size int(8) NOT NULL default '0',
  platform varchar(50) NOT NULL default '',
  logourl varchar(60) NOT NULL default '',
  submitter int(11) NOT NULL default '0',
  status tinyint(2) NOT NULL default '0',
  date int(10) NOT NULL default '0',
  hits int(11) unsigned NOT NULL default '0',
  rating double(6,4) NOT NULL default '0.0000',
  votes int(11) unsigned NOT NULL default '0',
  comments int(11) unsigned NOT NULL default '0',
  license varchar(255) NOT NULL default '',
  mirror varchar(255) NOT NULL default '',
  price varchar(10) NOT NULL default 'Free',
  paypalemail varchar(255) NOT NULL default '',
  features text NOT NULL,
  requirements text NOT NULL,
  homepagetitle varchar(255) NOT NULL default '',
  forumid int(11) NOT NULL default '0',
  limitations varchar(255) NOT NULL default '30 day trial',
  dhistory text NOT NULL,
  published int(10) NOT NULL default '0',
  expired int(10) NOT NULL default '0',
  updated int(11) NOT NULL default '0',
  offline tinyint(1) NOT NULL default '0',
  description text NOT NULL,
  modifysubmitter int(11) NOT NULL default '0',
  requestdate int(11) NOT NULL default '0',
  PRIMARY KEY  (requestid)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_mod`
#


# --------------------------------------------------------

#
# Table structure for table `mydownloads_reviews`
#

CREATE TABLE mydownloads_reviews (
  review_id int(11) unsigned NOT NULL auto_increment,
  lid int(11) NOT NULL default '0',
  title varchar(255) default NULL,
  review text,
  submit int(11) NOT NULL default '0',
  date int(11) NOT NULL default '0',
  uid int(10) NOT NULL default '0',
  rated int(11) NOT NULL default '0',
  PRIMARY KEY  (review_id),
  KEY categoryid (lid)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_reviews`
#

# --------------------------------------------------------

#
# Table structure for table `mydownloads_text`
#

CREATE TABLE mydownloads_text (
  lid int(11) unsigned NOT NULL default '0',
  description text NOT NULL,
  KEY lid (lid)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_text`
#

# --------------------------------------------------------

#
# Table structure for table `mydownloads_votedata`
#

CREATE TABLE mydownloads_votedata (
  ratingid int(11) unsigned NOT NULL auto_increment,
  lid int(11) unsigned NOT NULL default '0',
  ratinguser int(11) NOT NULL default '0',
  rating tinyint(3) unsigned NOT NULL default '0',
  ratinghostname varchar(60) NOT NULL default '',
  ratingtimestamp int(10) NOT NULL default '0',
  PRIMARY KEY  (ratingid),
  KEY ratinguser (ratinguser),
  KEY ratinghostname (ratinghostname),
  KEY lid (lid)
) TYPE=MyISAM;

#
# Dumping data for table `mydownloads_votedata`
#

