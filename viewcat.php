<?php 
// $Id: viewcat.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";

$start = isset($HTTP_GET_VARS['start']) ? intval($HTTP_GET_VARS['start']) : 0;
$orderby = isset($HTTP_GET_VARS['orderby']) ? convertorderbyin($HTTP_GET_VARS['orderby']) : "published ASC";
$cid = isset($HTTP_GET_VARS['cid']) ? $HTTP_GET_VARS['cid'] : 0;

include XOOPS_ROOT_PATH . "/header.php";
$xoopsOption['template_main'] = 'mydownloads_viewcat.html';

$groups = ($xoopsUser) ? $xoopsUser->getGroups() : XOOPS_GROUP_ANONYMOUS;
$gperm_handler = &xoops_gethandler('groupperm');

/**
 * Begin Main page Heading etc
 */
$catarray['imageheader'] = imageheader();
$catarray['letters'] = letters();
$catarray['toolbar'] = toolbar();
$xoopsTpl->assign('catarray', $catarray);

$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");
$pathstring = "<a href='index.php'>" . _MD_MAIN . "</a>&nbsp;:&nbsp;";
$pathstring .= $mytree->getNicePathFromId($cid, "title", "viewcat.php?op=");
$xoopsTpl->assign('category_path', $pathstring);

$xoopsTpl->assign('category_id', $cid);

$arr = array();
$arr = $mytree->getFirstChild($cid, "title");

if (count($arr) > 0)
{
    $scount = 1;
    foreach($arr as $ele)
    {
        if (!$gperm_handler->checkRight('DownCatPerm', $ele['cid'], $groups, $xoopsModule->getVar('mid')))
        {
            continue;
        } 
        $result = $xoopsDB->query("SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE CID = " . $ele['cid'] . "");
        $hasitems = $xoopsDB->getRowsNum($result);

        if ($hasitems > 0)
        {
            $sub_arr = array();
            $sub_arr = $mytree->getFirstChild($ele['cid'], "title");
            $space = 0;
            $chcount = 0;
            $infercategories = "";
            $hassubitems = 0;

            foreach($sub_arr as $sub_ele)
            {
                if (!$gperm_handler->checkRight('DownCatPerm', $sub_ele['cid'], $groups, $xoopsModule->getVar('mid')))
                {
                    continue;
                } 
                $result = $xoopsDB->query("SELECT * FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE CID = " . $sub_ele['cid'] . "");
                $hassubitems = $xoopsDB->getRowsNum($result);

                if ($hassubitems > 0)
                {
                    $xoopsTpl->assign('hassubitems', 1);
                    $chtitle = $myts->makeTboxData4Show($sub_ele['title']);
                    if ($chcount > 5)
                    {
                        $infercategories .= "...";
                        break;
                    } 
                    if ($space > 0) $infercategories .= ", ";
                    $infercategories .= "<a href=\"" . XOOPS_URL . "/modules/mydownloads/viewcat.php?cid=" . $sub_ele['cid'] . "\">" . $chtitle . "</a>";
                    $space++;
                    $chcount++;
                } 
            } 
            $xoopsTpl->append('subcategories', array('title' => $myts->makeTboxData4Show($ele['title']), 'id' => $ele['cid'], 'infercategories' => $infercategories, 'totallinks' => getTotalItems($ele['cid'], 1), 'count' => $hasitems));
            $scount++;
        } 
    } 
} 
$numrows = totallistings($cid);

if ($numrows > 0)
{
    $sql = "SELECT * FROM " . $xoopsDB->prefix('mydownloads_downloads') . " ";
    $xoopsTpl->assign('show_categort_title', true);
	if (isset($HTTP_GET_VARS['selectdate']))
    {
        $sql .= "WHERE TO_DAYS(FROM_UNIXTIME(published)) = TO_DAYS(FROM_UNIXTIME(" . $HTTP_GET_VARS['selectdate'] . ")) 
			AND published > 0 AND published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0
			ORDER BY published";
    } elseif (isset($HTTP_GET_VARS['list']))
    {
        $sql .= "WHERE title LIKE '" . $HTTP_GET_VARS['list'] . "%' AND published > 0 AND 
			published <= " . time() . " AND (expired = 0 OR expired > " . time() . ") AND offline = 0 
			ORDER BY title";
    } 
    else
    {
        $sql .= "WHERE cid=" . $cid . " AND published > 0 AND published <= " . time() . " 
			AND (expired = 0 OR expired > " . time() . ") AND offline = 0 
			ORDER BY " . $orderby . "";
        	$xoopsTpl->assign('show_categort_title', false);
    } 
    $result = $xoopsDB->query($sql, $xoopsModuleConfig['perpage'] , $start);
    $amount = $xoopsDB->getRowsNum($sql);

    while ($down_arr = $xoopsDB->fetchArray($result))
    {
        if ($gperm_handler->checkRight('DownFilePerm', $down_arr['lid'], $groups, $xoopsModule->getVar('mid')))
        {
			include XOOPS_ROOT_PATH . "/modules/mydownloads/include/downloadinfo.php";
        } 
    } 
    $xoopsTpl->assign('lang_description', _MD_DESCRIPTIONC);
    $xoopsTpl->assign('lang_lastupdate', _MD_LASTUPDATEC);
    $xoopsTpl->assign('lang_hits', _MD_HITSC);
    $xoopsTpl->assign('lang_ratingc', _MD_RATINGC);
    $xoopsTpl->assign('lang_email', _MD_EMAILC);
    $xoopsTpl->assign('lang_ratethissite', _MD_RATETHISFILE);
    $xoopsTpl->assign('lang_reviewfile' , _MD_REVIEWTHISFILE);
    $xoopsTpl->assign('lang_reportbroken', _MD_REPORTBROKEN);
    $xoopsTpl->assign('lang_tellafriend', _MD_TELLAFRIEND);
    $xoopsTpl->assign('lang_modify', _MD_MODIFY);
    $xoopsTpl->assign('lang_version' , _MD_VERSION);
    $xoopsTpl->assign('lang_dlnow' , _MD_DLNOW);
    $xoopsTpl->assign('lang_category' , _MD_CATEGORYC);
    $xoopsTpl->assign('lang_size' , _MD_FILESIZE);
    $xoopsTpl->assign('lang_platform' , _MD_SUPPORTEDPLAT);
    $xoopsTpl->assign('lang_homepage' , _MD_HOMEPAGE);
    $xoopsTpl->assign('lang_comments' , _COMMENTS);
    $xoopsTpl->assign('lang_price' , _MD_PRICE);
    $xoopsTpl->assign('lang_limits' , _MD_LIMITS);
    $xoopsTpl->assign('lang_mirror' , _MD_MIRROR);
    $xoopsTpl->assign('lang_downloads' , _MD_DOWNLOADHITS);
    $xoopsTpl->assign('lang_license' , _MD_DOWNLICENSE);
    $xoopsTpl->assign('lang_publisher' , _MD_PUBLISHER);
    $xoopsTpl->assign('lang_reviews' , _MD_REVIEWS);
    $xoopsTpl->assign('lang_fulldetails' , _MD_VIEWDETAILS);
    $xoopsTpl->assign('lang_downloadnow', _MD_DOWNLOADNOW);
	$xoopsTpl->assign('lang_sortby', _MD_SORTBY);
    $xoopsTpl->assign('show_links', false);
    if ($numrows >= 1 && $cid != 0)
    {
        $xoopsTpl->assign('show_links', true);
        $orderbyTrans = convertorderbytrans($orderby);
        $xoopsTpl->assign('lang_sortby', _MD_SORTBY);
        $xoopsTpl->assign('lang_title', _MD_TITLE);
        $xoopsTpl->assign('lang_date', _MD_DATE);
        $xoopsTpl->assign('lang_rating', _MD_RATING);
        $xoopsTpl->assign('lang_popularity', _MD_POPULARITY);
        $xoopsTpl->assign('lang_cursortedby', sprintf(_MD_CURSORTBY, convertorderbytrans($orderby)));
    } 

    $xoopsTpl->assign('show_screenshot', false);
    if ($xoopsModuleConfig['useshots'] == 1)
    {
        $xoopsTpl->assign('shotwidth', $xoopsModuleConfig['shotwidth']);
        $xoopsTpl->assign('shotheight', $xoopsModuleConfig['shotheight']);
        $xoopsTpl->assign('show_screenshot', true);
    } 

    include_once XOOPS_ROOT_PATH . '/class/pagenav.php';
    $pagenav = new XoopsPageNav($numrows, $xoopsModuleConfig['perpage'] , $start, 'start');
    $catview['navbar'] = '<div align="right">' . $pagenav->renderNav() . '</div>';
    $orderby = convertorderbyout($orderby);
    $xoopsTpl->assign('catview', $catview);
} 

include XOOPS_ROOT_PATH . "/modules/mydownloads/footer.php";

?>
