<?php 
// $Id: modfile.php,v 1.2 2004/06/05 09:05:04 mithyt2 Exp $
// ------------------------------------------------------------------------ //
// XOOPS - PHP Content Management System                      //
// Copyright (c) 2000 XOOPS.org                           //
// <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// This program is free software; you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation; either version 2 of the License, or        //
// (at your option) any later version.                                      //
// //
// You may not change or alter any portion of this comment or credits       //
// of supporting developers from this source code or any supporting         //
// source code which is considered copyrighted (c) material of the          //
// original comment or credit authors.                                      //
// //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
// //
// You should have received a copy of the GNU General Public License        //
// along with this program; if not, write to the Free Software              //
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------ //
include "header.php";
include_once XOOPS_ROOT_PATH . "/class/xoopstree.php";
$myts = &MyTextSanitizer::getInstance(); // MyTextSanitizer object
$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat"), "cid", "pid");

if (!is_object($xoopsUser))
{
    redirect_header(XOOPS_URL . "/user.php", 2, _MD_LOGINFIRST);
    exit();
} 

foreach ($HTTP_POST_VARS as $k => $v)
{
    ${$k} = $v;
} 

foreach ($HTTP_GET_VARS as $k => $v)
{
    ${$k} = $v;
} 

if (isset($HTTP_POST_VARS['submit']))
{
    $lid = intval($HTTP_POST_VARS["lid"]);
    $url = $myts->makeTboxData4Save($url);
    $logourl = $myts->makeTboxData4Save($HTTP_POST_VARS["logourl"]);
    $cid = intval($HTTP_POST_VARS["cid"]);
    $title = $myts->makeTboxData4Save($HTTP_POST_VARS["title"]);
    $homepage = $myts->makeTboxData4Save($HTTP_POST_VARS["homepage"]);
    $version = $myts->makeTboxData4Save($HTTP_POST_VARS["version"]);
    $size = $myts->makeTboxData4Save($HTTP_POST_VARS["size"]);
    $platform = $myts->makeTboxData4Save($HTTP_POST_VARS["platform"]);
    $license = $myts->makeTboxData4Save($HTTP_POST_VARS["license"]);
	$limitations = $myts->makeTboxData4Save($HTTP_POST_VARS["limitations"]);
	
	$description = $myts->makeTareaData4Save($HTTP_POST_VARS["description"]);
    $submitter = $xoopsUser->uid(); 
    $requestdate = time();
	// If user is not admin, send to modification requests, else just update the download
    if (!$xoopsUser->isAdmin($xoopsModule->mid()))
    { 
        // $xoopsDB -> query( "INSERT INTO " . $xoopsDB -> prefix( "mydownloads_mod" ) . " (requestid, lid, cid, title, url, homepage, version, size, platform, logourl, description, modifysubmitter )
        // VALUES ('', $lid, $cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $description, $submitter )" ) )
        $sql = "INSERT INTO " . $xoopsDB->prefix('mydownloads_mod') . " (requestid, lid, cid, title, url, homepage, version, size, platform, logourl, description, modifysubmitter, requestdate) ";
        $sql .= "VALUES ('', $lid, $cid, '$title', '$url', '$homepage', '$version', '$size', '$platform', '$logourl', '$description', $submitter, $requestdate )";
        $result = $xoopsDB->queryF($sql);
        $error = "" . _MD_ERROR . ": <br /><br />" . $sql;
		if (!$result)
        {
            trigger_error($error, E_USER_ERROR);
        } 
        $tags = array();
        $tags['MODIFYREPORTS_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/admin/index.php?op=listModReq';
        $notification_handler = &xoops_gethandler('notification');
        $notification_handler->triggerEvent('global', 0, 'file_modify', $tags);
        redirect_header("index.php", 1, _MD_THANKSFORINFO);
        exit();
    } 
    else
    {
        $result = $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_downloads") . " SET cid = '$cid', title = '$title', url = '$url', homepage = '$homepage', version = '$version', size = '$size', platform = '$platform', logourl = '$logourl', status = '2', date = '" . time() . "' WHERE lid = " . $lid . "");
        $result = $xoopsDB->query("UPDATE " . $xoopsDB->prefix("mydownloads_text") . " SET description = '$description' WHERE lid = " . $lid . "");
        redirect_header("index.php", 2, _MD_ADMININFOUPDATED);
        exit();
    } 
} 
else
{
    include XOOPS_ROOT_PATH . "/header.php";
    include_once XOOPS_ROOT_PATH . "/class/xoopstree.php" ;
    include XOOPS_ROOT_PATH . "/class/xoopsformloader.php";

    echo "<div align = 'center'>" . imageheader() . "</div><br />";
    echo "<div align = 'left'>" . _MD_SUB_SMODIFYMNAMEDESC . "</div>";

    global $xoopsDB, $xoopsModuleConfig;

    $lid = intval($HTTP_GET_VARS['lid']);

    $result = $xoopsDB->query("SELECT cid, title, url, homepage, version, size, platform, logourl, license, limitations FROM " . $xoopsDB->prefix("mydownloads_downloads") . " WHERE lid=$lid ");
    list($cid, $title, $url, $homepage, $version, $size, $platform, $logourl, $license, $limitations) = $xoopsDB->fetchRow($result);
    $result2 = $xoopsDB->query("SELECT description FROM " . $xoopsDB->prefix("mydownloads_text") . " WHERE lid=$lid");
    list($description) = $xoopsDB->fetchRow($result2);

    $xoopsTpl->assign('lang_requestmod', _MD_REQUESTMOD);

    $title = $myts->makeTboxData4Edit($title);
    $url = $myts->makeTboxData4Edit($url);
    $homepage = $myts->makeTboxData4Edit($homepage);
    $version = $myts->makeTboxData4Edit($version);
    $size = $myts->makeTboxData4Edit($size);
    $platform = $myts->makeTboxData4Edit($platform);
    $logourl = $myts->makeTboxData4Edit($logourl);
    $license = $myts->makeTboxData4Edit($license);
    $limitations = $myts->makeTboxData4Edit($limitations);
    $description = $myts->makeTareaData4Edit($description);

    $sform = new XoopsThemeForm(_MD_SUBMITCATHEAD, "storyform", xoops_getenv('PHP_SELF'));
    $sform->addElement(new XoopsFormText(_MD_FILETITLE, 'title', 50, 80, $title), true);
    //$old_url = XOOPS_URL;
	
		//if (preg_match($old_url,$url))
	//{
	//	$sform->addElement(new XoopsFormHidden('url', $url));
	//}else{
		$sform->addElement(new XoopsFormText(_MD_DLURL, 'url', 50, 80, $url), true);
	//}
	    
	$mytree = new XoopsTree($xoopsDB->prefix("mydownloads_cat "), "cid ", "pid");
    ob_start();
    $sform->addElement(new XoopsFormHidden('cid', $cid));
    $mytree->makeMySelBox('title', 'title', $cid, 0);
    $sform->addElement(new XoopsFormLabel(_MD_CATEGORYC, ob_get_contents()));
    ob_end_clean();
    $sform->addElement(new XoopsFormText(_MD_HOMEPAGEC, 'homepage', 50, 80, $homepage), true);
    $sform->addElement(new XoopsFormText(_MD_VERSIONC, 'version', 10, 20, $version), false);
    $sform->addElement(new XoopsFormText(_MD_FILESIZEC, 'size', 10, 20, $size), false);
    //$sform->addElement(new XoopsFormText(_MD_PLATFORMC, 'platform', 50, 80, $platform), false);
        $platform_array = $xoopsModuleConfig['platform'];
        $platform_select = new XoopsFormSelect('', 'platform', $platform, '', '', 0);
        $platform_select->addOptionArray($platform_array);
        $platform_tray = new XoopsFormElementTray(_MD_PLATFORMC, '&nbsp;');
        $platform_tray->addElement($platform_select);
        $sform->addElement($platform_tray);

        $license_array = $xoopsModuleConfig['license'];
        $license_select = new XoopsFormSelect('', 'license', $license, '', '', 0);
        $license_select->addOptionArray($license_array);
        $license_tray = new XoopsFormElementTray(_MD_LICENCEC, '&nbsp;');
        $license_tray->addElement($license_select);
        $sform->addElement($license_tray);

        $limitations_array = $xoopsModuleConfig['limitations'];
        $limitations_select = new XoopsFormSelect('', 'limitations', $limitations, '', '', 0);
        $limitations_select->addOptionArray($limitations_array);
        $limitations_tray = new XoopsFormElementTray(_MD_LIMITATIONS, '&nbsp;');
        $limitations_tray->addElement($limitations_select);
        $sform->addElement($limitations_tray);		
		
    $sform->addElement(new XoopsFormDhtmlTextArea(_MD_DESCRIPTION, 'description', $description, 15, 60), true);
    $sform->addElement(new XoopsFormHidden('logourl', $logourl));
    $option_tray = new XoopsFormElementTray(_MD_OPTIONS, '<br />');
    $notify_checkbox = new XoopsFormCheckBox('', 'notifypub');
    $notify_checkbox->addOption(1, _MD_NOTIFYAPPROVE);
    $option_tray->addElement($notify_checkbox);
    $sform->addElement($option_tray);
    $button_tray = new XoopsFormElementTray('', '');
    $button_tray->addElement(new XoopsFormHidden('lid', $lid));
    $button_tray->addElement(new XoopsFormButton('', 'submit', _SUBMIT, 'submit'));
    $sform->addElement($button_tray);
    $sform->display();
    include XOOPS_ROOT_PATH . '/footer.php';
} 

?>
